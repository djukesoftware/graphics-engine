#type fragmentShader
#name lightModelShader
#version 430 core

out vec4 color;
in vec3 fr_vertex;
uniform samplerCube skybox;

void main() {
    vec3 texture = texture(skybox, fr_vertex).xyz;
    color = vec4(texture,1);
}

#type vertexShader
#version 430 core

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
out vec3 fr_vertex;

void main() {
    vec4 nVertex = vec4(vertex * 50, 1.0);
    vec4 v_pos = viewMatrix * vec4(0,0,0,1);
    //nVertex = vec4((nVertex + v_pos).xyz, 1.0);

    vec4 v_Position = viewMatrix * nVertex;
    v_Position = vec4(v_Position.xyz - v_pos.xyz, 1.0);

    gl_Position = projectionMatrix * v_Position;
    fr_vertex = vertex;
}
