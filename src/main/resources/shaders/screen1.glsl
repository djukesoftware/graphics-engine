#type fragmentShader
#name lightModelShader
#version 430 core

layout (location = 0) out vec4 color;
in vec2 tc;
uniform sampler2D texture;
uniform float time;

void main() {
    vec3 texColor = texture2D(texture, tc).xyz;

    texColor.x += texture2D(texture, vec2(tc.x + 0.04 * pow(sin(tc.y * 10 + time),5), tc.y)).x;
    texColor.z += texture2D(texture, vec2(tc.x - 0.04 * sin(tc.y * 10), tc.y)).z;

    color = vec4(texColor,1);
}

#type vertexShader
#version 430 core

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

out vec2 tc;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 textureCoords; // texture coordinates

void main() {
    vec4 nVertex = vec4(vertex, 1.0);
    mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = MVP * nVertex;
    tc = vec2(textureCoords.x, -textureCoords.y);
}
