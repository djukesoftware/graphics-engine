#type fragmentShader
#name lightModelShader
#version 430 core

layout (location = 0) out vec4 color;
in vec2 tc;
uniform sampler2D texture;

void main() {
    vec3 texColor = texture2D(texture, tc).xyz;
    color = vec4(texColor,1);
}

#type vertexShader
#version 430 core

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

out vec2 tc;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 textureCoords; // texture coordinates

void main() {
    vec4 nVertex = vec4(vertex, 1.0);
    mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = MVP * nVertex;
    tc = textureCoords;
}
