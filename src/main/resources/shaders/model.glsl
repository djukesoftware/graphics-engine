#type fragmentShader
#name modelShader
#version 430 core

layout (location = 0) out vec4 color;
// out vec4 color;

in vec2 fr_textureCoords;
in vec3 cs_normal;
in vec3 cs_lightDirection;
in vec3 cs_eyeDirection;

uniform float time;

// Get textures for model
uniform sampler2D texture;
uniform sampler2D normalTexture;
uniform sampler2D illumination;
uniform sampler2D specular;
uniform samplerCube skybox;

// General light model
vec3 diffuseLight(vec3 lightColor, vec3 normal, vec3 lightDirection);

vec3 specularLight(vec3 lightColor, float level,
                   vec3 normal, vec3 lightDirection, vec3 eyeDirection);

void main() {
    // Get color from every texture
    vec3 textureColor = texture2D(texture, fr_textureCoords).rgb;
    vec3 illuminationColor = texture2D(illumination, fr_textureCoords).rgb;
    vec3 specularTextureColor = texture2D(specular, fr_textureCoords).rgb;

    // Set settings of the light
    vec3 lightColor = vec3(1,1,1);

    vec3 colorDir = reflect(-cs_eyeDirection, cs_normal);
    vec3 colorSky = texture(skybox, colorDir).xyz;

    // Calculate light
    vec3 diffuseColor = textureColor *
            diffuseLight(lightColor, cs_normal, cs_lightDirection);
    // specularTextureColor *
    vec3 specularColor = specularLight(lightColor, 1, cs_normal, cs_lightDirection, cs_eyeDirection);

    // Calculate output color
    //color = vec4(colorSky, 1);
    color = vec4(illuminationColor + diffuseColor + specularColor / 10,1);
}

vec3 diffuseLight(vec3 lightColor, vec3 normal, vec3 lightDirection) {
    return lightColor * clamp(dot(normal, lightDirection),0,1);
}

vec3 specularLight(vec3 lightColor, float level,
                   vec3 normal, vec3 lightDirection, vec3 eyeDirection) {
    vec3 reflection = reflect(-lightDirection, normal);
    return lightColor * pow(clamp(dot(eyeDirection, reflection),0,1), level);
}

#type vertexShader
#version 430 core

out vec2 fr_textureCoords;
out vec3 cs_normal;
out vec3 cs_lightDirection;
out vec3 cs_eyeDirection;

// Get model game.matrix for current rendering of model
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

uniform float time;

layout (location = 0) in vec3 vertex; // mesh of a model
layout (location = 1) in vec3 normal; // normals of a model
layout (location = 2) in vec2 textureCoords; // texture coordinates
layout (location = 3) in vec3 tangent;
layout (location = 4) in vec3 bitangent;

// set position of light source
const vec3 lightPosition = vec3(0,0.5,-1);

void main() {
    // Convert vertices to camera space
    mat4 MV = viewMatrix * modelMatrix;

    // Calculate vectors in camera space
    vec3 cs_vertex          = (MV * vec4(vertex, 1)).xyz;
    vec3 cs_lightPosition   = (viewMatrix * vec4(lightPosition, 1)).xyz;
    // Vector as output of vertex shader
    cs_normal               = normalize((MV * vec4(normal, 0)).xyz);
    cs_eyeDirection         = vec3(0,0,0) - cs_vertex;
    cs_lightDirection       = normalize(cs_lightPosition - cs_vertex);
    // Send texture coordinates to fragment shader
    fr_textureCoords = textureCoords;

    // calculate vertex on the screen
    gl_Position = (projectionMatrix * MV) * vec4(vertex, 1.0);
}

