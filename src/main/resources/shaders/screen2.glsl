#type fragmentShader
#name lightModelShader
#version 430 core

layout (location = 0) out vec4 color;
in vec2 tc;
uniform sampler2D texture;
uniform float time;

void main() {
    vec3 texColor = texture2D(texture, tc).xyz;

    float dx = 0.01f;
    vec3 sum;
    int steps = 8;
    for (int i = -(steps / 2); i <= (steps / 2); i++) {
        sum += texture2D(texture, vec2(tc.x + (i*dx), tc.y)).xyz;
    }
    for (int i = -(steps / 2); i <= (steps / 2); i++) {
        sum += texture2D(texture, vec2(tc.x, tc.y + (i*dx))).xyz;
    }
    sum /= (steps + 1) * 2;

    texColor.x = pow(texColor.x + 0.5f, 8);
    float delta = sin(tc.y * 100 + time);
    delta = round(delta / 1.9f) / 20;
    if (delta != 0) {
        //float alpha = clamp(sin(tc.y * 10),0,1) + 0.2f;
        float alpha = 1;
        texColor.x += texture2D(texture, vec2(tc.x + delta,tc.y)).x / 2 * alpha;
        texColor.x += texture2D(texture, vec2(tc.x - delta,tc.y)).x / 2 * alpha;
    }
    texColor.y = texColor.x;
    texColor.z = 0;
    texColor.x = 0;

    texColor += vec3(0,0.01f,0) +   clamp(vec3(1,1,1) * (pow(sin(tc.x * 80), 10) - 0.8), 0, 1) / 2;
    texColor +=                     clamp(vec3(1,1,1) * (pow(sin(tc.y * 80), 10) - 0.8), 0, 1) / 2;

    color = vec4(texColor,1);
}

#type vertexShader
#version 430 core

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

out vec2 tc;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 textureCoords; // texture coordinates

void main() {
    vec4 nVertex = vec4(vertex, 1.0);
    mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = MVP * nVertex;
    tc = vec2(textureCoords.x, -textureCoords.y);
}
