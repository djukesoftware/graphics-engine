#type fragmentShader
#name lightModelShader
#version 430 core
#define DELTA 2

layout (location = 0) out vec4 color;
in vec2 tc;
in vec3 cs_normal;
in vec3 cs_eyeDirection;
uniform sampler2D texture;

void main() {
    vec3 texColor = texture2D(texture, tc).xyz;
    float delta = 1 - dot(cs_normal, cs_eyeDirection) / (length(cs_normal) * length(cs_eyeDirection));
    delta = pow(delta, DELTA);
    color = vec4(vec3(117.0/255.0,187.0/255.0,253.0/255.0) * delta,1);
}

#type vertexShader
#version 430 core

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

out vec2 tc;
out vec3 cs_normal;
out vec3 cs_eyeDirection;

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 textureCoords; // texture coordinates

void main() {
    // Vector as output of vertex shader
    mat4 MV = viewMatrix * modelMatrix;
    vec3 cs_vertex          = (MV * vec4(vertex, 1)).xyz;
    cs_normal               = normalize((MV * vec4(normal, 0)).xyz);
    cs_eyeDirection         = vec3(0,0,0) - cs_vertex;

    vec4 nVertex = vec4(vertex, 1.0);
    mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = MVP * nVertex;
    tc = textureCoords;
}
