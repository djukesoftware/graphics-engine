#type fragmentShader
#name lightModelShader
#version 430 core

out vec4 color;
void main() {
    color = vec4(1,1,1,1);
}

#type vertexShader
#version 430 core

uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;

void main() {
    vec4 nVertex = vec4(vertex, 1.0);
    mat4 MVP = projectionMatrix * viewMatrix * modelMatrix;
    gl_Position = MVP * nVertex;
}
