package net.djuke.game.scenes;

import net.djuke.engine.data.Package;
import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.camera.Camera;
import net.djuke.engine.graphics.components.camera.Camera3D;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.texture.Texture;
import net.djuke.game.GameData;

import java.io.IOException;

/**
 * DnaScene
 *
 * @author Vadim
 * @version 1.0 05.10.2017
 */
public class DnaScene extends Scene {

    private Camera camera;
    private Model model;

    public DnaScene() {
    }

    public void load() {

        // Set current camera of scene
        camera = new Camera3D();
        setCamera(camera);

        Package matrix = GameData.getPackage("game/matrix");
        Package shadersData = GameData.getPackage("shaders");

        try {
            model = new Model("Space ship");
            model.setMesh((Mesh)matrix.get("dna.obj"));
            model.setShader((Shader)shadersData.get("dna.glsl"));
            model.addTexture((Texture)matrix.get("texture.txr"));
            model.getModelMatrix().translate(0,0,-1f);
            add(model);

        } catch (IOException e) {
            // just ignore this
        }
    }

    public void delete() {

    }

    public void update() {
        model.getModelMatrix().rotate(0.01f,0,1,0);
    }


}
