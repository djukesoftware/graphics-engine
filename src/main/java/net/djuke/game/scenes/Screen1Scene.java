package net.djuke.game.scenes;

import net.djuke.engine.data.Package;
import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.camera.Camera;
import net.djuke.engine.graphics.components.camera.Camera3D;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.texture.Texture;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.game.GameData;
import org.liquidengine.legui.component.Label;

import java.io.IOException;

/**
 * Screen1Scene
 *
 * @author Vadim
 * @version 1.0 04.10.2017
 */
public class Screen1Scene extends Scene {
    private Camera camera;
    private Model model;
    private Label label;
    private InterfaceFrame gui;

    public Screen1Scene() {
    }

    public void load() {
        gui = new InterfaceFrame();
        // Set current camera of scene
        camera = new Camera3D();
        setCamera(camera);

        Package spaceShipData = GameData.getPackage("game/models/spaceShip");
        Package shadersData = GameData.getPackage("shaders");

        label = new Label("subsonic_");
        label.getTextState().setFontSize(150);
        label.setPosition(100,650);
        try {
            // Add all textures to model
            Texture texture = (Texture) spaceShipData.get("texture.txr");
            texture.setName("texture");
            Texture normal = (Texture) spaceShipData.get("normal.txr");
            normal.setName("normalTexture");
            Texture specular = (Texture) spaceShipData.get("specular.txr");
            specular.setName("specular");
            Texture illumination = (Texture) spaceShipData.get("illumination.txr");
            illumination.setName("illumination");

            model = new Model("Space ship");
            model.setMesh((Mesh)spaceShipData.get("model.obj"));
            model.setShader((Shader)shadersData.get("simple.glsl"));
            //model.setShader((Shader)shadersPackage.getObject("model.glsl"));
            model.addTexture((Texture)spaceShipData.get("texture.txr"));
            model.addTexture((Texture)spaceShipData.get("normal.txr"));
            model.addTexture((Texture)spaceShipData.get("illumination.txr"));
            model.getModelMatrix().translate(0,0,-3f);
            add(model);

            //GraphicsInterface.getInstance().addComponent(label);
            Label label = new Label("Screens");
            label.getTextState().setFontSize(10);
            gui.getContainer().add(label);
            //add(gui);
        } catch (IOException e) {
            // just ignore this
        }
    }

    public void delete() {

    }

    public void update() {
        model.getModelMatrix().rotate(0.01f,1, 0, 0);
    }

}
