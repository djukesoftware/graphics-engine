package net.djuke.game.scenes;

import net.djuke.engine.core.Core;
import net.djuke.engine.data.Data;
import net.djuke.engine.data.Package;
import net.djuke.engine.data.beans.TextureData;
import net.djuke.engine.graphics.MousePicking;
import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.Skybox;
import net.djuke.engine.graphics.components.camera.Camera3D;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.texture.CubeTexture;
import net.djuke.engine.graphics.components.texture.Texture;
import net.djuke.engine.graphics.gui.Billboard;
import net.djuke.engine.graphics.gui.DjukeImage;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.engine.script.ScriptManager;
import net.djuke.engine.utils.tools.EngineControlView;
import net.djuke.engine.utils.tools.JSEngineView;
import net.djuke.engine.utils.tools.SceneView;
import net.djuke.game.Game;
import net.djuke.game.GameData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.liquidengine.legui.component.Button;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.optional.align.HorizontalAlign;
import org.liquidengine.legui.icon.ImageIcon;

import java.io.IOException;

import static java.lang.System.out;

/**
 * GameScene
 *
 * @author Vadim
 * @version 1.0 16.07.2017
 */
public class GameScene extends Scene {

    private Logger logger = LogManager.getLogger("GameScene");
    private Camera3D camera;
    private SceneView view;
    private Billboard billboard;
    private Label label;

    private Button button = new Button();

    private Vector3f vector = new Vector3f(0,0,-3f);
    private Vector3f up = new Vector3f(0,1,0);
    private MousePicking mousePicking;

    private InterfaceFrame gui;

    private Model model2;
    private Model sphere;

    public void load() {
        // Set current camera of scene
        camera = new Camera3D();
        camera.getViewMatrix().translate(0,-0.5f,0);
        setCamera(camera);

        gui = new InterfaceFrame();

        // Get game data
        Package icons = GameData.getPackage("game/icons");
        Package spaceShipData = GameData.getPackage("game/models/spaceShip");
        Package shadersData = GameData.getPackage("shaders");
        Package skyboxData = GameData.getPackage("skybox");
        Package matrix = GameData.getPackage("game/matrix");
        Package eyeball = GameData.getPackage("game/models/eyeball");
        Package engineData = Data.getEngineData();

        // Add all textures to model
        try {
            Texture texture = (Texture) spaceShipData.get("texture.txr");
            texture.setName("texture");
            texture = (Texture) eyeball.get("texture.txr");
            texture.setName("texture");
            texture = (Texture) eyeball.get("normal.txr");
            texture.setName("normalTexture");

            Texture normal = (Texture) spaceShipData.get("normal.txr");
            normal.setName("normalTexture");
            Texture specular = (Texture) spaceShipData.get("specular.txr");
            specular.setName("specular");
            Texture illumination = (Texture) spaceShipData.get("illumination.txr");
            illumination.setName("illumination");
        } catch (IOException e) {
            logger.error("First: " + e.getMessage());
        }

        try {

            Skybox skybox = new Skybox("Skybox", new CubeTexture(
                    (TextureData) skyboxData.get("right.txr"),
                    (TextureData) skyboxData.get("left.txr"),
                    (TextureData) skyboxData.get("top.txr"),
                    (TextureData) skyboxData.get("bottom.txr"),
                    (TextureData) skyboxData.get("front.txr"),
                    (TextureData) skyboxData.get("back.txr")
            ));
            add(skybox);

            sphere = new Model("Sphere");
            sphere.setMesh((Mesh)eyeball.get("eyeball.obj"));
            sphere.setShader((Shader)engineData.get("model.glsl"));
            sphere.addTexture((Texture)eyeball.get("normal.txr"));
            sphere.addTexture((Texture)eyeball.get("texture.txr"));
            sphere.getModelMatrix().translate(0,0,-4f);
            add(sphere);
            mousePicking = new MousePicking(this, sphere, 1);

            ScriptManager.getInstance().addBinding("sphere", sphere);

            Model model = new Model("Space ship");
            model.setMesh((Mesh)spaceShipData.get("model.obj"));
            model.setShader((Shader)shadersData.get("simple.glsl"));
            //model.setShader((Shader)shadersPackage.get("model.glsl"));
            model.addTexture((Texture)spaceShipData.get("texture.txr"));
            model.addTexture((Texture)spaceShipData.get("normal.txr"));
            model.addTexture((Texture)spaceShipData.get("illumination.txr"));
            model.getModelMatrix().translate(0,1,-4f);
            //add(model);

            model2 = new Model("Space ship another");
            model2.setMesh((Mesh)spaceShipData.get("model.obj"));
            model2.setShader((Shader)shadersData.get("model.glsl"));
            model2.addTexture((Texture)spaceShipData.get("texture.txr"));
            model2.addTexture((Texture)spaceShipData.get("normal.txr"));
            model2.addTexture((Texture)spaceShipData.get("illumination.txr"));
            model2.getModelMatrix().translate(0,-1,-4f);
            billboard = new Billboard(getCamera(), model2.getModelMatrix());
            //add(model2);

            Texture generalTexture = (Texture)matrix.get("texture.txr");

            // *********************************************************
            Model screens = new Model("Screens");
            screens.setMesh((Mesh)matrix.get("screens.obj"));
            screens.addTexture(generalTexture);
            screens.setShader((Shader)shadersData.get("simple.glsl"));
            screens.getModelMatrix().translate(0,0,-2f);
            screens.getModelMatrix().scale(0.5f);
            screens.getModelMatrix().rotate((float)Math.toRadians(-90), 0,1,0);
            //add(screens);
            // *********************************************************
            screens = new Model("Screen1");
            screens.setMesh((Mesh)matrix.get("screen1.obj"));
            // screens.addTexture(Game.dnaFrame);
            screens.setShader((Shader)shadersData.get("screen1.glsl"));
            screens.getModelMatrix().translate(0,0,-2f);
            screens.getModelMatrix().scale(0.5f);
            screens.getModelMatrix().rotate((float)Math.toRadians(-90), 0,1,0);
            add(screens);
            // *********************************************************
            screens = new Model("Screen2");
            screens.setMesh((Mesh)matrix.get("screen2.obj"));
            //screens.addTexture(Game.screenFrame);
            screens.setShader((Shader)shadersData.get("screen2.glsl"));
            screens.getModelMatrix().translate(0,0,-2f);
            screens.getModelMatrix().scale(0.5f);
            screens.getModelMatrix().rotate((float)Math.toRadians(-90), 0,1,0);
            add(screens);
            // *********************************************************
            screens = new Model("Screen3");
            screens.setMesh((Mesh)matrix.get("screen3.obj"));
            //screens.addTexture(Game.screenFrame);
            screens.setShader((Shader)shadersData.get("screen3.glsl"));
            screens.getModelMatrix().translate(0,0,-2f);
            screens.getModelMatrix().scale(0.5f);
            screens.getModelMatrix().rotate((float)Math.toRadians(-90), 0,1,0);
            add(screens);
            // *********************************************************
            screens = new Model("Screen4");
            screens.setMesh((Mesh)matrix.get("screen4.obj"));
            //screens.addTexture(Game.smallScreenFrame);
            screens.setShader((Shader)shadersData.get("screen4.glsl"));
            screens.getModelMatrix().translate(0,0,-2f);
            screens.getModelMatrix().scale(0.5f);
            screens.getModelMatrix().rotate((float)Math.toRadians(-90), 0,1,0);
            add(screens);


            // Create graphical user interface
            // ...
            // Add GUI to render inside current scene
            // ...

        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        new JSEngineView(gui);
        view = new SceneView(this, gui);

        label = new Label("Sphere");
        label.getTextState().setHorizontalAlign(HorizontalAlign.CENTER);
        //gui.addComponent(label);

        try {
            button.getTextState().setText("");
            button.setSize(64,64);
            button.setBackgroundColor(0,0,0,0);
            button.setBorder(null);
            DjukeImage image = new DjukeImage((Texture)icons.get("planet.txr"));
            ImageIcon icon = new ImageIcon(image);
            icon.setSize(new Vector2f(64, 64));
            button.setBackgroundIcon(icon);
            gui.getContainer().add(button);
        } catch (IOException e) {

        }

        gui.getContainer().add(EngineControlView.getInstance());

        add(gui);
    }

    public void update() {
        float time = (float)Core.getTime();

        vector.set((float)Math.sin(time) * 4, 0, 3f);
        camera.getViewMatrix().identity().rotateTowards(vector,up);

        //screen4.render();
        //screen1.render();
        //bioScreen.render();

        sphere.getModelMatrix().translation((float)Math.sin(time),0,-5 + (float)Math.cos(time));
        sphere.setDebug(mousePicking.intersect());

        view.update();
        model2.getModelMatrix().translation((float)Math.sin(time / 10) * 2, 0,-3f);
        float x = billboard.getScreenCoordinates().x - 32;
        float y = billboard.getScreenCoordinates().y - 32;
        button.setPosition(x, y);
        label.setPosition(billboard.getScreenCoordinates());
    }

    public void delete() {
        out.println("Delete data");
    }

}
