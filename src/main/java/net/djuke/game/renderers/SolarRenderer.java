package net.djuke.game.renderers;

import net.djuke.engine.data.Data;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.renderers.Renderer;
import net.djuke.game.Game;
import net.djuke.game.GameData;
import net.djuke.game.mechanics.GObject;
import net.djuke.game.mechanics.world.Planet;
import net.djuke.game.mechanics.world.Solar;
import net.djuke.game.mechanics.world.SpaceObject;
import net.djuke.game.mechanics.world.Star;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * SolarRenderer
 *
 * @author Vadim
 * @version 1.0 25.11.2017
 */
public class SolarRenderer implements Renderer<Solar> {

    private Logger logger = LogManager.getLogger(this.getClass());
    private Renderer<Model> modelRenderer;

    private Model starModel;
    private Model planetModel;

    public SolarRenderer() {
        /*
        В качестве модели для отрисовки планет временно будем использовать
        стандартную модель сферы из данных движка
         */
        try {
            Mesh mesh = (Mesh) Data.getEngineData().get("sphere.obj");

            starModel = new Model("star");
            starModel.setMesh(mesh);
            starModel.setShader((Shader) GameData.getPackage("game/models/star").get("shader.glsl"));

            planetModel = new Model("planet");
            planetModel.setMesh(mesh);
            planetModel.setShader((Shader) GameData.getPackage("game/models/planet").get("shader.glsl"));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        modelRenderer = Graphics.getRenderer(Model.class);
    }

    @Override
    public void render(Scene environment, Solar solar) {
        for (GObject object : solar.getObjects()) {
            if (object instanceof SpaceObject) {

                SpaceObject sObject = (SpaceObject)object;
                if (sObject instanceof Planet) {
                    planetModel.getModelMatrix().identity().translate(solar.getObjectPosition(sObject)).scale(sObject.getRadius());
                    modelRenderer.render(environment, planetModel);
                } else if (sObject instanceof Star) {
                    planetModel.getModelMatrix().identity().scale(sObject.getRadius());
                    modelRenderer.render(environment, starModel);
                }

            }
        }
    }

}
