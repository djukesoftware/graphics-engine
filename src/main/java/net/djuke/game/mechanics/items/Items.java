package net.djuke.game.mechanics.items;

import net.djuke.game.mechanics.GObject;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Items
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public class Items {

    private static List<GObject> items = new CopyOnWriteArrayList<>();

    static {
        items.add(new Krion());
    }

    public static boolean containsItem(GObject object) {
        return items.contains(object);
    }

    public static GObject getItem(String name) {
        for (GObject item : items) {
            if (item.getName().equals(name)) return item;
        }
        return null;
    }
}
