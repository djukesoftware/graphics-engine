package net.djuke.game.mechanics.items;

import net.djuke.game.mechanics.GContainer;
import net.djuke.game.mechanics.GObject;

/**
 * Item
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public interface Item extends GObject {

    default void setParent(GContainer parent) {
        // do nothing
    }

    default GContainer getParent() {
        return null;
    }

    default void update(double time) {
        // do nothing
    }

}
