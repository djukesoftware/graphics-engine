package net.djuke.game.mechanics.items;


/**
 * Krion
 *
 * @author Vadim
 * @version 1.0 29.11.2017
 */
public class Krion implements Item {

    Krion() {

    }

    public String getName() {
        return "items.krion";
    }

    public String getDescription() {
        return "items.krionDescription";
    }

}
