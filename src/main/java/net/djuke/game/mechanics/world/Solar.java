package net.djuke.game.mechanics.world;

import net.djuke.engine.utils.StringGenerator;
import net.djuke.game.mechanics.GContainer;
import net.djuke.game.mechanics.GObject;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Solar
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public class Solar implements GContainer {

    private String name;
    private String description;
    private List<GObject> objects;
    private List<Vector3f> objectsPositions;

    public Solar() {

    }

    public void generate() {
        objects = new ArrayList<>();
        objectsPositions = new ArrayList<>();

        // Генерируем имя для нашей системы
        name = StringGenerator.generateName();
        // Создаём планет по больше
        for (int c=1; c<10; c++) {
            // create new planet
            Planet planet = new Planet();
            // generate
            planet.generate();
            // put to this solar
            putObject(planet);
        }

        Star star = new Star();
        star.generate();
        putObject(star);
    }

    public void update(double delta) {
        for (GObject object : objects) {
            object.update(delta);
        }
    }


    @Override
    public GContainer getParent() {
        return null;
    }

    @Override
    public void setParent(GContainer parent) {
        // nothing
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getObjectIndex(GObject object) {
        return objects.indexOf(object);
    }

    public Vector3f getObjectPosition(GObject object) {
        return objectsPositions.get(objects.indexOf(object));
    }

    @Override
    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public List<GObject> getObjects() {
        return objects;
    }

    @Override
    public void removeObject(GObject object) {
        objects.remove(object);
    }

    public void setObjects(List<GObject> objects) {
        this.objects = objects;
    }

    @Override
    public void putObject(GObject object) {
        object.setParent(this);
        if (objects == null) objects = new ArrayList<>();
        objectsPositions.add(new Vector3f());
        objects.add(object);
    }

}
