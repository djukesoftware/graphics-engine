package net.djuke.game.mechanics.world;

import net.djuke.engine.utils.StringGenerator;
import net.djuke.game.mechanics.GContainer;
import net.djuke.game.mechanics.GObject;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Planet
 *
 * @author Vadim
 * @version 1.0 29.11.2017
 */
public class Planet implements GContainer, SpaceObject {

    private String name;
    private float radius;
    private float orbitRadius;

    private float alpha;
    private float orbitSpeed;

    private List<GObject> objects;
    private GContainer parent;

    public Planet() {

    }

    public String getType() {
        return "type.planet";
    }

    public void generate() {
        Random random = new Random();
        radius = 0.15f + random.nextFloat() * 0.1f;
        orbitRadius = 2 + random.nextFloat() * 20;
        orbitSpeed = 0.1f + random.nextFloat() / 10f;
        alpha = random.nextFloat() * 6.28f;
        name = StringGenerator.generateName();
    }

    public void update(double delta) {
        if (parent != null && parent instanceof Solar) {
            alpha += orbitSpeed * (delta /10);
            Solar solar = (Solar) parent;
            Vector3f position = solar.getObjectPosition(this);
            position.set((float)Math.sin(alpha) * orbitRadius, 0, (float)Math.cos(alpha) * orbitRadius);
        }
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public float getOrbitRadius() {
        return orbitRadius;
    }

    public void setOrbitRadius(float orbitRadius) {
        this.orbitRadius = orbitRadius;
    }

    public void setObjects(List<GObject> objects) {
        this.objects = objects;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GContainer getParent() {
        return parent;
    }

    public void setParent(GContainer parent) {
        this.parent = parent;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<GObject> getObjects() {
        return objects;
    }

    @Override
    public void removeObject(GObject object) {
        objects.remove(object);
    }

    @Override
    public void putObject(GObject object) {
        if (objects == null) objects = new ArrayList<>();
        objects.add(object);
    }
}
