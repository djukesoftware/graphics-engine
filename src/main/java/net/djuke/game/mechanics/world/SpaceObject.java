package net.djuke.game.mechanics.world;

import net.djuke.game.mechanics.GObject;

/**
 * SpaceObject
 *
 * @author Vadim
 * @version 1.0 11.01.2018
 */
public interface SpaceObject extends GObject {

    float getRadius();
}
