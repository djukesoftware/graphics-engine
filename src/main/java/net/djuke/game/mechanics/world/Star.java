package net.djuke.game.mechanics.world;

import net.djuke.engine.utils.StringGenerator;
import net.djuke.game.mechanics.GContainer;
import net.djuke.game.mechanics.GObject;

/**
 * Star
 *
 * @author Vadim
 * @version 1.0 11.01.2018
 */
public class Star implements GObject, SpaceObject {

    private String name;

    public Star() {

    }

    public String getType() {
        return "type.star";
    }

    public void generate() {
        name = StringGenerator.generateName();
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public GContainer getParent() {
        return null;
    }

    @Override
    public void setParent(GContainer container) {

    }

    @Override
    public void update(double delta) {

    }

    public float getRadius() {
        return 1;
    }
}
