package net.djuke.game.mechanics.objects.characters;

import net.djuke.game.mechanics.GContainer;
import net.djuke.game.mechanics.GObject;
import net.djuke.game.mechanics.objects.characters.actions.Action;
import net.djuke.game.mechanics.objects.characters.actions.Actions;

import java.util.List;

/**
 * Character - superclass for all characters in game
 *
 * @author Vadim
 * @version 1.0 21.11.2017
 */
public abstract class Character implements GObject {

    private String name;
    private String race;

    private String description;
    private List<Integer> reactions;

    private GContainer parent;

    public Character() {

    }

    public Character(String name) {
        this.name = name;
    }

    public GContainer getParent() {
        return parent;
    }

    public void setParent(GContainer parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract String getRace();

    /**
     * Над каждым персонажем в игре можно совершить какое-нибудь действие (толкнуть, ударить, ограбить, отравить и тому подобное).
     * Данный метод отвечает за совершение действия над данным персонажем
     * @see Actions
     * @param action id of action
     */
    public abstract void doActionOnCharacter(Action action);

    /**
     * Совершить последовательно несколько действий над персонажем
     * @param actions list of actions
     */
    public final void doActionsOnCharacter(Action... actions) {
        for (Action action : actions) {
            doActionOnCharacter(action);
        }
    }

    public void update(double delta) {

    }

    /**
     * После того как над персонажем совершили действие (или по его собственной воли), сам персонаж может ответить какой-либо
     * реакцией, которая будет обрабатываться уже отрисовщиком персонажа. К примеру выкрикнуть что-либо или сказать что-нибудь.
     * @return list of reactions id
     */
    public final List<Integer> getReactions() {
        return reactions;
    }

    /**
     * Удаляем все возможные реакции
     */
    public final void clearReactions() {
        reactions.clear();
    }

}
