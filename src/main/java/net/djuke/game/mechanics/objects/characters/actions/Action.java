package net.djuke.game.mechanics.objects.characters.actions;

/**
 * Action
 *
 * @author Vadim
 * @version 1.0 21.11.2017
 */
public interface Action {

    String getName();

}
