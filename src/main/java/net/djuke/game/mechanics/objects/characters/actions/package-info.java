/**
 * Содержит класс всех действий, которые можно произвести над персонажами
 *
 * @version 1.0 21.11.2017
 * @author Vadim
 */
package net.djuke.game.mechanics.objects.characters.actions;