package net.djuke.game.mechanics.objects.characters;

import net.djuke.game.mechanics.objects.characters.actions.Action;
import net.djuke.game.mechanics.objects.characters.actions.Actions;

import java.util.ResourceBundle;

/**
 * Human - класс человека
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public class Human extends Character {

    public Human() {

    }

    public Human(String name) {
        setName(name);
    }

    public String getRace() {
        return ResourceBundle.getBundle("game").getString("characters.races.human");
    }

    /**
     * Выполнить какое либо действие над человеком
     * @see Actions
     * @param action id of action
     */
    public void doActionOnCharacter(Action action) {

    }
}
