package net.djuke.game.mechanics.objects.characters.actions;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Action - хранит все возможные действия, которые мы можем провести над персонажами.
 *
 * @author Vadim
 * @version 1.0 21.11.2017
 */
public class Actions {

    private static List<Action> actions = new CopyOnWriteArrayList<>();

    static {
        actions.add(new Hug());
    }

    public static List<Action> getActionsList() {
        return actions;
    }

    public static Action getActionByName(String name) {
        for (Action action : actions) {
            if (action.getName().equals(name)) return action;
        }
        return null;
    }

    public static Action getActionById(int id) {
        return actions.get(id);
    }

    public static int getActionId(Action action) {
        return actions.indexOf(action);
    }

}
