package net.djuke.game.mechanics.objects.characters.actions;

/**
 * Hug
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public class Hug implements Action {

    Hug() {

    }

    public String getName() {
        return "Hug";
    }

}
