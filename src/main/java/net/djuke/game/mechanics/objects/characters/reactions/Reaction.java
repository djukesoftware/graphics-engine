package net.djuke.game.mechanics.objects.characters.reactions;

/**
 * Reaction
 *
 * @author Vadim
 * @version 1.0 21.11.2017
 */
public interface Reaction {

    String getName();

}
