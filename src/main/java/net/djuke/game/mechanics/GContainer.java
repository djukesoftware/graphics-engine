package net.djuke.game.mechanics;

import java.util.List;

/**
 * GContainer - любой игровой объект, который может содержать в себе
 * другие игровые объекты.
 *
 * @author Vadim
 * @version 1.0 29.11.2017
 */
public interface GContainer extends GObject {

    List<GObject> getObjects();

    void removeObject(GObject object);

    void putObject(GObject object);

    default void moveObject(GObject object) {
        // Delete object from old container
        if (object.getParent() != null) object.getParent().removeObject(object);
        // Set new parent of this object
        object.setParent(this);
        // Put object to list
        putObject(object);
    }
}
