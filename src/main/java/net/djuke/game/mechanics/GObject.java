package net.djuke.game.mechanics;

import net.djuke.engine.graphics.components.scene.SceneObject;
import net.djuke.game.mechanics.GContainer;

/**
 * GObject - интерфейс, описывающий любой игровой объект
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public interface GObject extends SceneObject {

    /**
     * Get simple name of game object
     * @return name of game object
     */
    String getName();

    default void generate() {
        // do nothing;
    }

    default String getType() {
        return "type.unknown";
    }

    default String getDescription() {
        return "items.withoutDescription";
    }

    /**
     * Получить контейнер, в котором хранится данный объект
     * @return вернуть контейнер
     */
    GContainer getParent();

    /**
     * Установить новый контейнер, хранящий в себе данный объект
     * @param container контейнер
     */
    void setParent(GContainer container);

    void update(double delta);

}
