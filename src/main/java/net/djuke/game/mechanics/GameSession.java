package net.djuke.game.mechanics;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * GameSession - представление игрового сохранения с информацией о загрузки игровых данных, а так же мета-инфомацией:
 * последний игрок, время сохранения, время создания сохранения и т.д. и т.п.
 *
 * @author Vadim
 * @version 1.0 16.11.2017
 */
public class GameSession {

    private String name;
    private String createDate;
    private String lastGameDate;
    private float gameTime;

    private static World world;

    public GameSession() {

    }

    /**
     * Создаём игровую сессию под определённым именем
     * и устанавливаем дату создания
     * @param name имя игровой сессии
     */
    public GameSession(String name) {
        this.name = name;
        LocalDate date = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        createDate = date.format(formatter);
        lastGameDate = date.format(formatter);

        world = new World();
        world.generateWorld();
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getLastGameDate() {
        return lastGameDate;
    }

    public void setLastGameDate(String lastGameDate) {
        this.lastGameDate = lastGameDate;
    }

    public float getGameTime() {
        return gameTime;
    }

    public void setGameTime(float gameTime) {
        this.gameTime = gameTime;
    }
}
