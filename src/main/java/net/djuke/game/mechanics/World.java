package net.djuke.game.mechanics;

import net.djuke.game.mechanics.objects.characters.Human;
import net.djuke.game.mechanics.world.Solar;
import java.util.List;
import java.util.Random;

/**
 * World - represents the game world
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public class World {

    private static Random random = new Random();

    private Human           player;
    private Solar           currentSolar;
    private List<String>    solarsList;

    // TODO: Передавать новогого игрока
    public void generateWorld() {
        currentSolar = new Solar();
        currentSolar.generate();

        player = new Human("Vadim Gush");
        currentSolar.moveObject(player);
    }

    public Human getPlayer() {
        return player;
    }

    public Solar getCurrentSolar() {
        return currentSolar;
    }

    public void update(double delta) {
        currentSolar.update(delta);
    }

}
