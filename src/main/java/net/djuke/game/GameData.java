package net.djuke.game;

import net.djuke.engine.data.Package;
import net.djuke.game.mechanics.GameSession;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * GameData - хранит основные ресурсы, используемые в игре.
 *
 * @author Vadim
 * @version 1.0 31.07.2017
 */
public class GameData {

    private static Map<String, Package>    dataCollection = new HashMap<>();

    /**
     * Полуичить пакет данных по определённому имени
     * @param name name of package
     * @return package of game data
     */
    public static Package getPackage(String name) {
        return dataCollection.get(name);
    }

    /**
     * Puts package of game data to current map
     * @param name name of package
     * @param data package with game data
     */
    public static void putPackage(String name, Package data) {
        dataCollection.put(name, data);
    }

    /**
     * Removes package of game data
     * @param name name of package
     */
    public static void removePackage(String name) {
        dataCollection.remove(name);
    }

    /**
     * Returns list of packages
     * @return list of packages
     */
    public static Collection<String> getPackagesList() {
        return dataCollection.keySet();
    }

}
