package net.djuke.game;

import net.djuke.engine.core.Core;
import net.djuke.engine.data.Data;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.texture.Frame;
import net.djuke.game.mechanics.GameSession;
import net.djuke.game.mechanics.world.Solar;
import net.djuke.game.renderers.SolarRenderer;
import net.djuke.game.stages.GameStage;
import net.djuke.game.stages.LoadingStage;
import net.djuke.game.stages.MenuStage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

/**
 * GameCore - the core of game project. Contain last game session and settings of game.
 * @see net.djuke.game.GameSettings
 * @see GameSession
 *
 * @author Vadim
 * @version 1.0 16.07.2017
 */
public class Game {

    private static Logger           logger;
    private static Frame            mainFrame;
    private static GameSettings     settings;
    private static GameSession      session;
    private static ResourceBundle   localization;

    static {
        logger = LogManager.getLogger(Game.class);
    }

    /**
     * Run core of the game
     */
    public static void startup() {

        try {
            logger.info("Loading game settings");
            settings = (GameSettings) Data.loadObject("resources/configuration/game.json", Data.OS_FILE_SYSTEM);
            Locale.setDefault(Locale.forLanguageTag(settings.getLocale()));
        } catch (IOException e) {
            settings = GameSettings.getDefault();
        }

        logger.info("Loading localization for " + Locale.getDefault().getDisplayName());
        localization = ResourceBundle.getBundle("game");

        mainFrame = new Frame((int)Graphics.getSettings().getResolutionWidth(), (int)Graphics.getSettings().getResolutionHeight());
        mainFrame.setName("Main frame");
        Graphics.getWindow().addFrame(mainFrame);

        loadGameData();

        Graphics.addRenderer(Solar.class, new SolarRenderer());

        startGame();

        shutdown();
    }

    private static void startGame() {
        logger.info("Stating menu stage");
        MenuStage menu = new MenuStage();
        mainFrame.setScene(menu);

        logger.info("Waiting game session");
        session = menu.getGameSession();

        if (session != null) {
            mainFrame.setScene(new GameStage());
            worldUpdate();
        }
    }

    private static void loadGameData() {
        logger.info("Switch to loading stage");
        LoadingStage loadingScene = new LoadingStage();
        mainFrame.setScene(loadingScene);

        logger.info("Waiting loading of data from loading stage");
        loadingScene.waitData();
    }

    /**
     * Shutdowns game core
     */
    private static void shutdown() {
        try {
            logger.info("Saving settings and current session");
            Data.saveObject("resources/configuration/game.json", settings, Data.OS_FILE_SYSTEM);
            if (session!= null) Data.saveObject("resources/sessions/" + session.getName() + ".json", session, Data.OS_FILE_SYSTEM);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * Update game world
     */
    private static void worldUpdate() {
        double startupTime = Core.getTime();
        double lastUpdateTime = Core.getTime();

        while (Graphics.getWindow().isOpen()) {
            double delta = Core.getTime() - lastUpdateTime;

            session.getWorld().update((float)delta);

            lastUpdateTime = Core.getTime();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        session.setGameTime(session.getGameTime() + (float)(Core.getTime() - startupTime));
    }

    /**
     * Get current settings of game core
     * @return settings of game core
     */
    public static GameSettings getSettings() {
        return settings;
    }

    public static GameSession getSession() {
        return session;
    }

    public static ResourceBundle getLocalization() {
        return localization;
    }

}
