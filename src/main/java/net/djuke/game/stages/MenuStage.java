package net.djuke.game.stages;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.game.Game;
import net.djuke.game.GameData;
import net.djuke.game.GameSettings;
import net.djuke.game.mechanics.GameSession;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.liquidengine.legui.component.Button;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.event.MouseClickEvent;

import java.util.ResourceBundle;

/**
 * MenuStage - главное меню, отвачающее за загрузку игрового сохранения.
 * @see GameSession
 *
 * @author Vadim
 * @version 1.0 16.11.2017
 */
public class MenuStage extends Scene {

    private Logger logger = LogManager.getLogger(MenuStage.class);
    private GameSession gameSession;
    private boolean exit = false;

    public void load() {
        InterfaceFrame gui = new InterfaceFrame();

        GameSettings settings = Game.getSettings();
        if (settings.isSkipMenu())
            loadGameSession(settings.getDefaultGameSession());
        else {
            // Create input field for javascript source

            Label accountEmail = new Label("Perihelion");
            accountEmail.getTextState().setFontSize(50);
            accountEmail.setPosition(Graphics.interfaceUnitToPixels(1), Graphics.interfaceUnitToPixels(1));
            gui.getContainer().add(accountEmail);

            Label version = new Label("Prototype");
            version.setPosition(Graphics.interfaceUnitToPixels(1), Graphics.interfaceUnitToPixels(1 + 0.4f * 1));
            gui.getContainer().add(version);

            ResourceBundle localization = Game.getLocalization();

            Button newGame = new Button(localization.getString("core.newGame"));
            newGame.setSize(Graphics.interfaceUnitToPixels(2), Graphics.interfaceUnitToPixels(0.3f));
            newGame.setPosition(Graphics.interfaceUnitToPixels(1), Graphics.interfaceUnitToPixels(1 + 0.4f * 3));
            gui.getContainer().add(newGame);

            newGame.getListenerMap().addListener(MouseClickEvent.class, (e) -> {
                loadGameSession("default");
            });

            Button exitGame = new Button(localization.getString("core.exitGame"));
            exitGame.setSize(Graphics.interfaceUnitToPixels(2), Graphics.interfaceUnitToPixels(0.3f));
            exitGame.setPosition(Graphics.interfaceUnitToPixels(1), Graphics.interfaceUnitToPixels(1 + 0.4f * 4));
            gui.getContainer().add(exitGame);

            exitGame.getListenerMap().addListener(MouseClickEvent.class, (e) -> {
                exitGame();
            });

        }

        add(gui);


    }

    /**
     * Загружаем игровую сессию по имени и уведмляем последний поток, ожидающий загрузки сохранения (GameCore)
     * @see Game
     * @param name имя сохранения
     */
    private synchronized void loadGameSession(String name) {
        gameSession = new GameSession(name);
        notify(); // gamecore ожидает, пока menustage получит последнее сохранение
    }

    private synchronized void exitGame() {
        exit = true;
        notify();
    }

    public void update() {

    }

    public void delete() {

    }

    /**
     * Получаем загруженное игровое сохранение. Если загрузка ещё не произошла, то поток, вызывающий данный метод,
     * будет ожидать загрузки данных
     * @return loaded game session
     */
    public synchronized GameSession getGameSession() {
        while(gameSession == null && !exit) {
            try {
                wait();
            } catch (InterruptedException e) {
                logger.error(e.getMessage());
            }
        }
        return gameSession;
    }

}
