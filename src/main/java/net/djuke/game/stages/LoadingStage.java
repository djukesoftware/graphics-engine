package net.djuke.game.stages;

import net.djuke.engine.data.Data;
import net.djuke.engine.data.Package;
import net.djuke.engine.graphics.GraphicsDataLoader;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.camera.Camera2D;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.game.GameData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector4f;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.component.ProgressBar;
import org.liquidengine.legui.component.optional.align.HorizontalAlign;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * LoadingScene - subclass of Scene for rendering loading screen and ExecuteService for running task of loading
 * game data inside a Render thread.
 *
 * @author Vadim
 * @version 1.0 16.07.2017
 */
public class LoadingStage extends Scene {

    private Logger logger = LogManager.getLogger(this.getClass());
    private Label information;
    private ProgressBar bar;

    private Queue<String> packages = new LinkedList<>();
    private FutureTask<Package> currentPackage;
    private int packagesCount;

    public void load() {
        setCamera(new Camera2D());

        // Create interface
        InterfaceFrame gui = new InterfaceFrame();
        // Panel for information about loading
        Panel loadingScreen = new Panel();
        loadingScreen.setBackgroundColor(0,0,0,0);
        loadingScreen.setSize(Graphics.getSettings().getResolution());
        loadingScreen.setBorder(null);
        gui.getContainer().add(loadingScreen);
        // Label with status
        information = new Label();
        information.getTextState().setTextColor(1,1,1,1);
        information.getTextState().setText(ResourceBundle.getBundle("game").getString("core.loadingGameResources"));
        information.getTextState().setHorizontalAlign(HorizontalAlign.CENTER);
        information.setPosition(Graphics.widthPercentToPixels(0.5f), Graphics.heightPercentToPixels(0.9f));
        loadingScreen.add(information);
        // Add all components to interface
        bar = new ProgressBar();
        bar.setSize(Graphics.widthPercentToPixels(0.9f), Graphics.interfaceUnitToPixels(0.25f));
        bar.setPosition(Graphics.widthPercentToPixels(0.05f), Graphics.heightPercentToPixels(0.95f));
        bar.setProgressColor(new Vector4f(1,0,0,1));
        loadingScreen.add(bar);
        add(gui);

        // Add paths of game data
        packages.add("game/models/lightModel");
        packages.add("game/matrix");
        packages.add("shaders");
        // packages.add("skybox");
        packages.add("game/models/spaceShip");
        packages.add("game/icons");
        packages.add("game/models/eyeball");
        packages.add("game/models/star");
        packages.add("game/models/planet");
        packages.add("game/interface");

        packagesCount = packages.size();
    }

    /**
     * Добавляем путь к пакету, который необходимо загрузить
     * @param path
     */
    public void addPath(String path) {
        packages.add(path);
    }

    public void update() {
        // Если у нас нет загружаемого пакета
        if (currentPackage == null) {
            // Просматриваем очередь
            if (packages.size() > 0) {
                // Получаем последний путь к пакету
                String lastElement = packages.remove();
                // Запускаем его загрузку в отдельном потоке
                currentPackage = Data.concurrentLoadPackage(lastElement, Data.ENGINE_FILE_SYSTEM);
                float alpha = (float)(packagesCount - packages.size()) / packagesCount;
                bar.setProgressColor(new Vector4f(1 - alpha,alpha,0,1));
                bar.setValue(alpha * 100f);

                // Если же очередь пустая, то уведомляем остальных о окончании загрузки данных
            } else {
                try {
                    /*
                    Отдельно загружаем skybox'ы, потому что из множества файлов надо создать одну текстуру. В будущем я добавлю
                    файл package-build, который будет содержать информацию о создании объектов на основе множества файлов
                     */
                    GameData.putPackage("skybox", Data.loadPackage("game/skyboxes/tucanaGalaxy", Data.ENGINE_FILE_SYSTEM));
                } catch (IOException e) { logger.error(e.getMessage()); }

                // GameCore ожидает пока закончится загрузка данных и поэтому нам необходимо его уведомлять
                synchronized (this) {
                    notify();
                }
            }
        } else {
            // Если у нас есть загружаемый пакет, то проверяем статус его загрузки
            if (currentPackage.isDone()) {
                try {
                    // Load graphics data from loaded package
                    Package graphicsData = GraphicsDataLoader.load(currentPackage.get());
                    GameData.putPackage(graphicsData.getName(), graphicsData);
                } catch (InterruptedException | ExecutionException e) {
                    logger.error(e.getMessage());
                }
                // And switch to next package
                currentPackage = null;
            }
        }
    }

    public synchronized void waitData() {
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void delete() {
    }

}
