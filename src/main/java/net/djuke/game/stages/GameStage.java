package net.djuke.game.stages;

import net.djuke.engine.data.Package;
import net.djuke.engine.data.beans.TextureData;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.GraphicsSettings;
import net.djuke.engine.graphics.MousePicking;
import net.djuke.engine.graphics.components.Skybox;
import net.djuke.engine.graphics.components.camera.Camera3D;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.texture.CubeTexture;
import net.djuke.engine.graphics.components.texture.Texture;
import net.djuke.engine.graphics.gui.DjukeImage;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.engine.utils.tools.EngineControlView;
import net.djuke.engine.utils.tools.JSEngineView;
import net.djuke.engine.utils.tools.SceneView;
import net.djuke.game.Game;
import net.djuke.game.GameData;
import net.djuke.game.mechanics.GObject;
import net.djuke.game.mechanics.World;
import net.djuke.game.mechanics.world.Planet;
import net.djuke.game.mechanics.world.Solar;
import net.djuke.game.mechanics.world.SpaceObject;
import net.djuke.game.mechanics.world.Star;
import org.joml.Vector3f;
import org.liquidengine.legui.component.ImageView;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Panel;
import org.liquidengine.legui.component.optional.align.HorizontalAlign;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.event.MouseDragEvent;
import org.liquidengine.legui.listener.MouseClickEventListener;
import org.liquidengine.legui.listener.MouseDragEventListener;

import java.io.IOException;

/**
 * GameStage
 *
 * @author Vadim
 * @version 1.0 25.11.2017
 */
public class GameStage extends Scene {

    private World world;

    private Camera3D camera;
    // Calculation of camera position
    private Vector3f pos        = new Vector3f(0,0,0);
    private Vector3f up         = new Vector3f(0,1,0);
    private Vector3f cameraPos  = new Vector3f(0, 1, -3);

    private Label objectName;
    private Label typeName;

    private boolean fly = false;
    private Vector3f target;
    private Vector3f start;
    private float lin_alpha = 0;
    private float radius = 2;
    private SpaceObject current;

    // Variables for smooth rotation
    private float alpha;
    private float a_alpha;
    private float delta;
    private float a_delta;

    private float zoom = 10;
    private float a_zoom;

    private DjukeImage image;
    private Panel panel;

    @Override
    public void load() {
        camera = new Camera3D();
        camera.getViewMatrix().translate(0,1,-3);
        setCamera(camera);

        InterfaceFrame gui = new InterfaceFrame();
        GraphicsSettings settings = Graphics.getSettings();
        panel = new Panel();
        panel.setSize(settings.getResolutionWidth(), settings.getResolutionHeight());
        panel.setBackgroundColor(1,0,0,0);
        panel.setBorder(null);
        gui.getContainer().add(panel);

        Package guiResources = GameData.getPackage("game/interface");
        try {
            image = new DjukeImage((Texture) guiResources.get("bottom_menu.txr"));
            ImageView view = new ImageView(image);
            view.setSize(593, 214);
            view.setBorder(null);
            view.setPosition(Graphics.getSettings().getResolutionWidth() / 2f - 593f / 2f, Graphics.getSettings().getResolutionHeight() - 214);
            gui.getContainer().add(view);
        } catch (IOException e) {

        }

        try {
            Package skyboxData = GameData.getPackage("skybox");
            Skybox skybox = new Skybox("Skybox", new CubeTexture(
                    (TextureData) skyboxData.get("right.txr"),
                    (TextureData) skyboxData.get("left.txr"),
                    (TextureData) skyboxData.get("top.txr"),
                    (TextureData) skyboxData.get("bottom.txr"),
                    (TextureData) skyboxData.get("front.txr"),
                    (TextureData) skyboxData.get("back.txr")
            ));
            add(skybox);
        } catch (IOException e) {

        }

        // Получаем текущий мир
        world = Game.getSession().getWorld();
        Solar solar = world.getCurrentSolar();
        add(solar);

        panel.getListenerMap().addListener(MouseDragEvent.class, (MouseDragEventListener)(event) -> {
            a_alpha -= Graphics.getWindow().getMouse().getSpeed().x / 100;
            a_delta -= Graphics.getWindow().getMouse().getSpeed().y / 2000;
        });

        // Так, по умолчанию камера направлена на звезду, но мы как бы ещё не нашли её
        for (GObject object : solar.getObjects()) {
            if (object instanceof Star) {
                current = (SpaceObject)object;
                radius = ((Star) object).getRadius();
            }
        }

        objectName = new Label(current.getName());
        objectName.getTextState().setTextColor(1, 0.75f, 0.5f,0.75f);
        objectName.getTextState().setFontSize(30);
        objectName.setPosition(Graphics.getSettings().getResolutionWidth() / 2f, 25);
        objectName.getTextState().setHorizontalAlign(HorizontalAlign.CENTER);
        gui.getContainer().add(objectName);

        typeName = new Label(Game.getLocalization().getString(current.getType()));
        typeName.getTextState().setTextColor(1, 0.75f, 0.5f,0.75f);
        typeName.setPosition(Graphics.getSettings().getResolutionWidth() / 2f, 50);
        typeName.getTextState().setHorizontalAlign(HorizontalAlign.CENTER);
        gui.getContainer().add(typeName);


        // При кликанье мышки мы должны найти объект, с которым пересёкся курсор и установить его как
        // текущий для просмотра
        panel.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener)(event) -> {
            // Если мы не находимся в режиме перемещения
            if (!fly) {

                float distance = 999;
                boolean intersect = false;
                SpaceObject findObject = null;

                // Ищем ближайший (!) объект, с которым пересекался курсор
                for (GObject object : solar.getObjects()) {
                    if (object instanceof SpaceObject) {
                        Vector3f planetPosition = solar.getObjectPosition(object);
                        MousePicking mousePicking = new MousePicking(this, planetPosition, ((SpaceObject) object).getRadius());
                        // Если есть пересечение с объектом, то всё отлично!
                        if (mousePicking.intersect()) {
                            if (cameraPos.distance(planetPosition) < distance) {
                                distance = cameraPos.distance(planetPosition);
                                intersect = true;
                                findObject = (SpaceObject)object;
                            }
                        }
                    }
                }

                // Если пересечение было найдено, то обновляем наш целевой объект
                if (intersect && findObject != current) {
                    current = findObject;
                    radius = findObject.getRadius() * 4f;
                    fly = true;
                    target = solar.getObjectPosition(findObject);
                    pos = new Vector3f(pos);
                    start = new Vector3f(pos);
                    lin_alpha = 0;

                    // Обновляем информацию об объекте на экране
                    objectName.getTextState().setText(current.getName());
                    typeName.getTextState().setText(Game.getLocalization().getString(current.getType()));
                    if (current instanceof Star) {
                        typeName.getTextState().setTextColor(1, 0.9f, 0.5f,0.75f);
                        objectName.getTextState().setTextColor(1, 0.9f, 0.5f,0.75f);
                    }
                    else {
                        typeName.getTextState().setTextColor(0.5f,0.9f,1,0.75f);
                        objectName.getTextState().setTextColor(0.5f,0.9f,1,0.75f);
                    }
                }
            }
        });



        new JSEngineView(gui);
        SceneView sceneView = new SceneView(this, gui);
        gui.getContainer().add(EngineControlView.getInstance());

        add(gui);
    }

    @Override
    public void update() {
        if (Graphics.getWindow().getMouse().getWheelOffset() != 0) a_zoom = (float)Graphics.getWindow().getMouse().getWheelOffset() / 20f;
        a_delta /= 1.1f;
        a_alpha /= 1.05f;
        a_zoom /= 1.05f;

        alpha += a_alpha;
        delta += a_delta;
        zoom += a_zoom;

        if (delta < -0.99f) delta = -0.99f;
        if (delta > 0.99f) delta = 0.99f;
        if (zoom < radius) zoom = radius;

        if (fly) {
            lin_alpha += 0.025;
            start.lerp(target, lin_alpha, pos);

            if (lin_alpha >= 1) {
                fly = false;
                pos = target;
            }
        }

        EngineControlView.getInstance().update();

        cameraPos.set((float) Math.sin(-alpha / 100) * zoom * (float)Math.cos(delta), (float)Math.sin(delta) * zoom, (float) Math.cos(-alpha / 100) * zoom * (float)Math.cos(delta));
        cameraPos.add(pos);
        camera.getViewMatrix().identity().lookAt(cameraPos, pos, up);
    }

    @Override
    public void delete() {

    }

    private float clip(float alpha) {
        if (alpha < 0) return 0;
        else if (alpha > 1) return 1;
        else return alpha;
    }
}
