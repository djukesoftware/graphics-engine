package net.djuke.game;

import java.util.Locale;

/**
 * GameSettings
 *
 * @author Vadim
 * @version 1.0 17.11.2017
 */
public class GameSettings {

    private String      defaultGameSession;
    private boolean     skipMenu;
    private String      locale;
    private String      email;

    public static GameSettings getDefault() {
        GameSettings settings = new GameSettings();
        settings.setLocale(Locale.ENGLISH.toLanguageTag());
        return settings;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getDefaultGameSession() {
        return defaultGameSession;
    }

    public void setDefaultGameSession(String defaultGameSession) {
        this.defaultGameSession = defaultGameSession;
    }

    public boolean isSkipMenu() {
        return skipMenu;
    }

    public void setSkipMenu(boolean skipMenu) {
        this.skipMenu = skipMenu;
    }
}
