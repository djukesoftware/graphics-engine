package net.djuke.engine.network;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.*;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Gravatar {

    private String email;

    private String fullname;
    private String username;
    private String aboutme;
    private String location;
    private byte[] photo;

    public Gravatar(String email) {
        this.email = email;
        // Get hash of email
        String hash = md5Hex(email);

        try {
            // Loading photo from Gravatar
            URL url = new URL("https://gravatar.com/avatar/" + hash + "?s=256");

            // Creating input stream for read data from connection
            BufferedInputStream bis = new BufferedInputStream(url.openStream());
            // Creating byte output stream for collect all data
            ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
            // Read data to the buffer

            byte[] buffer = new byte[1024]; int count;
            while ((count = bis.read(buffer)) != -1) {
                byteArray.write(buffer, 0, count);
            }
            byteArray.close();
            bis.close();

            // Convert outputStream to byte array
            photo = byteArray.toByteArray();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Loading all information about account
        StringBuilder string = new StringBuilder();
        try {
            URL url = new URL("https://gravatar.com/" + hash + ".json");
            Scanner scanner = new Scanner(new BufferedInputStream(url.openStream()));
            while (scanner.hasNextLine()) {
                string.append(scanner.nextLine());
            }
            scanner.close();

            // Parse json string with information about account
            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(string.toString()).getAsJsonObject();
            JsonObject accountInformation = object.get("entry").getAsJsonArray().get(0).getAsJsonObject();
            fullname = getInfo("displayName", accountInformation);
            username = getInfo("preferredUsername", accountInformation);
            aboutme = getInfo("aboutMe", accountInformation);
            location = getInfo("currentLocation", accountInformation);

        } catch (IOException e) {
            // TODO: Write logger
            System.out.println("User not found");
        }

    }

    private String getInfo(String key, JsonObject object) {
        try {
            return object.get(key).getAsString();
        } catch (NullPointerException e) {
            System.out.println(key + " not found.");
            return "";
        }
    }

    public String getLocation() {
        return location;
    }

    public String getAboutme() {
        return aboutme;
    }

    public String getFullname() {
        return fullname;
    }

    public String getUsername() {
        return username;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getEmail() {
        return email;
    }

    /**
     * Delete photo from RAM
     */
    public void clearPhoto() {
        photo = null;
    }

    private static String md5Hex(String message) {
        try {
            MessageDigest md =
                    MessageDigest.getInstance("MD5");
            return hex (md.digest(message.getBytes("CP1252")));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            // TODO: write logger
        }
        return null;
    }

    private static String hex(byte[] array) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i]
                    & 0xFF) | 0x100).substring(1,3));
        }
        return sb.toString();
    }
}

