package net.djuke.engine.network;

import java.security.KeyPair;

/**
 * Network
 *
 * @author Vadim
 * @version 1.0 27.06.2017
 */
public class Network {

    private static Network instance;

    private String username;
    private String password;
    private KeyPair keyPair;

    public static Network getInstance() {
        if (instance == null) instance = new Network();
        return instance;
    }

    // don't have connection with server
    // not authorized
    // authorized

    /**
     * State of current connection
     */
    private interface State {
        String send(String cmd);
        String getUsername();
        String getPassword();
        void setUsername();
        void setPassword();
    }
}
