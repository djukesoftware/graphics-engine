package net.djuke.engine.input;

import net.djuke.engine.graphics.Graphics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * InputManager
 *
 * @author Vadim
 * @version 1.0 27.07.2017
 */
public class InputManager {

    private static InputManager instance;

    private Logger logger = LogManager.getLogger("Input subsystem");
    private Mouse defaultMouse;
    private Keyboard defaultKeyboard;

    private InputManager() {}

    public void startup() {
        logger.info("Get mouse from the Render thread");
        defaultMouse = Graphics.getWindow().getMouse();
        logger.info("Get keyboard from the Render thread");
        defaultKeyboard = Graphics.getWindow().getKeyboard();
    }

    /**
     * Return default mouse
     * @return default mouse or null if a mouse is not created
     */
    public Mouse getMouse() {
        return defaultMouse;
    }

    /**
     * Return default keyboard
     * @return default keyboard or null if a keyboard is not created
     */
    public Keyboard getKeyboard() {
        return defaultKeyboard;
    }

    public void shutdown() {

    }

    public static synchronized InputManager getInstance() {
        if (instance == null) instance = new InputManager();
        return instance;
    }

}
