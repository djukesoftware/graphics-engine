package net.djuke.engine.input;

import org.joml.Vector2f;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Mouse
 *
 * @author Vadim
 * @version 1.0 24.05.2017
 */
public interface Mouse {

    int BUTTON_RIGHT = GLFW_MOUSE_BUTTON_RIGHT;
    int BUTTON_LEFT = GLFW_MOUSE_BUTTON_LEFT;

    int PUSH = GLFW_PRESS;
    int RELEASE = GLFW_RELEASE;

    Vector2f getPosition();
    Vector2f getSpeed();
    double getWheelOffset();
    boolean buttonIs(int button, int instance);

}
