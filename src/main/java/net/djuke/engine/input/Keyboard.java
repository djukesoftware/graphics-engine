package net.djuke.engine.input;

/**
 * Keyboard
 *
 * @author Vadim
 * @version 1.0 24.05.2017
 */
public interface Keyboard {

    void addTextInputCallback(Callback callback);

    interface Callback {
        void update(String lastChar);
    }

}
