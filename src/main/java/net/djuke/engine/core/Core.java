package net.djuke.engine.core;

import net.djuke.engine.data.Data;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.input.InputManager;
import net.djuke.engine.plugin.PluginManager;
import net.djuke.engine.script.ScriptManager;
import net.djuke.game.Game;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

/**
 * Core - ядро графического движка, отвечающее за хранение глобальных настроек и запуск остальных подсистем.
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class Core {

    private static final Logger     logger = LogManager.getLogger("Engine core");
    private static EngineSettings   settings;
    private static List<String>     arguments = new ArrayList<>();

    public static void main(String... args) {
        Thread.currentThread().setName("Core");
        arguments.addAll(Arrays.asList(args));

        // Create package for storage engine data
        logger.info("Starting data subsystem");
        Data.startup();

        // Load engine settings
        logger.info("Loading engine settings");
        settings = loadEngineSettings();
        logger.info("System language: " + Locale.getDefault().getDisplayLanguage());
        Locale.setDefault(Locale.forLanguageTag(settings.getLocale()));
        logger.info("Engine language: " + Locale.getDefault().getDisplayLanguage());

        // Getting resource bundle with localization
        ResourceBundle localization = ResourceBundle.getBundle("engine");

        // After that, start graphics subsystem
        logger.info(localization.getString("startingGraphicsSubsystem"));
        Graphics.startup();

        // Start input manager
        logger.info("Start input subsystem");
        InputManager.getInstance().startup();

        // After start up of all others subsystem, we start script manager
        // for extensions
        logger.info("Start script subsystem");
        ScriptManager.getInstance().startup();

        // After all prepares, start all plugins
        if (!arguments.contains("disablePlugins")) {
            // Here we start new Plugin threads
            logger.info("Start plugin subsystem");
            PluginManager.getInstance().startup();
        }

        logger.info("Starting project");
        Game.startup();

        if (!arguments.contains("disablePlugins")) {
            logger.info("Shutdown plugin manager");
            PluginManager.getInstance().shutdown();
        }

        logger.info("Shutdown script subsystem");
        ScriptManager.getInstance().shutdown();

        logger.info("Shutdown input subsystem");
        InputManager.getInstance().shutdown();

        logger.info("Shutdown graphics subsystem");
        Graphics.shutdown();

        logger.info("Shutdown data subsystem");
        Data.shutdown();

        logger.info("Saving engine settings");
        saveEngineSettings(settings);
    }

    public static EngineSettings getSettings() {
        return settings;
    }

    private static void saveEngineSettings(EngineSettings settings) {
        try {
            Data.saveObject("resources/configuration/engine.json", settings, Data.OS_FILE_SYSTEM);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private static EngineSettings loadEngineSettings() {
        EngineSettings settings;
        try {
            settings = (EngineSettings) Data.loadObject("resources/configuration/engine.json", Data.OS_FILE_SYSTEM);
        } catch (IOException e) {
            logger.error(e.getMessage());
            settings = EngineSettings.getDefault();
        }
        // Set default locale
        return settings;
    }

    public static double getTime() {
        return glfwGetTime();
    }

    public static List<String> getProgramArguments() {
        return arguments;
    }

}
