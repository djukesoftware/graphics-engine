package net.djuke.engine.core;

import net.djuke.engine.graphics.GraphicsSettings;
import org.joml.Vector2f;

import java.awt.*;
import java.util.Locale;

/**
 * EngineSettings - хранит настройки графического движка
 *
 * @author Vadim
 * @version 1.0 24.11.2017
 */
public class EngineSettings {

    private String[]            args;
    private String              locale;
    private GraphicsSettings    graphics;

    public EngineSettings() {

    }

    public static EngineSettings getDefault() {
        EngineSettings settings = new EngineSettings();
        settings.setLocale(Locale.getDefault().toLanguageTag());

        GraphicsSettings graphics = new GraphicsSettings();
        graphics.setBrightness(0);
        graphics.setContrast(1);
        graphics.setGamma(1);
        graphics.setFullscreen(true);
        graphics.setMsaa(false);
        Dimension resolution = Toolkit.getDefaultToolkit().getScreenSize();
        graphics.setResolution(new Vector2f((float)resolution.getWidth(), (float)resolution.getHeight()));
        settings.setGraphics(graphics);
        return settings;
    }

    public GraphicsSettings getGraphics() {
        return graphics;
    }

    public void setGraphics(GraphicsSettings graphics) {
        this.graphics = graphics;
    }

    public String[] getArgs() {
        return args;
    }

    /**
     * Установить параметры запуска движка
     * @param args параметры запуска движка
     */
    public void setArgs(String[] args) {
        this.args = args;
    }

    /**
     * Get localeTag of engine locale
     * @see Locale
     * @return localeTag as String
     */
    public String getLocale() {
        return locale;
    }

    /**
     * Установить текущую локаль движка по средствам передачи localTag
     * @see Locale
     * @param locale localeTag as String
     */
    public void setLocale(String locale) {
        this.locale = locale;
    }
}
