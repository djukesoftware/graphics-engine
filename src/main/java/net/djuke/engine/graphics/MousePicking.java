package net.djuke.engine.graphics;

import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.camera.Camera;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.input.Mouse;
import org.joml.*;

/**
 * MouserPicking
 *
 * @author Vadim
 * @version 1.0 06.10.2017
 */
public class MousePicking {

    // Environment
    private Scene scene;
    private Model model;

    // Default vectors for calculations
    private final Vector4f nullVector = new Vector4f(0,0,0,1);
    private final Vector4f oneVector = new Vector4f(0,1,0,1);

    private Vector4f cameraPosition  = new Vector4f();
    private Vector4f modelPosition   = new Vector4f();
    private Vector4f size            = new Vector4f(); // vector for calculating size of sphere
    private Vector4f direction       = new Vector4f(); // vector for calculations

    private Spheref     sphere   ; // sphere for intersection
    private Rayf        rayf     = new Rayf(); // direction for intersection
    private Vector2f    result   = new Vector2f(); // result of something

    // Matrices for calculations
    private Matrix4f invProjection  = new Matrix4f();
    private Matrix4f invView        = new Matrix4f();

    public MousePicking(Scene scene, Model model, float size) {
        // Create sphere for intersection detection
        sphere = new Spheref(0, 0, 0, size);
        // model for intersection
        this.model = model;
        // Get current scene
        this.scene = scene;
    }

    public MousePicking(Scene scene, Vector3f position, float size) {
        this.scene = scene;
        sphere = new Spheref(0,0,0,size);
        this.modelPosition = new Vector4f(position, 0);
    }

    public boolean intersect() {
        // Get Camera of this scene
        Camera camera = scene.getCamera();
        camera.getProjectionMatrix().invert(invProjection);
        camera.getViewMatrix().invert(invView);  // invert game.matrix of view

        // Get coordinates of mouse
        Mouse mouse = Graphics.getWindow().getMouse();
        direction.set(
                mouse.getPosition().x() / (Graphics.getSettings().getResolutionWidth() / 2) - 1,
                (Graphics.getSettings().getResolutionHeight() - mouse.getPosition().y()) / (Graphics.getSettings().getResolutionHeight() / 2) - 1,
                -1,
                1);

        // Calculate direction in world coordinates
        direction.mul(invProjection);
        direction.set(direction.x, direction.y, -1, 0);

        // To world coordinates
        direction.mul(invView);
        direction.normalize();

        // Calculate camera position
        nullVector.mul(invView, cameraPosition);

        // Create direction
        rayf.oX = cameraPosition.x;
        rayf.oY = cameraPosition.y;
        rayf.oZ = cameraPosition.z;
        rayf.dX = direction.x;
        rayf.dY = direction.y;
        rayf.dZ = direction.z;

        // Update parameters of sphere
        if (model != null) {
            nullVector.mul(model.getModelMatrix(), modelPosition);
            oneVector.mul(model.getModelMatrix(), size);
        }
        sphere.x = modelPosition.x;
        sphere.y = modelPosition.y;
        sphere.z = modelPosition.z;
        if (model != null) sphere.r = size.y();

        return Intersectionf.intersectRaySphere(rayf, sphere, result);
    }
}
