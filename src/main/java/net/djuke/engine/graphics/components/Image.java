package net.djuke.engine.graphics.components;

import java.awt.image.BufferedImage;

/**
 * Image - класс для хранения изображений
 *
 * @author Vadim
 * @version 1.0 20.11.2017
 */
public class Image {

    private BufferedImage image;

    public Image(int width, int height, int type) {
        image = new BufferedImage(width, height, type);
    }

    public BufferedImage getImage() {
        return image;
    }
}
