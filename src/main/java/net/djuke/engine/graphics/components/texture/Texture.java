package net.djuke.engine.graphics.components.texture;

import net.djuke.engine.data.beans.TextureData;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.Image;
import org.lwjgl.BufferUtils;

import java.awt.image.BufferedImage;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE0;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.opengl.GL30.glGenerateMipmap;
import static org.lwjgl.opengl.GL42.glTexStorage2D;

/**
 * Texture - class for contain textures
 */
public class Texture {

    int id;
    int width;
    int height;
    protected String name = "id"; // what the fuck?

    protected Texture() {

    }

    public Texture(TextureData data) {
        load(data);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private void load(TextureData data) {
        id = glGenTextures();

        glBindTexture(GL_TEXTURE_2D, id);
        // Create empty buffer for storage id
        glTexStorage2D(GL_TEXTURE_2D, Graphics.getSettings().getMipmapLevels(), GL_RGBA8, data.getWidth(), data.getHeight());
        // Write data into buffer
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, data.getWidth(),data.getHeight(),
                GL_RGBA, GL_UNSIGNED_BYTE, data.getBuffer());

        // Enable trilinear filtering (when we use interpolation between mip levels)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        // Generating mipmaps of current id
        glGenerateMipmap(GL_TEXTURE_2D);

        // Unbind current id
        glBindTexture(GL_TEXTURE_2D, 0);

        width = data.getWidth();
        height = data.getHeight();
    }

    public void bind(int id) {
        glActiveTexture(GL_TEXTURE0 + id);
        glBindTexture(GL_TEXTURE_2D, this.id);
    }

    public Image convertToImage() {
        Image image = new Image(width, height, BufferedImage.TYPE_INT_RGB);
        ByteBuffer buffer = BufferUtils.createByteBuffer(width * height * 4);
        bind(0);
        // Получаем изображение из текстуры
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        glBindTexture(GL_TEXTURE_2D, 0);

        // Теперь нам надо данные из буфера аккуратно поместить в изображение
        int pixelSize = 4;
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                int pos = (x + (width * y)) * pixelSize;
                int r = buffer.get(pos) & 0xFF;
                int g = buffer.get(pos + 1) & 0xFF;
                int b = buffer.get(pos + 2) & 0xFF;
                image.getImage().setRGB(x, height - (y+1), (0xFF << 24) | (r << 16) | (g << 8) | b);
            }
        }
        return image;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getId() {
        return id;
    }

    public void delete() {
        glDeleteTextures(id);
    }
}

