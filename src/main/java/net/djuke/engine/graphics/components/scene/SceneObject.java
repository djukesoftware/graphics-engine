package net.djuke.engine.graphics.components.scene;

/**
 * SceneObject - interface for all objects on scene
 *
 * @author Vadim
 * @version 1.0 12.09.2017
 */
public interface SceneObject {

    String getName();

}
