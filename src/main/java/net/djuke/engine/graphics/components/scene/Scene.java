package net.djuke.engine.graphics.components.scene;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.camera.Camera;
import net.djuke.engine.graphics.renderers.Renderer;

import java.util.ArrayList;
import java.util.List;

/**
 * Scene - scene for rendering inside one window
 * Created by azusa on 3/9/17.
 */
public abstract class Scene {

    // Context of current scene
    private boolean isLoad = false;

    // Camera of scene
    private Camera camera;

    // All objects
    private List<SceneObject> objects = new ArrayList<>();

    /**
     * Loading data for current scene
     */
    public abstract void load();

    /**
     * Update instance of scene
     */
    public abstract void update();

    /**
     * Delete data of scene
     */
    public abstract void delete();

    /**
     * Render current scene
     */
    public final void render() {
        // If current scene was not load
        if (!isLoad) {
            load();
            isLoad = true;
        }

        if (camera !=null) camera.update();

        update();

        for (Object model : objects) {

            Renderer renderer = Graphics.getRenderer(model.getClass());
            if (renderer != null) renderer.render(this, model);

        }
    }

    /**
     * Adds object to render state
     * @param object object for rendering
     */
    public final void add(SceneObject object) {
        objects.add(object);
    }

    /**
     * Remove object from this scene
     * @param object object on scene
     */
    public final void remove(SceneObject object) { objects.add(object); }

    public final List<SceneObject> getObjects() {
        return objects;
    }

    public final void setCamera(Camera camera) {
        this.camera = camera;
        if (!objects.contains(camera)) objects.add(camera);
    }

    public final Camera getCamera() {
        return camera;
    }

}
