package net.djuke.engine.graphics.components.camera;

import net.djuke.engine.graphics.Graphics;
import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

/**
 * Camera3D
 *
 * @author Vadim
 * @version 1.0 17.07.2017
 */
public class Camera3D implements Camera {

    private String name = Camera3D.class.getSimpleName();

    private Matrix4f projectionMatrix = new Matrix4f();
    private Matrix4f viewMatrix = new Matrix4f();

    private FloatBuffer projectionMatrixBuffer;
    private FloatBuffer viewMatrixBuffer;

    private float fov = 60f;
    private float near = 0.01f;
    private float far = 600f;

    /**
     * Create default camera with fov as 90 degrees, near as 0.01 and far as 100.
     */
    public Camera3D() {
        createBuffers();
        updateViewMatrix();
        updateProjectionMatrix();
    }

    public Camera3D(String name) {
        createBuffers();
        updateViewMatrix();
        updateProjectionMatrix();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Creates camera with custom parameters
     * @param fov field of view
     * @param far max distance of rendering
     * @param near near distance of rendering
     */
    public Camera3D(float fov, float far, float near) {
        this.fov = fov;
        this.far = far;
        this.near = near;
        createBuffers();
        updateViewMatrix();
        updateProjectionMatrix();
    }

    private void createBuffers() {
        projectionMatrixBuffer = BufferUtils.createFloatBuffer(16);
        viewMatrixBuffer = BufferUtils.createFloatBuffer(16);
    }

    private void updateProjectionMatrix() {
        float width = Graphics.getSettings().getResolution().x();
        float height = Graphics.getSettings().getResolution().y();
        float aspect = width / height;
        // Update projection game.matrix
        projectionMatrix = projectionMatrix.setPerspective((float)Math.toRadians(fov), aspect, near, far);
        // update game.matrix buffer
        projectionMatrix.get(projectionMatrixBuffer);
    }

    private void updateViewMatrix() {
        // update game.matrix buffer
        viewMatrix.get(viewMatrixBuffer);
    }

    public void setFov(float fov) {
        this.fov = fov;
    }

    public void setNear(float near) {
        this.near = near;
    }

    public void setFar(float far) {
        this.far = far;
    }

    public float getFov() {
        return fov;
    }

    public float getNear() {
        return near;
    }

    public float getFar() {
        return far;
    }

    public void update() {
        updateProjectionMatrix();
        updateViewMatrix();
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public FloatBuffer getViewMatrixBuffer() {
        return viewMatrixBuffer;
    }

    public FloatBuffer getProjectionMatrixBuffer() {
        return projectionMatrixBuffer;
    }
}
