package net.djuke.engine.graphics.components;

import net.djuke.engine.graphics.components.scene.SceneObject;

/**
 * Light
 *
 * @author Vadim
 * @version 1.0 03.10.2017
 */
public final class Light implements SceneObject {

    private final String name;

    public Light(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
