package net.djuke.engine.graphics.components.camera;

import net.djuke.engine.graphics.components.scene.SceneObject;
import org.joml.Matrix4f;

import java.nio.FloatBuffer;

/**
 * Interface for camera
 * Created by azusa on 3/16/17.
 */
public interface Camera extends SceneObject {

    FloatBuffer getViewMatrixBuffer();
    FloatBuffer getProjectionMatrixBuffer();

    Matrix4f getViewMatrix();
    Matrix4f getProjectionMatrix();

    void update();

}
