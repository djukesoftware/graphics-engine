package net.djuke.engine.graphics.components;

import net.djuke.engine.core.Core;
import net.djuke.engine.data.beans.ShaderSource;
import net.djuke.engine.data.beans.ShaderSources;
import net.djuke.engine.graphics.Graphics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.GL_GEOMETRY_SHADER;
import static org.lwjgl.opengl.GL40.GL_TESS_CONTROL_SHADER;
import static org.lwjgl.opengl.GL40.GL_TESS_EVALUATION_SHADER;
import static org.lwjgl.opengl.GL43.GL_COMPUTE_SHADER;

/**
 * Class for shaders
 * Created by azusa on 3/18/17.
 */
public final class Shader {

    private final Logger logger = LogManager.getLogger(Shader.class);
    private final int shaderProgram;

    /**
     * Create shader program from array of shader sources
     * @param sources sources of shader
     */
    public Shader(ShaderSources sources) {

        // Create shader program
        shaderProgram = glCreateProgram();

        // Loading sources of every shader
        int[] shaderSources = new int[6];
        for (ShaderSource source : sources.getSources()) {
            if (source != null) {
                switch (source.getType()) {
                    case "fragmentShader":
                        shaderSources[0] = loadFragmentShader(source.getSource());
                        break;
                    case "vertexShader":
                        shaderSources[1] = loadVertexShader(source.getSource());
                        break;
                    case "tessellationShader":
                        shaderSources[2] = loadTessellationShader(source.getSource());
                        break;
                    case "tessellationEvaluationShader":
                        shaderSources[3] = loadTessellationEvaluationShader(source.getSource());
                        break;
                    case "geometryShader":
                        shaderSources[4] = loadGeometryShader(source.getSource());
                        break;
                    case "computeShader":
                        shaderSources[5] = loadComputeShader(source.getSource());
                        break;
                    default:
                        logger.warn("Unknown type of shader \"" + source.getType() + "\". This shader was ignored.");
                }
            } else logger.warn("One of the shaders is null");
        }

        // attach every shader to shader program
        for (int shaderSource : shaderSources)
            if (shaderSource != 0) attachShader(shaderSource);

        // Линкуем прикреплённые шейдеры в одну шейдерную программу
        glLinkProgram(shaderProgram);

        // detach all shader sources from shader program
        for (int shaderSource : shaderSources)
            if (shaderSource != 0) glDetachShader(shaderProgram, shaderSource);

        // Проверяем, может ли программа исполняться в данном OpenGL контексте
        glValidateProgram(shaderProgram);
        // Null sources because we don't need them
        sources = null;
    }

    private int loadFragmentShader(StringBuilder source) {
        int id = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(id, source);
        glCompileShader(id);
        printShaderLog(id);
        return id;
    }

    private int loadTessellationShader(StringBuilder source) {
        int id = glCreateShader(GL_TESS_CONTROL_SHADER);

        glShaderSource(id, source);
        glCompileShader(id);
        printShaderLog(id);
        return id;
    }

    private int loadTessellationEvaluationShader(StringBuilder source) {
        int id = glCreateShader(GL_TESS_EVALUATION_SHADER);

        glShaderSource(id, source);
        glCompileShader(id);
        printShaderLog(id);
        return id;
    }

    private int loadGeometryShader(StringBuilder source) {
        int id = glCreateShader(GL_GEOMETRY_SHADER);

        glShaderSource(id, source);
        glCompileShader(id);
        printShaderLog(id);
        return id;
    }

    private int loadVertexShader(StringBuilder source) {
        int id = glCreateShader(GL_VERTEX_SHADER);

        glShaderSource(id, source);
        glCompileShader(id);
        printShaderLog(id);
        return id;
    }

    private int loadComputeShader(StringBuilder source) {
        int id = glCreateShader(GL_COMPUTE_SHADER);

        glShaderSource(id, source);
        glCompileShader(id);
        printShaderLog(id);
        return id;
    }

    private void attachShader(int id) {
        if (id != -1) glAttachShader(shaderProgram, id);
    }

    public void bind() {
        // Use current shader program
        glUseProgram(shaderProgram);

        // Send information about graphics settings to current shader
        sendVector2("display", Graphics.getSettings().getResolution());
        sendFloat("time", (float)Core.getTime());
    }

    public void unbind() {
        glUseProgram(0); // bind empty shader program
    }

    public int getShaderProgram() {
        return shaderProgram;
    }

    /**
     * Delete opengl shader program
     */
    public void delete() {
        glUseProgram(0);
        glDeleteProgram(shaderProgram);
    }

    public void sendFloat(String name, float value) {
        glUniform1f(glGetUniformLocation(shaderProgram, name), value);
    }

    public void sendVector2(String name, Vector2f vector) {
        glUniform2f(glGetUniformLocation(shaderProgram, name), vector.x(), vector.y());
    }

    public void sendVector3(String name, Vector3f vector) {
        glUniform3f(glGetUniformLocation(shaderProgram, name), vector.x(), vector.y(), vector.z());
    }

    public void sendVector4(String name, Vector4f vector) {
        glUniform4f(glGetUniformLocation(shaderProgram, name), vector.x(), vector.y(), vector.z(), vector.w());
    }

    public void sendInteger(String name, int value) {
        glUniform1i(glGetUniformLocation(shaderProgram, name), value);
    }

    public void sendMatrix4f(String name, FloatBuffer buffer) {
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, name), false, buffer);
    }

    public void sendBoolean(String name, boolean value) {
        glUniform1i(glGetUniformLocation(shaderProgram, name), value?1:0);
    }

    public void sendIntegerArray(String name, int[] array) {
        glUniform1iv(glGetUniformLocation(shaderProgram, name), array);
    }

    public void sendTexture(String name, int id) {
        glUniform1i(glGetUniformLocation(shaderProgram, name), id);
    }

    private void printShaderLog(int shader) {
        String log = glGetShaderInfoLog(shader);
        if (!log.equals("")) logger.info(log);
    }

}
