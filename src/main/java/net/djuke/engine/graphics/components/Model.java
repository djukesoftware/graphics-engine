package net.djuke.engine.graphics.components;

import net.djuke.engine.graphics.components.scene.SceneObject;
import net.djuke.engine.graphics.components.texture.Texture;
import org.joml.Matrix4f;

import java.util.ArrayList;
import java.util.List;

/**
 * Model - represents of any 3D model with shader, textures, mesh and others
 *
 * @author Vadim
 * @version 1.0 15.09.2017
 */
public class Model implements SceneObject {

    // Information for scene
    private String name = "";

    // Information of model
    private Mesh mesh;
    private Shader shader;
    private Matrix4f modelMatrix    = new Matrix4f();
    private List<Texture> textures  = new ArrayList<>();
    private boolean debug = false;

    public boolean isDebug() {
        return debug;
    }

    public void setDebug(boolean debug) {
        this.debug = debug;
    }

    public Model(String name) {
        this.name = name;
    }

    public Matrix4f getModelMatrix() {
        return modelMatrix;
    }

    public void setModelMatrix(Matrix4f modelMatrix) {
        this.modelMatrix = modelMatrix;
    }

    public String getName() {
        return name;
    }

    public Mesh getMesh() {
        return mesh;
    }

    public void setMesh(Mesh mesh) {
        this.mesh = mesh;
    }

    public List<Texture> getTextures() {
        return textures;
    }

    public void setTextures(List<Texture> textures) {
        this.textures = textures;
    }

    public void addTexture(Texture texture) {
        textures.add(texture);
    }

    public void removeTexture(Texture texture) {
        textures.remove(texture);
    }

    public Shader getShader() {
        return shader;
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }
}
