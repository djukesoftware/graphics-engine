package net.djuke.engine.graphics.components.texture;

import net.djuke.engine.data.beans.TextureData;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;

/**
 * CubeTexture
 *
 * @author Vadim
 * @version 1.0 01.09.2017
 */
public class CubeTexture extends Texture {

    private String name;
    private int id;

    public CubeTexture(TextureData... textures) {
        load(textures);
    }

    private void load(TextureData... textures) {
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_CUBE_MAP, id);

        // Loading all textures to one texture
        int i = 0;
        for (TextureData texture : textures) {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGBA, texture.getWidth(), texture.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.getBuffer());
            i++;
        }

        // Enable trilinear filtering (when we use interpolation between mip levels)
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }

    public void bind(int id) {
        glActiveTexture(GL_TEXTURE0 + id);
        glBindTexture(GL_TEXTURE_CUBE_MAP, this.id);
    }

    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    public void delete() {
        glDeleteTextures(id);
    }
}
