package net.djuke.engine.graphics.components;

import net.djuke.engine.data.beans.ModelData;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.*;

/**
 * Model - represents ready for rendering model, created from ModelData.
 * @see ModelData
 *
 * Created by azusa on 3/17/17.
 */
public class Mesh {

    // Names of vao and buffers
    private int vao;
    private int vertexBufferId      = -1;
    private int normalBufferId      = -1;
    private int textureBufferId     = -1;
    private int tangentBufferId     = -1;
    private int bitangentBufferId   = -1;
    // Count of vertices for rendering
    private int vertexCount;

    /**
     * Create new model from model data
     * @param data data of model, loaded from model file
     */
    public Mesh(ModelData data) {
        loadModelData(data);
    }

    /**
     * Load mesh and other data of model (UV coordinates and normals) to graphics memory
     * @param data data of model
     */
    private void loadModelData(ModelData data) {
        // Create vertex array to contain two buffers of data
        vao = glGenVertexArrays();
        // Bind current vertex array
        glBindVertexArray(vao);

        if (data.getVertexBuffer() != null) {
            // Set count of vertices
            vertexCount = data.getVertexCount();

            // Creating vertex buffer
            vertexBufferId = glGenBuffers();
            // Bind vertex buffer and creating float buffer
            glBindBuffer(GL_ARRAY_BUFFER, vertexBufferId);
            glBufferData(GL_ARRAY_BUFFER, data.getVertexBuffer(), GL_STATIC_DRAW);
            glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(0);
        }

        // Creating buffer for normals
        if (data.getNormalBuffer() != null) {
            normalBufferId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, normalBufferId);
            glBufferData(GL_ARRAY_BUFFER, data.getNormalBuffer(), GL_STATIC_DRAW);
            glVertexAttribPointer(1, 3, GL_FLOAT, true, 0, 0);
            glEnableVertexAttribArray(1);
        }

        // Buffer for coordinates of texture
        if (data.getTextureBuffer() != null) {
            textureBufferId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, textureBufferId);
            glBufferData(GL_ARRAY_BUFFER, data.getTextureBuffer(), GL_STATIC_DRAW);
            glVertexAttribPointer(2, 2, GL_FLOAT, false, 0, 0);
            glEnableVertexAttribArray(2);
        }

        if (data.getTangentBuffer() != null) {
            tangentBufferId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, tangentBufferId);
            glBufferData(GL_ARRAY_BUFFER, data.getTangentBuffer(), GL_STATIC_DRAW);
            glVertexAttribPointer(3, 3, GL_FLOAT, true, 0, 0);
            glEnableVertexAttribArray(3);
        }

        if (data.getBitangentBuffer() != null) {
            bitangentBufferId = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, bitangentBufferId);
            glBufferData(GL_ARRAY_BUFFER, data.getBitangentBuffer(), GL_STATIC_DRAW);
            glVertexAttribPointer(4, 3, GL_FLOAT, true, 0, 0);
            glEnableVertexAttribArray(4);
        }

        // unbind current vertex array
        glBindVertexArray(0);
    }

    /**
     * Render current model
     */
    public void render() {
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, vertexCount); // set type of primitivies to render
    }

    /**
     * Delete data of model
     */
    public void delete() {
        // delete vbos
        if (vertexBufferId != -1) glDeleteBuffers(vertexBufferId);
        if (normalBufferId != -1) glDeleteBuffers(normalBufferId);
        if (textureBufferId != -1) glDeleteBuffers(textureBufferId);
        if (tangentBufferId != -1) glDeleteBuffers(tangentBufferId);
        if (bitangentBufferId != -1) glDeleteBuffers(bitangentBufferId);
        // delete one vao
        glDeleteVertexArrays(vao);
    }

}
