package net.djuke.engine.graphics.components.scene;

import net.djuke.engine.graphics.components.Light;
import net.djuke.engine.graphics.components.Skybox;

import java.util.ArrayList;
import java.util.List;

/**
 * SceneEnvironment
 *
 * @author Vadim
 * @version 1.0 03.10.2017
 */
public class SceneEnvironment {

    private List<Light> lights = new ArrayList<>();
    private Skybox skybox;

    public Skybox getSkybox() {
        return skybox;
    }

    public List<Light> getLights() {
        return lights;
    }

    void addLight(Light light) {
        lights.add(light);
    }

    void setSkybox(Skybox skybox) {
        this.skybox = skybox;
    }
}
