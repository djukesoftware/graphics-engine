package net.djuke.engine.graphics.components.texture;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.GraphicsSettings;
import net.djuke.engine.graphics.components.scene.Scene;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.glDrawBuffers;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL32.glFramebufferTexture;

/**
 * Frame - отвечает за отрисовку одного конкретного кадра. За каждым кадром закреплена сцена
 * , которая рисуется внутри этого кадра. Каждый кадр отрисовывается в определённую текстуру.
 *
 * @author Vadim
 * @version 1.0 04.10.2017
 */
public class Frame extends Texture {

    private Logger logger = LogManager.getLogger(Frame.class);
    private Scene scene;
    private int framebufferName;
    private boolean smooth = false;
    private boolean load = false;

    /**
     * Creates empty frame without scene to render
     * @param width width of frame
     * @param height height of frame
     */
    public Frame(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Create frame for rendering scene
     * @param scene scene to render inside frame
     * @param width width of frame
     * @param height height of frame
     */
    public Frame(Scene scene, int width, int height) {
        this.scene = scene;
        this.width = width;
        this.height = height;
    }

    /**
     * Creates frame for rendering scene
     * @param scene scene to render inside frame
     * @param width width of frame
     * @param height height of frame
     * @param smooth enable smooth of texture
     */
    public Frame(Scene scene, int width, int height, boolean smooth) {
        this.smooth = smooth;
        this.scene = scene;
        this.width = width;
        this.height = height;
    }

    private void createTexture() {
        // Create framebuffer
        framebufferName = glGenFramebuffers();
        glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);
        name = "Frame" + framebufferName;

        // Create empty texture
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, smooth ? GL_LINEAR : GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, smooth ? GL_LINEAR : GL_NEAREST);

        // Create depth buffer for texture
        int depthBuffer = glGenRenderbuffers();
        glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

        // Attach depth buffer to texture
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, id, 0);
        glDrawBuffers(GL_COLOR_ATTACHMENT0);

        // Check status of framebuffer
        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) logger.error("Frame buffer not created");

        // Unbind framebuffer
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        // Unbind current id
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Scene getScene() {
        return scene;
    }

    public void delete() {
        glDeleteTextures(this.id);
    }

    /**
     * Render scene to current frame
     */
    public void render() {
        // If frame not created
        if (!load) {
            // create new framebuffer with texture
            createTexture();
            load = true;
        }

        // Save old resolution
        int oldWidth = (int)Graphics.getSettings().getResolutionWidth();
        int oldHeight = (int)Graphics.getSettings().getResolutionHeight();
        // Switch to new resolution
        Graphics.getSettings().getResolution().set(width, height);

        // Rendering to texture
        glBindFramebuffer(GL_FRAMEBUFFER, framebufferName);
        glViewport(0,0,width, height);

        // Set alpha channel
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Render this scene
        scene.render();

        // Switch to old resolution
        Graphics.getSettings().getResolution().set(oldWidth, oldHeight);
        // Go back to default graphics settings and rendering to screen
        // glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0,0, (int) Graphics.getSettings().getResolutionWidth(), (int) Graphics.getSettings().getResolutionHeight());
    }
}
