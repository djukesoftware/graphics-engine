package net.djuke.engine.graphics.components.camera;

import net.djuke.engine.graphics.Graphics;
import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

/**
 * Camera2D
 *
 * @author Vadim
 * @version 1.0 17.07.2017
 */
public class Camera2D implements Camera {

    private String name = Camera2D.class.getSimpleName();

    private Matrix4f projectionMatrix = new Matrix4f();
    private Matrix4f viewMatrix = new Matrix4f();

    private FloatBuffer viewMatrixBuffer;
    private FloatBuffer projectionMatrixBuffer;

    public Camera2D() {
        createBuffers();
        updateProjectionMatrix();
    }

    public Camera2D(String name) {
        createBuffers();
        updateProjectionMatrix();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public FloatBuffer getViewMatrixBuffer() {
        return viewMatrixBuffer;
    }

    public FloatBuffer getProjectionMatrixBuffer() {
        return projectionMatrixBuffer;
    }

    public Matrix4f getProjectionMatrix() {
        return projectionMatrix;
    }

    public Matrix4f getViewMatrix() {
        return viewMatrix;
    }

    public void update() {
        updateProjectionMatrix();
        updateViewMatrix();
    }

    private void createBuffers() {
        viewMatrixBuffer = BufferUtils.createFloatBuffer(16);
        projectionMatrixBuffer = BufferUtils.createFloatBuffer(16);
    }

    private void updateViewMatrix() {
        // update game.matrix buffer
        viewMatrix.get(viewMatrixBuffer);
    }

    private void updateProjectionMatrix() {
        // Get parameters of screen
        float width = Graphics.getSettings().getResolutionWidth();
        float height = Graphics.getSettings().getResolutionHeight();
        float delta = width / height;
        // Update game.matrix
        projectionMatrix.m00(delta);
        // Update game.matrix buffer
        projectionMatrix.get(projectionMatrixBuffer);
    }

}
