package net.djuke.engine.graphics.components;

import net.djuke.engine.graphics.components.scene.SceneObject;
import net.djuke.engine.graphics.components.texture.CubeTexture;
import net.djuke.engine.graphics.components.texture.Texture;

/**
 * Skybox
 *
 * @author Vadim
 * @version 1.0 15.09.2017
 */
public final class Skybox implements SceneObject {

    private final String name;
    private final CubeTexture texture;

    public Skybox(String name, CubeTexture texture) {
        this.name = name;
        this.texture = texture;
    }

    public String getName() {
        return name;
    }

    public Texture getTexture() {
        return texture;
    }

}
