package net.djuke.engine.graphics;

import net.djuke.engine.data.Package;
import net.djuke.engine.data.beans.ModelData;
import net.djuke.engine.data.beans.ShaderSources;
import net.djuke.engine.data.beans.TextureData;
import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.texture.Texture;

import java.io.IOException;

/**
 * GraphicsDataLoader - class for loading graphics data inside render thread
 *
 * @author Vadim
 * @version 1.0 31.07.2017
 */
public class GraphicsDataLoader {

    /**
     * Convert package of loaded files to package of objects for usage in graphics subsystem
     * @param source package of loaded files
     * @return package of objects
     */
    public static Package load(Package source) {
        Package data = new Package(source.getName());
        for (String file : source.getObjectsList()) {
            try {
                if (source.get(file) instanceof ShaderSources) data.put(file, new Shader((ShaderSources)source.get(file)));
                else if (source.get(file) instanceof TextureData) data.put(file, new Texture((TextureData)source.get(file)));
                else if (source.get(file) instanceof ModelData) data.put(file, new Mesh((ModelData)source.get(file)));
            } catch (IOException e) {
                // just do nothing
            }
        }
        return data;
    }

}
