package net.djuke.engine.graphics;

import org.joml.Vector2f;

import java.io.Serializable;

/**
 * GraphicSettings - settings of graphics subsystem
 *
 * @author Vadim
 * @version 1.0 13.07.2017
 */
public class GraphicsSettings implements Serializable {

    // Start-up variables
    private Vector2f resolution;
    private boolean fullscreen;
    private boolean msaa;
    private int msaaQuality;
    private int mipmapLevels;
    // Settings of image
    private float contrast;
    private float brightness;
    private float gamma;
    // Dynamic variables
    private volatile int interfaceUnitSize;

    public GraphicsSettings() {

    }

    public float getGamma() {
        return gamma;
    }

    public void setGamma(float gamma) {
        this.gamma = gamma;
    }

    public float getContrast() {
        return contrast;
    }

    public void setContrast(float contrast) {
        this.contrast = contrast;
    }

    public float getBrightness() {
        return brightness;
    }

    public void setBrightness(float brightness) {
        this.brightness = brightness;
    }

    public int getInterfaceUnitSize() {
        return interfaceUnitSize;
    }

    public void setInterfaceUnitSize(int interfaceUnitSize) {
        this.interfaceUnitSize = interfaceUnitSize;
    }

    public void setMipmapLevels(int mipmapLevels) {
        this.mipmapLevels = mipmapLevels;
    }

    public int getMipmapLevels() {
        return mipmapLevels;
    }

    public int getMsaaQuality() {
        return msaaQuality;
    }

    public void setMsaaQuality(int msaaQuality) {
        this.msaaQuality = msaaQuality;
    }

    public boolean isMsaa() {
        return msaa;
    }

    public void setMsaa(boolean msaa) {
        this.msaa = msaa;
    }

    public float getResolutionWidth() {
        return resolution.x();
    }

    public float getResolutionHeight() {
        return resolution.y();
    }

    public Vector2f getResolution() {
        return resolution;
    }

    public void setResolution(Vector2f resolution) {
        this.resolution = resolution;
    }

    public boolean isFullscreen() {
        return fullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public void setDefaultSettings() {
        gamma = 1.0f;
        contrast = 1.0f;
        brightness = 0.0f;
    }
}
