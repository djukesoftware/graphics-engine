package net.djuke.engine.graphics.gui;

import org.joml.Vector2fc;
import org.liquidengine.legui.system.context.Context;
import org.liquidengine.legui.system.renderer.nvg.NvgImageRenderer;
import org.lwjgl.nanovg.NVGPaint;
import org.lwjgl.nanovg.NanoVGGL3;

import java.util.Map;

import static org.lwjgl.nanovg.NanoVG.*;

/**
 * DjukeImageRenderer
 *
 * @author Vadim
 * @version 1.0 25.08.2017
 */
class DjukeImageRenderer<T extends DjukeImage> extends NvgImageRenderer<T> {

    @Override
    protected void renderImage(T image, Vector2fc position, Vector2fc size, Map<String, Object> properties, Context context, long nanovg) {
        NVGPaint imagePaint = NVGPaint.calloc();

        int texture = image.getTexture().getId();
        int nvgid = image.getNvgid();
        if (nvgid == -1) {
            nvgid = NanoVGGL3.nvglCreateImageFromHandle(nanovg, texture, image.getWidth(), image.getHeight(), 0);
            image.setNvgid(nvgid);
        }
        float x = position.x();
        float y = position.y();
        float height = size.y();
        float width = size.x();
        float r = (float) properties.getOrDefault(C_RADIUS, 0);
        nvgBeginPath(nanovg);
        nvgImagePattern(nanovg, x, y, width, height, 0,  nvgid, image.getAlpha(), imagePaint);
        nvgRoundedRect(nanovg, x, y, width, height, r);
        nvgFillPaint(nanovg, imagePaint);
        nvgFill(nanovg);

        imagePaint.free();
    }
}
