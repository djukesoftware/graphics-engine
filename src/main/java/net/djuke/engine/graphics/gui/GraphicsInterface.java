package net.djuke.engine.graphics.gui;

import net.djuke.engine.graphics.Graphics;
import org.liquidengine.legui.component.Frame;
import org.liquidengine.legui.listener.processor.EventProcessor;
import org.liquidengine.legui.system.context.Context;
import org.liquidengine.legui.system.context.DefaultCallbackKeeper;
import org.liquidengine.legui.system.event.SystemCursorPosEvent;
import org.liquidengine.legui.system.handler.processor.SystemEventProcessor;
import org.liquidengine.legui.system.renderer.Renderer;
import org.liquidengine.legui.system.renderer.RendererProvider;
import org.liquidengine.legui.system.renderer.nvg.NvgRenderer;
import org.liquidengine.legui.system.renderer.nvg.NvgRendererProvider;
import org.liquidengine.legui.theme.Themes;
import org.liquidengine.legui.theme.dark.DarkTheme;

/**
 * GraphicsInterface
 *
 * @author Vadim
 * @version 1.0 06.08.2017
 */
public class GraphicsInterface {

    private static GraphicsInterface instance;

    private Context context;
    private Renderer renderer;
    private volatile Frame frame;
    private SystemEventProcessor systemEventProcessor;

    private GraphicsInterface() {

    }

    public void startup() {
        int width = (int) Graphics.getSettings().getResolutionWidth();
        int height = (int) Graphics.getSettings().getResolutionHeight();

        frame = new Frame(width, height);
        context = new Context(Graphics.getWindow().getWindow());

        DefaultCallbackKeeper keeper = new DefaultCallbackKeeper();
        keeper.registerCallbacks(Graphics.getWindow().getWindow());

        systemEventProcessor = new SystemEventProcessor();
        systemEventProcessor.addDefaultCallbacks(keeper);

        renderer = new NvgRenderer();
        renderer.initialize();

        ((NvgRendererProvider)RendererProvider.getInstance()).putImageRenderer(DjukeImage.class, new DjukeImageRenderer());

        Thread thread = new Thread(() -> {
            while (true) {
                systemEventProcessor.processEvents(frame, context);
                EventProcessor.getInstance().processEvents();
            }
        });
        thread.setName("LEGUI EventProcessor");
        thread.start();

        Themes.setDefaultTheme(new DarkTheme());
    }

    public void setFrame(InterfaceFrame frame) {
        this.frame = frame;
    }

    public void render() {
        this.context.updateGlfwWindow();

        renderer.render(frame, this.context);

        systemEventProcessor.processEvents(frame, context);
        EventProcessor.getInstance().processEvents();
        systemEventProcessor.pushEvent(new SystemCursorPosEvent(
                Graphics.getWindow().getWindow(),
                Graphics.getWindow().getMouse().getPosition().x,
                Graphics.getWindow().getMouse().getPosition().y));
    }

    public void delete() {
        renderer.destroy();
    }

    public static GraphicsInterface getInstance() {
        if (instance == null) instance = new GraphicsInterface();
        return instance;
    }
}
