package net.djuke.engine.graphics.gui;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.GraphicsSettings;
import net.djuke.engine.graphics.components.scene.SceneObject;
import org.liquidengine.legui.component.Frame;

/**
 * InterfaceFrame
 *
 * @author Vadim
 * @version 1.0 11.10.2017
 */
public class InterfaceFrame extends Frame implements SceneObject {

    private String name;

    public InterfaceFrame() {
        super((int) Graphics.getSettings().getResolutionWidth(), (int) Graphics.getSettings().getResolutionHeight());

        load("Graphics Interface");
    }

    public InterfaceFrame(String name) {
        super((int) Graphics.getSettings().getResolutionWidth(), (int) Graphics.getSettings().getResolutionHeight());
        load(name);
    }

    private void load(String name) {
        this.name = name;
    }

    public void update() {
    }

    @Override
    public String getName() {
        return name;
    }
}
