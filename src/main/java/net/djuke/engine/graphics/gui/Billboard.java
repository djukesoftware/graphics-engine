package net.djuke.engine.graphics.gui;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.camera.Camera;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector4f;

/**
 * Billboard - calculate position of a 3D model on the screen.
 *
 * @author Vadim
 * @version 1.0 30.07.2017
 */
public class Billboard {

    private Matrix4f model;
    private Camera camera;

    private Vector2f screenCoordinates = new Vector2f();
    private Vector4f center = new Vector4f();
    private Vector4f result = new Vector4f();
    private Matrix4f MVP = new Matrix4f();

    public Billboard(Camera camera, Matrix4f model) {
        this.model = model;
        this.camera = camera;
    }

    public Vector2f getScreenCoordinates() {

        // view mul by model and save to MVP
        camera.getViewMatrix().mul(model, MVP);
        // Projection mul by MVP and save to MVP
        camera.getProjectionMatrix().mul(MVP, MVP);
        // center mul by MVP and save to result
        center.mul(MVP, result);


        // Get screen coordinates
        screenCoordinates.set(
                result.get(0) / result.get(2),
                result.get(1) / result.get(2));

        screenCoordinates.x  = (((screenCoordinates.x + 1) / 2f) * Graphics.getSettings().getResolutionWidth());
        screenCoordinates.y = (((-screenCoordinates.y + 1) / 2f) * Graphics.getSettings().getResolutionHeight());

        return screenCoordinates;
    }


}
