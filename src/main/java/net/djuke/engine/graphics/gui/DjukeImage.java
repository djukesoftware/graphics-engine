package net.djuke.engine.graphics.gui;

import net.djuke.engine.graphics.components.texture.Texture;
import org.liquidengine.legui.image.Image;

/**
 * DjukeImage
 *
 * @author Vadim
 * @version 1.0 25.08.2017
 */
public class DjukeImage extends Image{

    private Texture texture;
    private int nvgid = -1;
    private float alpha = 1;

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public DjukeImage(Texture texture) {
        this.texture = texture;
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public int getNvgid() {
        return nvgid;
    }

    public void setNvgid(int nvgid) {
        this.nvgid = nvgid;
    }

    public int getWidth() {
        return texture.getWidth();
    }

    public int getHeight() {
        return texture.getHeight();
    }
}
