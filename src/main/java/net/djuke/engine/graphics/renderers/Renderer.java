package net.djuke.engine.graphics.renderers;

import net.djuke.engine.graphics.components.scene.Scene;

/**
 * Renderer
 *
 * @author Vadim
 * @version 1.0 11.09.2017
 */
public interface Renderer<T > {

    void render(Scene environment, T object);

}
