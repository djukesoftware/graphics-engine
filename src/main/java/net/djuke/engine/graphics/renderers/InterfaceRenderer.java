package net.djuke.engine.graphics.renderers;

import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.gui.GraphicsInterface;
import net.djuke.engine.graphics.gui.InterfaceFrame;

/**
 * InterfaceRenderer
 *
 * @author Vadim
 * @version 1.0 15.09.2017
 */
public class InterfaceRenderer implements Renderer<InterfaceFrame> {

    public void render(Scene environment, InterfaceFrame object) {
        // Update instance of interface
        object.update();
        // Switch to current interface frame
        GraphicsInterface.getInstance().setFrame(object);
        GraphicsInterface.getInstance().render();
    }
}
