package net.djuke.engine.graphics.renderers;

import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.texture.Texture;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

/**
 * ModelRenderer
 *
 * @author Vadim
 * @version 1.0 11.09.2017
 */
public class ModelRenderer implements Renderer<Model> {

    private FloatBuffer buffer = BufferUtils.createFloatBuffer(16);

    public void render(Scene environment, Model object) {
        if (object.getShader() != null) {
            Shader shader = object.getShader();
            shader.bind();

            // Bind all textures to render state
            int textureid = 0;
            for (Texture texture : object.getTextures()) {
                texture.bind(textureid);
                shader.sendTexture(texture.getName(), textureid);
                textureid++;
            }

            // Send information about model game.matrix
            object.getModelMatrix().get(buffer);
            shader.sendMatrix4f("modelMatrix", buffer);

            shader.sendBoolean("debug", object.isDebug());

            // Send information about camera game.matrix
            shader.sendMatrix4f("viewMatrix", environment.getCamera().getViewMatrixBuffer());
            shader.sendMatrix4f("projectionMatrix", environment.getCamera().getProjectionMatrixBuffer());

            if (object.getMesh() != null) object.getMesh().render();

            shader.unbind();
        }
    }

}
