package net.djuke.engine.graphics.renderers;

import net.djuke.engine.data.Data;
import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.Skybox;
import net.djuke.engine.graphics.components.scene.Scene;

import java.io.IOException;

/**
 * SkyboxRenderer
 *
 * @author Vadim
 * @version 1.0 15.09.2017
 */
public class SkyboxRenderer implements Renderer<Skybox> {

    private Shader shader;
    private Mesh mesh;

    public void render(Scene environment, Skybox object) {
        if (shader != null && mesh != null) {
            shader.bind();

            shader.sendMatrix4f("viewMatrix", environment.getCamera().getViewMatrixBuffer());
            shader.sendMatrix4f("projectionMatrix", environment.getCamera().getProjectionMatrixBuffer());
            object.getTexture().bind(0);
            shader.sendTexture("skybox", 0);

            mesh.render();

            shader.unbind();
        } else {
            try {
                Shader shader = (Shader) Data.getEngineData().get("skybox.glsl");
                Mesh mesh = (Mesh) Data.getEngineData().get("cube.obj");
                this.shader = shader;
                this.mesh = mesh;
            } catch (IOException e) {

            }
        }
    }

}
