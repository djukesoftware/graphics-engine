package net.djuke.engine.graphics.window;

/**
 * FPSCounter
 *
 * @author Vadim
 * @version 1.0 25.07.2017
 */
class FPSCounter {

    private double lastUpdateTime;
    private int currentCounter;
    private int count;

    double getLastUpdateTime() {
        return lastUpdateTime;
    }

    void setLastUpdateTime(double lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    int getCurrentCounter() {
        return currentCounter;
    }

    void setCurrentCounter(int currentCounter) {
        this.currentCounter = currentCounter;
    }

    int getCount() {
        return count;
    }

    void setCount(int count) {
        this.count = count;
    }

    float getAlpha() {
        return 60f / (float)count;
    }
}
