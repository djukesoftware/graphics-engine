package net.djuke.engine.graphics.window;

import net.djuke.engine.graphics.components.texture.Frame;
import net.djuke.engine.input.Keyboard;
import net.djuke.engine.input.Mouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Window - (adapter) creating window in desktop environment of operating system
 * Created by azusa on 3/9/17.
 */
public class Window  {

    private Logger logger = LogManager.getLogger("Window");
    private WindowDriver window = new WindowDriver();

    /**
     * Create single window for rendering
     */
    public void create() {
        window.setDaemon(true);
        window.setName("Render");
        window.start();
    }

    /**
     * @see java.lang.Thread
     */
    public void join() {
        try {
            window.join();
        } catch (InterruptedException e) {
            logger.error(e.getMessage());
        }
    }

    public boolean isOpen() {
        return window.isAlive();
    }

    /**
     * Destroy current window
     */
    public void destroy() {
        window.close();
    }

    /**
     * Close current window and create new one with rendering last scene
     */
    public synchronized void recreate() {
        window.recreate();
    }

    /**
     * Get current value of FPS
     * @return fps
     */
    public int getFPS() {
        return window.getFPS();
    }

    /**
     * Speed of updating objects (60 / fps)
     * @return alpha constant
     */
    public float getAlpha() {
        return window.getAlpha();
    }

    /**
     * Get id of window
     * @deprecated
     * @return if of window
     */
    public long getWindow() {
        return window.getWindow();
    }

    public List<Frame> getFrameList() {
        return window.getFrameList();
    }

    /**
     * Сделать скриншот текущего окна
     * @return screenshot
     */
    public Screenshot takeScreenshot() {
        return window.takeScreenshot();
    }

    /* INPUT FOR WINDOW */

    /**
     * Get a mouse from the current window
     * @return window mouse
     */
    public Mouse getMouse() {
        return window.getMouse();
    }

    public void addFrame(Frame frame) {
        window.addFrame(frame);
    }

    public void removeFrame(Frame frame) {
        window.removeFrame(frame);
    }

    public void setFrame(int id, Frame frame) {
        window.setFrame(id, frame);
    }

    /**
     * Get a keyboard from the current window
     * @return window keyboard
     */
    public Keyboard getKeyboard() {
        return window.getKeyboard();
    }

}
