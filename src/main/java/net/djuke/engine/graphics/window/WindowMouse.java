package net.djuke.engine.graphics.window;

import net.djuke.engine.input.Mouse;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWScrollCallback;

import java.nio.DoubleBuffer;

import static org.lwjgl.glfw.GLFW.*;

/**
 * WindowMouse
 *
 * @author Vadim
 * @version 1.0 27.07.2017
 */
class WindowMouse implements Mouse {

    private DoubleBuffer xpos = BufferUtils.createDoubleBuffer(1);
    private DoubleBuffer ypos = BufferUtils.createDoubleBuffer(1);

    private volatile Vector2f position = new Vector2f();
    private volatile Vector2f speed = new Vector2f();
    private volatile double wheel = 0;

    private volatile Vector2f lastPosition = new Vector2f();

    private volatile int BUTTON_RIGHT = RELEASE;
    private volatile int BUTTON_LEFT = RELEASE;

    private long window;

    public WindowMouse(long window) {
        // TODO: Разобраться с этим мусором
        this.window = window;
    }

    public void registerCallbacks() {
        glfwSetScrollCallback(window, (long win, double xpos, double ypos) -> {
            wheel = -ypos;
        });
    }

    public double getWheelOffset() {
        return wheel;
    }

    public Vector2f getPosition() {
        return position;
    }

    public boolean buttonIs(int button, int instance) {
        switch (button) {
            case Mouse.BUTTON_LEFT: return this.BUTTON_LEFT == instance;
            case Mouse.BUTTON_RIGHT: return this.BUTTON_RIGHT == instance;
            default: return false;
        }
    }

    @Override
    public Vector2f getSpeed() {
        return speed;
    }

    void update(long window) {
        // get instances of mouse buttons
        BUTTON_LEFT = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
        BUTTON_RIGHT = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
        // update position of the mouse
        glfwGetCursorPos(window, xpos, ypos);

        lastPosition.set(position); // position of last frame
        position.set((float)xpos.get(0), (float)ypos.get(0));
        lastPosition.sub(position, speed);
    }

    void clear() {
        wheel = 0;
    }

}
