package net.djuke.engine.graphics.window;

import net.djuke.engine.input.Keyboard;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.glfwSetCharCallback;

/**
 * WindowKeyboard
 *
 * @author Vadim
 * @version 1.0 27.07.2017
 */
class WindowKeyboard implements Keyboard {

    private String lastCharFromKeyboard = "";
    private List<Callback> callbackList = new ArrayList<>();

    WindowKeyboard() {
        // do nothing
    }

    void bind(long window) {
        // create callback
        glfwSetCharCallback(window, (long l, int i) -> {
            byte[] c = {(byte) i};
            try {
                lastCharFromKeyboard = new String(c, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            update(lastCharFromKeyboard);
        });
    }

    /**
     * Add callback to handle every char from keyboard input
     * @param callback callback object
     */
    public void addTextInputCallback(Callback callback) {
        callbackList.add(callback);
    }

    private void update(String lastChar) {
        for (Callback current : callbackList) {
            current.update(lastChar);
        }
    }


}
