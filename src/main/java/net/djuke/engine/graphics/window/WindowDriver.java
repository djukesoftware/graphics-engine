package net.djuke.engine.graphics.window;

import net.djuke.engine.data.Data;
import net.djuke.engine.data.beans.TextureData;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.GraphicsDataLoader;
import net.djuke.engine.graphics.components.Image;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.texture.Frame;
import net.djuke.engine.graphics.gui.GraphicsInterface;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.lwjgl.glfw.GLFWImage;
import org.lwjgl.opengl.GL;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;
import static org.lwjgl.opengl.GL30.GL_FRAMEBUFFER;
import static org.lwjgl.opengl.GL30.glBindFramebuffer;

/**
 * WindowDriver - window driver
 *
 * @author Vadim
 * @version 1.0 23.05.2017
 */
class WindowDriver extends Thread {

    // Interrupt parameters for window
    public enum InterruptMode {
        RECREATE, CLOSE
    }

    private volatile InterruptMode interruptType = InterruptMode.CLOSE;

    private WindowMouse mouse;
    private WindowKeyboard keyboard;

    private Logger logger = LogManager.getLogger("Window driver");
    private long window; // id of current window

    /**
     * Коллекция фреймов для отрисовки. Первый фрейм данной коллекции будет
     * отрисовываться напрямую в текущее окно
     */
    private List<Frame> frameList = new CopyOnWriteArrayList<>();
    /**
     * Переменная для запроса скриншота
     */
    private volatile Screenshot screenshot;
    private int screenid = 0;
    /**
     * Шейдер для отрисовки изображения внутри окна. Позволяет настраивать яркость, контрастность, а так же
     * добавлять некоторые пост-эффекты
     */
    private Shader screenShader;

    private FPSCounter fps = new FPSCounter();

    public void run() {
        createWindow();

        fps.setLastUpdateTime(glfwGetTime());

        // Create texture for rendering image to

        // Loading graphics data for engine
        logger.info("Loading graphics data of engine...");
        try {
            Data.getEngineData().putFrom(GraphicsDataLoader.load(Data.loadPackage("engine", Data.ENGINE_FILE_SYSTEM)));
            screenShader = (Shader) Data.getEngineData().get("render.glsl");
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        logger.info("Creating GUI...");
        GraphicsInterface.getInstance().startup();

        mouse.registerCallbacks();

        glfwSetKeyCallback(window, (long window, int key, int scancode, int action, int mods) -> {
            if (key == GLFW_KEY_PRINT_SCREEN && action == GLFW_RELEASE) {
                (new Thread(() -> {
                    try {
                        DateFormat format = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
                        Data.saveObject("resources/screenshots/screen-" + format.format(new Date(System.currentTimeMillis())) + ".png", takeScreenshot().get(), Data.OS_FILE_SYSTEM);
                    } catch (IOException e) {
                        // just ignore this, because is not fatal error
                    }
                })).start();
            }
        });


        while (glfwGetKey(window, GLFW_KEY_F1) != 1 && !interrupted()) {

            // Render all frames from queue
            for (Frame frame : frameList) frame.render();

            // Render to default framebuffer
            glBindFramebuffer(GL_FRAMEBUFFER, 0);

            // Set alpha channel
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

            // Update position of the mouse
            mouse.update(window);
            // Clear color buffer
            // Clear z buffer (if we don't do that, we get black screen because each pixel of z buffer
            // is 0 by default)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            // Подключаем шейдер для отрисовки изображения (пост-эффекты и остальное)
            if (screenShader != null) {
                // First frame in queue is main frame by default
                if (frameList.size() != 0) frameList.get(0).bind(0);
                // Bind this frame as texture
                screenShader.bind();
                screenShader.sendFloat("contrast", Graphics.getSettings().getContrast());
                screenShader.sendFloat("brightness", Graphics.getSettings().getBrightness());
                screenShader.sendFloat("gamma", Graphics.getSettings().getGamma());
                screenShader.sendTexture("texture", 0);
            }
            // На небольшом квадрате отрисовываем изображение
            glBegin(GL_QUADS);
            glVertex2f(-1, -1);
            glVertex2f(1, -1);
            glVertex2f(1, 1);
            glVertex2f(-1, 1);
            glEnd();
            if (screenShader != null) screenShader.unbind();

            // Создание скриншота, если это необходимо
            if (screenshot != null) {
                // Get current window frame and convert to Image
                Image image = frameList.get(0).convertToImage();
                // Put this image to screenshot
                screenshot.setImage(image);
                // Cancel screenshot creating
                screenshot = null;
            }

            fps.setCurrentCounter(fps.getCurrentCounter() + 1);
            if (fps.getLastUpdateTime() + 1 < glfwGetTime()) {
                fps.setCount(fps.getCurrentCounter());
                fps.setLastUpdateTime(glfwGetTime());
                fps.setCurrentCounter(0);
                // ha-ha здесь была строка, закрывающая окно через 2 минуты
            }

            mouse.clear();

            glfwPollEvents();
            glfwSwapBuffers(window);
        }

        if (interruptType == InterruptMode.CLOSE) {
            // Closing window
            logger.info("Destroying gui");
            GraphicsInterface.getInstance().delete();

            logger.info("Destroying window");
            glfwDestroyWindow(window);

        } else if (interruptType == InterruptMode.RECREATE) {
            // Destroy current window
            logger.info("Destroying window");
            glfwDestroyWindow(window);

            // Clear interrupt type
            interruptType = InterruptMode.CLOSE;

            // Create new window with new resolution
            run();
        }
    }

    /**
     * @return id of window
     * @deprecated
     */
    long getWindow() {
        return window;
    }

    /**
     * Метод для запроса скриншота
     *
     * @return current screenshot
     */
    public Screenshot takeScreenshot() {
        if (screenshot == null) {
            screenshot = new Screenshot();
            return screenshot;
        } else {
            // Если скриншот уже запросил другой поток, значит возвращаем уже созданный запрос на
            // создание скрина
            return screenshot;
        }
    }

    /**
     * Recreate the window after interrupt
     */
    void recreate() {
        interruptType = InterruptMode.RECREATE;
        interrupt();
    }

    void close() {
        interruptType = InterruptMode.CLOSE;
        interrupt();
    }

    void addFrame(Frame frame) {
        frameList.add(frame);
    }

    void removeFrame(Frame frame) {
        frameList.remove(frame);
    }

    void setFrame(int id, Frame frame) {
        frameList.set(id, frame);
    }

    int getFPS() {
        return fps.getCount();
    }

    public List<Frame> getFrameList() {
        return frameList;
    }

    private void createWindow() {
        // Check the GLFW library
        if (!glfwInit()) logger.error("Failed to init GLFW");
        if (Graphics.getSettings().isMsaa())
            glfwWindowHint(GLFW_SAMPLES, Graphics.getSettings().getMsaaQuality());
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        int width = (int) Graphics.getSettings().getResolutionWidth();
        int height = (int) Graphics.getSettings().getResolutionHeight();
        boolean fullscreen = Graphics.getSettings().isFullscreen();

        logger.info("Creating window");
        window = glfwCreateWindow(
                width, height, "engine", fullscreen ? glfwGetPrimaryMonitor() : GL_NONE, GL_NONE);
        glfwSetGamma(window, 1f);
        try {
            TextureData data = (TextureData) Data.loadObject("game/icons/icon.txr", Data.ENGINE_FILE_SYSTEM);
            GLFWImage image = GLFWImage.malloc();
            GLFWImage.Buffer imageBuffer = GLFWImage.malloc(1);
            image.set(data.getWidth(), data.getHeight(), data.getBuffer());
            imageBuffer.put(0, image);
            glfwSetWindowIcon(window, imageBuffer);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        glfwMakeContextCurrent(window);
        glfwShowWindow(window);
        glfwSetWindowPos(window, 100, 100);
        glfwSwapInterval(1);
        GL.createCapabilities();
        glViewport(0, 0, width, height);

        // Enable MSAA smoothing
        if (Graphics.getSettings().isMsaa()) glEnable(GL_MULTISAMPLE);

        // Enable z buffer (contain distance between screen and rendered fragment)
        glEnable(GL_DEPTH_TEST);

        // After we created window we create new mouse and keyboard
        mouse = new WindowMouse(window);
        keyboard = new WindowKeyboard();

        logger.info("Check version of GLFW: " + glfwGetVersionString());
        logger.info("OpenGL version: " + glGetString(GL_VERSION));
    }

    float getAlpha() {
        return fps.getAlpha();
    }

    public WindowMouse getMouse() {
        return mouse;
    }

    public WindowKeyboard getKeyboard() {
        return keyboard;
    }
}
