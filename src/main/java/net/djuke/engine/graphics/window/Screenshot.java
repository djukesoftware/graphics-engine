package net.djuke.engine.graphics.window;

import net.djuke.engine.graphics.components.Image;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Screenshot - отвечает за хранение скриншота. Скриншот текущего фрейма может быть получен не сразу, а только после
 * того как данный фрейм будет отрисован внутри окна и поэтому данный класс реализует интерфейс Future.
 * @see net.djuke.engine.graphics.components.texture.Frame
 *
 * @author Vadim
 * @version 1.0 20.11.2017
 */
public class Screenshot implements Future<Image> {

    private volatile Image image;

    void setImage(Image image) {
        this.image = image;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return image != null;
    }

    /**
     * Получаем изображение текущего скриншота. Если фрейм не был отрисован, то данный метод будет ожидать до тех пор
     * пока фрейм не отрисуется и его изображение не будет получено.
     * @return image of screenshot
     */
    @Override
    public Image get() {
        try {
            while (!isDone()) {
                Thread.sleep(10);
            }
            return image;
        } catch (InterruptedException e) {
            return null;
        }
    }

    @Override
    public Image get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        long counter = 0;
        while (isDone()) {
            Thread.sleep(10);
            counter += 10;
            if (counter > timeout) {
                throw new TimeoutException();
            }
        }
        return image;
    }
}
