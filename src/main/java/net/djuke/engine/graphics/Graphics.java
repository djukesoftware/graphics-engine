package net.djuke.engine.graphics;

import net.djuke.engine.core.Core;
import net.djuke.engine.graphics.components.Model;
import net.djuke.engine.graphics.components.Skybox;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.engine.graphics.renderers.InterfaceRenderer;
import net.djuke.engine.graphics.renderers.ModelRenderer;
import net.djuke.engine.graphics.renderers.Renderer;
import net.djuke.engine.graphics.renderers.SkyboxRenderer;
import net.djuke.engine.graphics.window.Window;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * GraphicsManager - основной класс для запуска и управления графической подсистемой. Отвечает за хранение отрисовщиков
 * и графических настроек.
 * @see net.djuke.engine.graphics.renderers.Renderer
 * @see net.djuke.engine.graphics.GraphicsSettings
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class Graphics {

    private static Logger logger = LogManager.getLogger("Graphics subsystem");

    // Map of renderers for rending object on the screen
    private static Map<Class, Renderer> rendererMap = new ConcurrentHashMap<>();
    // Settings of graphics subsystem
    private static GraphicsSettings settings;

    // Create root window
    private static final Window window = new Window();

    /**
     * Запуск графической подсистемы. Вызывается классом Core в главном потоке.
     * @see net.djuke.engine.core.Core
     */
    public static void startup() {
        // Load default renderers
        rendererMap.put(Model.class, new ModelRenderer());
        rendererMap.put(InterfaceFrame.class, new InterfaceRenderer());
        rendererMap.put(Skybox.class, new SkyboxRenderer());

        settings = Core.getSettings().getGraphics();

        // Calculate one unit of the screen for scaling graphics user interface
        float alpha = settings.getResolutionWidth() / 1920;
        float beta = settings.getResolutionHeight() / 1080;
        settings.setInterfaceUnitSize((int)(100 * Math.min(alpha, beta)));

        logger.info("Creating window...");
        window.create();
    }

    /**
     * Завершение работы графической подсистемы. Так же как и startup() вызывается классом Core в главном потоке.
     * @see net.djuke.engine.core.Core
     */
    public static void shutdown() {
        window.destroy();
    }

    /**
     * Добавляет отрисовщик объектов, распололженных внутри сцены.
     * @see net.djuke.engine.graphics.components.scene.Scene
     * @param clazz класс отрисовываемого объекта
     * @param renderer экземпляр отрисовщика
     */
    public static void addRenderer(Class clazz, Renderer renderer) {
        rendererMap.put(clazz, renderer);
    }

    /**
     * Возвращает отрисовщик для определённого объекта
     * @param clazz класс объекта
     * @return Renderer или null если отрисовщик не найден
     */
    public static Renderer getRenderer(Class clazz) {
        return rendererMap.get(clazz);
    }

    public static int interfaceUnitToPixels(float units) {
        return (int)(settings.getInterfaceUnitSize() * units);
    }

    public static int widthPercentToPixels(float percent) {
        return (int)(settings.getResolutionWidth() * percent);
    }

    public static int heightPercentToPixels(float percent) {
        return (int)(settings.getResolutionHeight() * percent);
    }

    /**
     * Возвращает главное окно
     * @return Window в котором происходит отрисовка
     */
    public static Window getWindow() {
        return window;
    }

    /**
     * Возвращает настройки графической подсистемы
     * @return GraphicsSettings
     */
    public static GraphicsSettings getSettings() {
        return settings;
    }

}
