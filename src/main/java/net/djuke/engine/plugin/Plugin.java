package net.djuke.engine.plugin;

/**
 * Plugin
 *
 * @author Vadim
 * @version 1.0 15.07.2017
 */
public interface Plugin extends Runnable {

    default void start(String name) {
        Thread thread = new Thread(this);
        thread.setName(name);
        thread.setDaemon(true);
        thread.start();
    }

    void run();
}

