package net.djuke.engine.plugin;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * PluginManager load plugins from the default directory and run them. PluginManager add every class of a
 * plugin to all extendable subsystems of the engine and create new thread for running plugins.
 *
 * @author Vadim
 * @version 1.0 21.06.2017
 */
public class PluginManager {

    private static PluginManager instance;

    private Logger logger = LogManager.getLogger("Plugin manager");
    private ArrayList<PluginWrapper> plugins = new ArrayList<>();

    private PluginManager() {

    }

    public void startup() {
        logger.info("Loading plugins from file system");
        loadPlugins();
    }

    public void shutdown() {

    }

    /**
     * Loading plugins from plugin directory
     */
    public void loadPlugins() {
        // open current directory
        File file = new File("plugins");
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            // if directory not empty, load every jar file from this directory
            if (files != null) {
                for (File currentFile : files) {
                    if (currentFile.getName().endsWith(".jar")) {
                        logger.info("Loading plugin from " + currentFile.getName());
                        plugins.add(loadPluginFromFile(currentFile));
                    }
                }
            }
        }
        // After loading plugins we must to extend engine subsystems
        for (PluginWrapper plugin : plugins) {
            if (plugin.getInformation().getName() != null) {
                logger.info("Loading plugin: " + plugin.getInformation().getName());

                // Check every class
                for (Class current : plugin.getClasses()) {
                    try {
                        // if class implements Plugin, then run new Plugin
                        if (Plugin.class.isAssignableFrom(current)) {
                            logger.info("Starting " + plugin.getInformation().getName() + " plugin");
                            ((Plugin)current.newInstance()).start(plugin.getInformation().getName());
                        }
                    } catch (IllegalAccessException | InstantiationException e) {
                        logger.error(e.getMessage());
                    }
                }

            } else {
                logger.warn("Plugin " + plugin.getFile().getName() + " will not load because there is no name");
            }
        }
    }

    public void updatePlugins() {
        // Check version of every local plugin and compare with version on the djuke plugin server
        // if version of local plugin not equals with version on DPS
        // then download new version of plugin and delete old version

    }

    public void downloadPlugin(String name) {
        // download plugin from DPS
    }

    /**
     * Get list of all local plugins
     * @return list of plugins
     */
    public List<String> getPluginList() {
        List<String> list = new ArrayList<>();
        for (PluginWrapper plugin : plugins) {
            list.add(plugin.getInformation().getName());
        }
        return list;
    }

    /**
     * Delete plugin by name
     * @param name name of local plugin
     */
    public void deletePlugin(String name) {
        PluginWrapper target = null;
        for (PluginWrapper plugin : plugins) {
            if (plugin.getInformation().getName().equals(name)) {
                plugin.getFile().delete();
                target = plugin;
            }
        }
        if (target != null) plugins.remove(target);
    }

    private PluginWrapper loadPluginFromFile(File file) {
        // Create new plugin
        PluginWrapper plugin = new PluginWrapper();
        plugin.setFile(file);
        try {
            // Loading jar file from directory
            JarFile jar = new JarFile(file);
            // Gel all files inside directory
            Enumeration<JarEntry> entries = jar.entries();
            JarEntry entry;
            // Create URL Class loader for loading classes from current jar
            ClassLoader classLoader = new URLClassLoader(new URL[]{file.toURI().toURL()});
            while (entries.hasMoreElements()) {
                entry = entries.nextElement();
                if (entry.getName().endsWith(".class")) {
                    // loading class to class loader
                    String path = entry.getName();
                    path = path.replaceAll("/", ".");
                    path = path.replaceAll(".class", "");
                    try {
                        plugin.addClass(classLoader.loadClass(path));
                    } catch (ClassNotFoundException e) {
                        logger.error("Class " + path + " not found inside current plugin");
                    }
                } else if (entry.getName().equals("plugin.info")) { // if we find file with information about plugin
                    logger.info("Reading plugin information from plugin.info");
                    // Read data from jar entry
                    BufferedInputStream bis = new BufferedInputStream(jar.getInputStream(entry));
                    BufferedReader reader = new BufferedReader(new InputStreamReader(bis));
                    String line;

                    while ((line = reader.readLine()) != null) {
                        // name : "Converter Plugin"
                        // author : "Vadim Gush"
                        if (line.split(":").length > 1 && line.split("\"").length > 1) {
                            String key = line.split(":")[0];
                            key = key.replaceAll(" ", "");
                            String value = line.split(":")[1].split("\"")[1];
                            if (key.equals("name")) plugin.getInformation().setName(value.replaceAll(" ", ""));
                            if (key.equals("description")) plugin.getInformation().setDescription(value);
                            try {
                                if (key.equals("version")) plugin.getInformation().setVersion(Integer.parseInt(value));
                            } catch (NumberFormatException e) {
                                logger.warn(file.getName() + ": not valid version of plugin in information file");
                            }
                            if (key.equals("email")) plugin.getInformation().setEmail(value);
                            if (key.equals("author")) plugin.getInformation().setAuthor(value);
                            if (key.equals("link")) plugin.getInformation().setLink(value);
                        }
                    }
                }
            }
        } catch (IOException e) {
            logger.error(e.getStackTrace());
        }
        return plugin;
    }

    public static PluginManager getInstance() {
        if (instance == null) instance = new PluginManager();
        return instance;
    }

}
