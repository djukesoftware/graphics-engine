package net.djuke.engine.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Plugin
 *
 * @author Vadim
 * @version 1.0 22.06.2017
 */
class PluginWrapper {

    private PluginInformation information;
    private List<Class> classes = new ArrayList<>();
    private File file;

    public PluginWrapper() {
        information = new PluginInformation();
    }

    public PluginInformation getInformation() {
        return information;
    }

    public void addClass(Class newClass) {
        classes.add(newClass);
    }

    public void setFile(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    public List<Class> getClasses() {
        return classes;
    }

    public class PluginInformation {

        // General information
        private String name;
        private String description;
        private int version;

        // Plugin is official if have valid digital signature
        private boolean official = false;

        // Information to display if plugin have digital signature
        private String author;
        private String email;
        private String link;

        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public int getVersion() {
            return version;
        }

        public void setVersion(int version) {
            this.version = version;
        }

        public void validate() {
            official = true;
        }

        public String getAuthor() {
            return official ? author : null;
        }

        public String getEmail() {
            return official ? email : null;
        }

        public String getLink() {
            return official ? link : null;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }
}
