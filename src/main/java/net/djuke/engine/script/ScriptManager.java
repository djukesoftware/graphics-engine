package net.djuke.engine.script;

import net.djuke.engine.core.Core;
import net.djuke.engine.data.Data;
import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.plugin.PluginManager;
import net.djuke.engine.utils.tools.EngineControlView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Script
 *
 * @author Vadim
 * @version 1.0 26.06.2017
 */
public class ScriptManager {

    private static ScriptManager instance;

    private Logger logger = LogManager.getLogger("Script engine");
    private ScriptEngine engine;
    private StringWriter writer = new StringWriter();

    private List<String> bindings = new ArrayList<>();

    private ScriptManager() { }

    public void startup() {
        logger.info("Creating JavaScript engine");
        ScriptEngineManager manager = new ScriptEngineManager();
        engine = manager.getEngineByName("nashorn");

        engine.getContext().setWriter(writer);

        addBinding("Core", Core.class);
        addBinding("Data", Data.class);
        addBinding("Graphics", Graphics.class);

        addBinding("plugins", PluginManager.getInstance());
        addBinding("engineData", Data.getEngineData());
        addBinding("engineControl", EngineControlView.getInstance());
    }

    public void addBinding(String name, Object object) {
        Map<String, Object> bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);
        bindings.put(name, object);
        this.bindings.add(name);
    }

    public Collection<String> getBindings() {
        return engine.getBindings(ScriptContext.ENGINE_SCOPE).keySet();
    }

    public Class getBindingClass(String name) {
        return engine.getBindings(ScriptContext.ENGINE_SCOPE).get(name).getClass();
    }

    public void shutdown() {

    }

    /**
     * Execute command on script engine
     * @param cmd source code
     * @return output of engine
     */
    public String eval(String cmd) {
        try {
            writer.append("Done: ");
            engine.eval(cmd);
            return writer.toString();
        } catch (ScriptException e) {
            logger.warn(e);
            writer.write(e.getMessage()+ "\n");
            return writer.toString();
        }
    }

    public static ScriptManager getInstance() {
        if (instance == null) instance = new ScriptManager();
        return instance;
    }
}
