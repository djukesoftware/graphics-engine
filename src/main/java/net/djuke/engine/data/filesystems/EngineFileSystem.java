package net.djuke.engine.data.filesystems;

import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * ProjectFileSystem
 *
 * @author Vadim
 * @version 1.0 28.06.2017
 */
public class EngineFileSystem implements FileSystem {

    public BufferedOutputStream write(String name) throws FileSystemException {
        // Смелое утверждение
        // Проверять я его конечно не буду
        throw new FileSystemException("You cannot save files to jar archive");
    }

    public BufferedInputStream read(String name) throws FileSystemException {
        try {
            return new BufferedInputStream(new FileInputStream(new File(
                    getClass().getClassLoader().getResource(name).toURI()
            )));
        } catch (NullPointerException | FileNotFoundException | URISyntaxException e){
            throw new FileSystemException("File \"" + name + "\" not found: " + e.getMessage());
        }
    }

    public String[] getFileList(String directory) throws FileSystemException {
        try {
            File file = new File(getClass().getClassLoader().getResource(directory).toURI());
            List<String> list = new ArrayList<>();
            if (file.isDirectory()) {
                handleDirectory("", file, list);
                String[] array = new String[list.size()];
                return list.toArray(array);
            } else throw new FileSystemException(directory + " is not directory");
        } catch (NullPointerException | URISyntaxException e) {
            throw new FileSystemException("Directory \"" + directory + "\" not found: " + e.getMessage());
        }
    }

    private void handleDirectory(String prefix, File file, List<String> list) {
        for (File current : file.listFiles()) {
            if (current.isDirectory()) handleDirectory(current.getName() + "/", current, list);
            else list.add(prefix + current.getName());
        }
    }

    public long lastModified(String name) throws FileSystemException {
        try {
            File file = new File(getClass().getClassLoader().getResource(name).toURI());
            return file.lastModified();
        } catch (NullPointerException | URISyntaxException e) {
            throw new FileSystemException(e);
        }
    }
}
