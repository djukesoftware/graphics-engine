package net.djuke.engine.data.filesystems;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/**
 * General interface for all filesystems
 * FileSystem allows read or write some file or get list of files in specifiec directory,
 * but not allow get list of all content
 * @version 10/05/2017
 */
public interface FileSystem {
	BufferedOutputStream write(String name) throws FileSystemException;
	BufferedInputStream read(String name) throws FileSystemException;
	String[] getFileList(String directory) throws FileSystemException;
	long lastModified(String name) throws FileSystemException;
}
