package net.djuke.engine.data.filesystems;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class OSFileSystem implements FileSystem {

	public BufferedOutputStream write(String name) throws FileSystemException {
		try {
			return new BufferedOutputStream(new FileOutputStream(new File(name)));
		} catch (FileNotFoundException e) {
			throw new FileSystemException("File not found: " + e);
		}
	}

	public BufferedInputStream read(String name) throws FileSystemException {
		try {
			return new BufferedInputStream(new FileInputStream(new File(name)));
		} catch (FileNotFoundException e) {
			throw new FileSystemException("File not found: " + e);
		}
	}

	public String[] getFileList(String directory) throws FileSystemException {
		File file = new File(directory);
		List<String> list = new ArrayList<>();
		if (file.isDirectory()) {
			handleDirectory("", file, list);
			String[] array = new String[list.size()];
			return list.toArray(array);
		} else throw new FileSystemException(directory + " is not directory");
	}

	private void handleDirectory(String prefix, File file, List<String> list) {
		for (File current : file.listFiles()) {
			if (current.isDirectory()) handleDirectory(current.getName() + "/", current, list);
			else list.add(prefix + current.getName());
		}
	}

	public long lastModified(String name) throws FileSystemException {
		try {
			File file = new File(name);
			return file.lastModified();
		} catch (NullPointerException e) {
			throw new FileSystemException(e);
		}
	}
}
