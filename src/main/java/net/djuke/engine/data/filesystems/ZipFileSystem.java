package net.djuke.engine.data.filesystems;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * File system for one zip archive.
 */
public class ZipFileSystem implements FileSystem, Closeable {

	private File archive;
	private ZipOutputStream zos;
	private ZipFile zip;

	private enum State { WRITE, READ }
	private State state = State.WRITE;

	/**
	 * Open or create zip archive
	 * @param path to archive
	 * @throws FileSystemException if cannot create or open zip archive
	 */
	public ZipFileSystem(String path) throws FileSystemException {
		try {
			// Open the archive or create new archive
			archive = new File(path);
			if (!archive.exists()) {
				archive.createNewFile();
				zos = new ZipOutputStream(new FileOutputStream(archive));
			} else {
				recoverData();
			}
		} catch (IOException e) {
			throw new FileSystemException("IO exception", e);
		}
	}

	/**
	 * Get BufferedOutputStream for write data to zip archive
	 * @param name name of file inside archive
	 * @return new BufferedOutputStream
	 * @throws FileSystemException if file system cannot create new output stream
	 */
	public BufferedOutputStream write(String name) throws FileSystemException {
		try {
			if (state == State.READ) {
				// just switch the mode
				state = State.WRITE;
				zos = new ZipOutputStream(new FileOutputStream(archive));
			}
			zos.putNextEntry(new ZipEntry(name));
			return new InnerOutputStream(zos);
		} catch (IOException e) {
			throw new FileSystemException("IO exception", e);
		}
	}

	/**
	 * Get BufferedInputStream for read data from zip archive
	 * @param name name of file inside archive
	 * @return new BufferedInputStream
	 * @throws FileSystemException if file system cannot open new output stream
	 */
	public BufferedInputStream read(String name) throws FileSystemException {
		try {
			if (state == State.WRITE) {
				zos.close(); // close output stream
				// Open the zip archive
				zip = new ZipFile(archive);
				state = State.READ; 
			}

			ZipEntry entry = zip.getEntry(name);
			if (entry == null) throw new FileSystemException(name + " not found inside " + archive.getName());

			InputStream is = zip.getInputStream(entry);
			return new BufferedInputStream(is);
		} catch (ZipException e) {
			throw new FileSystemException("It is not zip archive", e);
		} catch (IOException e) {
			throw new FileSystemException("IO exception", e);
		}
	}

	/**
	 * Close streams and release any data
	 * @throws IOException any exception of IO from output stream
	 */
	public void close() throws IOException {
		zos.close();
	}

	/**
	 * Get list of file inside zip archive
	 * @param directory name of directory
	 * @return list of file names
	 * @throws FileSystemException if directory not found
	 */
	public String[] getFileList(String directory) throws FileSystemException {
		try {
			// create new list for elements
			List<String> list = new ArrayList<>();
			// change mode
			if (state == State.WRITE) {
				if (zos != null) zos.close();
				zip = new ZipFile(archive);
				state = State.READ;
			}
			// get list of entries
			Enumeration e = zip.entries();
			while (e.hasMoreElements()) {
				ZipEntry entry = (ZipEntry)e.nextElement();
				// get list of files in this directory
				if (entry.getName().contains(directory) && !entry.isDirectory()) {
					list.add(entry.getName());
				}
			}
			// return list of entries
			String[] array = new String[list.size()];
			return list.toArray(array);
		} catch (ZipException e) {
			throw new FileSystemException("Zip exception", e);
		} catch (IOException e) {
			throw new FileSystemException("IO exception", e);
		}
	}

	public long lastModified(String name) throws FileSystemException {
	    return archive.lastModified();
	}

	/**
	 * Move data from existing archive to new archive
	 */
	private void recoverData() throws FileSystemException {
		try {
			archive.renameTo(new File("temp.zip"));
			zip = new ZipFile(new File("temp.zip"));
			Enumeration e = zip.entries();
			
			zos = new ZipOutputStream(new FileOutputStream(archive));

			while (e.hasMoreElements()) {
				ZipEntry entry = (ZipEntry)e.nextElement();
				BufferedInputStream bis = new BufferedInputStream(zip.getInputStream(entry));
				byte[] buffer = new byte[bis.available()];
				bis.read(buffer);
				bis.close();

				zos.putNextEntry(entry);
				zos.write(buffer);
			}
		} catch (IOException e) {
			throw new FileSystemException("IO exception", e);
		}
	}

	/**
	 * Inner class as wrapper for default BufferedOutputStream with different close() method
	 */
	private class InnerOutputStream extends BufferedOutputStream {
		public InnerOutputStream(OutputStream outputStream) {
			super(outputStream);
		}
		public void close() throws IOException {
			flush(); // write the buffer
		}
	}

}
