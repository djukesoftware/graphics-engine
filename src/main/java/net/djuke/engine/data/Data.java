package net.djuke.engine.data;

import net.djuke.engine.data.converters.ConvertException;
import net.djuke.engine.data.converters.Converter;
import net.djuke.engine.data.filesystems.EngineFileSystem;
import net.djuke.engine.data.filesystems.FileSystem;
import net.djuke.engine.data.filesystems.FileSystemException;
import net.djuke.engine.data.filesystems.OSFileSystem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.concurrent.FutureTask;

/**
 * Data - подсистема данных графического движка.
 *
 * @see Converter
 * @version 08/05/2017
 */
public class Data {

	/**
	 * File system of OS
	 */
	public static FileSystem 			OS_FILE_SYSTEM;
	/**
	 * File system of engine
	 */
    public static FileSystem 			ENGINE_FILE_SYSTEM;

	private static Logger 				logger;
	private static Package 				engineData;

	/**
	 * Запуск подсистемы данных.
	 */
	public static void startup() {
		logger = LogManager.getLogger(Data.class);
		engineData = new Package("engine");
		OS_FILE_SYSTEM = new OSFileSystem();
		ENGINE_FILE_SYSTEM = new EngineFileSystem();
	}

	public static void shutdown() {

	}

	/**
	 * Add new converter to list of converters
	 * @param converter converter for convert files
	 */
	public static void addConverter(Converter converter) {
		ConverterManager.getInstance().addConverter(converter);
	}

	/**
	 * Get resources of engine
	 * @return data of engine
	 */
	public static Package getEngineData() {
		return engineData;
	}

	/**
	 * Save object on file system
	 * @param object object for saving
	 * @param name name of file
	 * @throws UnknownFileFormatException if converter for this format don't exist
	 * @throws FileSystemException if program cannot get access to directory or parent directory don't exist
	 * @throws ConvertException if converter cannot convert object
	 */
	public static void saveObject(String name, Object object, FileSystem filesystem) throws IOException {
	    try {
			Converter converter = ConverterManager.getInstance().getConverterByFormat(getFormat(name));
			converter.save(object, filesystem.write(name));
		} catch (IOException e) {
			throw new IOException("File \"" + name + "\": " + e.getMessage());
		}
	}

	/**
	 * Load object from file system
	 * @param name name of file
	 * @param filesystem file system
	 * @throws UnknownFileFormatException if converter for this format don't exist
	 * @throws FileSystemException if program cannot get access to directory or parent directory don't exist
	 * @throws ConvertException if converter cannot convert object
	 * @return converted object
	 */
	public static Object loadObject(String name, FileSystem filesystem) throws IOException {
		try {
			Converter converter = ConverterManager.getInstance().getConverterByFormat(getFormat(name));
			return converter.load(filesystem.read(name));
		} catch (IOException e) {
			// Я тут добавил чуть больше информации поверх исключений
			throw new IOException("File \"" + name + "\": " + e.getMessage());
		}
	}

	/**
	 * Get map of objects loaded from files in specific package
	 * @param name name of package
	 * @param filesystem file system
	 * @return map of loaded objects
	 * @throws IOException if file not found
	 */
	public static Package loadPackage(String name, FileSystem filesystem) throws IOException {
	    try {
			// map is representation of package
			Package data = new Package(name);
			for (String file : filesystem.getFileList(name)) {
				if (file.split("\\.").length >= 2) {
					// Load file from file system and put to map
					data.put(file, loadObject(name + "/" + file, filesystem));
				}
			}
			return data;
		} catch (IOException e) {
			throw new IOException("Package \"" + name + "\": " + e.getMessage());
		}
	}

	/**
	 * Сохраняем целый пакет данных
	 * @param data пакет данных
	 * @param filesystem используемая файловая система
	 * @throws IOException в случае, если произошла ошибка во время сохранения
	 */
	public static void savePackage(Package data, FileSystem filesystem) throws IOException {
		try {
			for (String name : data.getObjectsList()) {
				saveObject(data.getName() + "/" + name, data.get(name), filesystem);
			}
		} catch (IOException e) {
			throw new IOException("Package \"" + data.getName() + "\": " + e.getMessage());
		}
	}

	/**
	 * Load object inside another thread
	 * @param name name of file
	 * @param filesystem file system
	 * @return result of loading
	 */
	public static FutureTask<Object> concurrentLoadObject(String name, FileSystem filesystem) {
	    FutureTask<Object> result = new FutureTask<>(() -> {
	    	try {
				return loadObject(name, filesystem);
			} catch (IOException e) {
	    		logger.error("File \"" + name + "\": " + e.getMessage());
	    		return null;
			}
	    });
	    Thread thread = new Thread(result);
	    thread.start();
	    return result;
	}

	/**
	 * Save object in another thread
	 * @param name name of file
	 * @param object object to save
	 * @param filesystem file system
	 */
	public static void concurrentSaveObject(String name, Object object, FileSystem filesystem) {
		Thread thread = new Thread(() -> {
			try {
				saveObject(name, object, filesystem);
			} catch (IOException e) {
				logger.error("File \"" + name + "\": " + e.getMessage());
			}
		});
		thread.start();
	}

	/**
	 * Load package in another thread
	 * @param name name of package for loading
	 * @param filesystem file system
	 * @return result
	 */
	public static FutureTask<Package> concurrentLoadPackage(String name, FileSystem filesystem) {
		FutureTask<Package> result = new FutureTask<>(() -> {
		    try {
		    	return loadPackage(name, filesystem);
			} catch (IOException e) {
		    	logger.error("Package \"" + name + "\": " + e.getMessage());
		    	return null;
			}
		});
		Thread thread = new Thread(result);
		thread.start();
		return result;
	}

	/**
	 * Save package in another package
	 * @param data package of data to save
	 * @param filesystem file system
	 */
	public static void concurrentSavePackage(Package data, FileSystem filesystem) {
		Thread thread = new Thread(() -> {
			try {
				savePackage(data, filesystem);
			} catch (IOException e) {
				logger.error("Package \"" + data.getName() + "\": " + e.getMessage());
			}
		});
		thread.start();
	}

	/**
	 * Get format of file
	 * @param name name of file
	 * @return format of file
	 */
	private static String getFormat(String name) {
		if (name.split("\\.").length > 1) return name.split("\\.")[name.split("\\.").length-1];
		else return null;
	}

}
