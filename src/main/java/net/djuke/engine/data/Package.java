package net.djuke.engine.data;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Package - storage and contains information about building other objects.
 *
 * @author Vadim
 * @version 1.0 01.09.2017
 */
public class Package {

    private Map<String, Object> files = new ConcurrentHashMap<>();
    private String name;

    /**
     * Create package for files
     * @param name name of package
     */
    public Package(String name) {
        this.name = name;
    }

    /**
     * Put object to package
     * @param name name of object
     * @param object object
     */
    public void put(String name, Object object) {
        files.put(name, object);
    }

    /**
     * Get object from package
     * @param name name of object
     * @return object
     * @throws IOException if object by this name not found
     */
    public Object get(String name) throws IOException {
        Object object = files.get(name);
        if (object == null) throw new FileNotFoundException("File " + name + " not found.");
        else return files.get(name);
    }

    /**
     * Копирует все объекты из одного пакеты в текущий
     * @param pack another package
     */
    public void putFrom(Package pack) {
        for (String objectName: pack.getObjectsList()) {
            try {
                put(objectName, pack.get(objectName));
            } catch (IOException e) {
                // just ignore this
            }
        }
    }

    public Collection<String> getObjectsList() {
        return files.keySet();
    }

    public void remove(String name) {
        files.remove(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
