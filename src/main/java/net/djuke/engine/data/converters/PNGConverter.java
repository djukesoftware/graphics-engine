package net.djuke.engine.data.converters;

import net.djuke.engine.graphics.components.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 * PNGConverter
 *
 * @author Vadim
 * @version 1.0 11.01.2018
 */
public class PNGConverter implements Converter<Image> {

    @Override
    public String getFormat() {
        return "png";
    }

    @Override
    public void save(Image object, BufferedOutputStream bos) throws ConvertException {
        BufferedImage image = object.getImage();
        try {
            ImageIO.write(image, "PNG", bos);
        } catch (IOException e) {
            throw new ConvertException(e);
        }
    }

    @Override
    public Image load(BufferedInputStream bis) throws ConvertException {
        return null;
    }
}
