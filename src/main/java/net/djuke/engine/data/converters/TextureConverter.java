package net.djuke.engine.data.converters;

import de.matthiasmann.twl.utils.PNGDecoder;
import net.djuke.engine.data.beans.TextureData;
import net.djuke.engine.graphics.components.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * TextureConverter - отвечает за сохранение и загрузку текстур (формат файлов txr).
 * Если вы хотите сохранить текстуру, то сначала конвертируйте её в изображение, а только потом передавайте в метод save().
 * @see net.djuke.engine.graphics.components.Image
 *
 * @author Vadim
 * @version 1.0 15.07.2017
 */
public class TextureConverter implements Converter {

    public String getFormat() {
        return "txr";
    }

    public void save(Object object, BufferedOutputStream bos) throws ConvertException {
        if (object instanceof Image) {
            BufferedImage image = ((Image)object).getImage();
            try {
                ImageIO.write(image, "PNG", bos);
            } catch (IOException e) {
                throw new ConvertException(e);
            }
        } else throw new ConvertException("Unknown type of input object.");
    }

    public Object load(BufferedInputStream bis) throws ConvertException {
        try {
            TextureData data = new TextureData();
            PNGDecoder decoder = new PNGDecoder(bis);

            ByteBuffer buffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            buffer.flip();

            data.setBuffer(buffer);
            data.setWidth(decoder.getWidth());
            data.setHeight(decoder.getHeight());
            bis.close();

            return data;
        } catch (IOException e) {
            throw new ConvertException(e);
        }
    }
}
