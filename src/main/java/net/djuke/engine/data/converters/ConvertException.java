package net.djuke.engine.data.converters;

import java.io.IOException;

/**
 * Exception of converting
 */
public class ConvertException extends IOException {

	public ConvertException() {
	}

	public ConvertException(String message) {
		super(message);
	}

	public ConvertException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConvertException(Throwable cause) {
		super(cause);
	}
}
