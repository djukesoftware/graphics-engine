package net.djuke.engine.data.converters;

import net.djuke.engine.data.beans.ShaderSource;
import net.djuke.engine.data.beans.ShaderSources;

import java.io.*;

/**
 * GLSLConverter
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class GLSLConverter implements Converter {

    public String getFormat() {
        return "glsl";
    }

    public void save(Object object, BufferedOutputStream bos) throws ConvertException {
        if (object instanceof ShaderSource) {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(bos));
            ShaderSource source = (ShaderSource)object;
            try {
                writer.write(source.toString());
                writer.close();
            } catch (IOException e) {
                throw new ConvertException(e);
            }
        } else throw new ConvertException("Input object is not shader source");
    }

    public Object load(BufferedInputStream bis) throws ConvertException {
        ShaderSources sources = new ShaderSources();
        ShaderSource source = null;
        BufferedReader reader = new BufferedReader(new InputStreamReader(bis));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                // #name nameOfShader
                if (line.contains("#") && line.contains("name")) {
                    // its just old derictive
                }
                // if we find entry with information about shader type
                // for example:
                // #type vertexShader
                else if (line.contains("#") && line.contains("type")) {
                    // then create shader with specific type
                    if (line.split(" ").length > 1) {
                        // Create new shader source with this type
                        source = new ShaderSource(line.split(" ")[1]);
                        // Add new shader to collection of shader sources
                        sources.addSource(source);
                    }
                } else if (source != null)
                    source.getSource().append(line.split("//")[0]).append("\n");
                else throw new ConvertException("Shader source don't contain line with information about type of shader");
            }
            reader.close();
        } catch (IOException e) {
            throw new ConvertException(e);
        }
        return sources;
    }

}
