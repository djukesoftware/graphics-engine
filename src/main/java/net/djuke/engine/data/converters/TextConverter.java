package net.djuke.engine.data.converters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;

/**
 * Convert objects to simple text file if object is a String.
 * @version 09/05/2017
 */
public class TextConverter implements Converter {

	public String getFormat() {
		return "txt";
	}

	/**
	 * Save object (String) to text file
	 * @param obj input object
	 * @param bos BufferedOutputStream from FileSystem
	 * @throws ConvertException if converters cannot convert this object to text file
	 */
	public void save(Object obj, BufferedOutputStream bos) throws ConvertException {
		String text = null;

		if (obj instanceof String) text = (String)obj;
		else throw new ConvertException("Object is not text");

		try {
			bos.write(text.getBytes());
			bos.close();
		} catch (IOException e) {
			throw new ConvertException(e);
		}
	}

	/**
	 * Load text file and convert them to object (String)
	 * @param bis BufferedInputStream from FileSystem
	 * @return new object
	 * @throws ConvertException if converters cannot convert this text file to object
	 */
	public Object load(BufferedInputStream bis) throws ConvertException {
		try {
			System.out.println("get available size of byte array");
			byte[] textByte = new byte[bis.available()];
			System.out.println("read bytes from stream");
			bis.read(textByte);
			System.out.println("close the stream");
			bis.close();
			return new String(textByte);
		} catch (IOException e) {
		    throw new ConvertException(e);
		}
	}
}
