package net.djuke.engine.data.converters;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/**
 * XMLConverter
 *
 * @author Vadim
 * @version 1.0 21.06.2017
 */
public class XMLConverter implements Converter {

    public String getFormat() {
        return "xml";
    }

    public Object load(BufferedInputStream bis) throws ConvertException {
        XMLDecoder decoder = new XMLDecoder(bis);
        Object result = decoder.readObject();
        decoder.close();
        return result;
    }

    public void save(Object object, BufferedOutputStream bos) throws ConvertException {
        XMLEncoder encoder = new XMLEncoder(bos);
        encoder.writeObject(object);
        encoder.close();
    }
}
