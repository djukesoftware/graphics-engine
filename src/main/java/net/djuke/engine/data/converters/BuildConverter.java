package net.djuke.engine.data.converters;

import net.djuke.engine.data.beans.BuildInformation;

import java.io.*;

/**
 * BuildConverter
 *
 * @author Vadim
 * @version 1.0 01.09.2017
 */
public class BuildConverter implements Converter {

    @Override
    public String getFormat() {
        return "build";
    }

    @Override
    public void save(Object object, BufferedOutputStream bos) throws ConvertException {
        throw new ConvertException("Is not possible");
    }

    @Override
    public Object load(BufferedInputStream bis) throws ConvertException {
        BuildInformation build = new BuildInformation();
        BufferedReader reader = new BufferedReader(new InputStreamReader(bis));
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                try {
                    line = line.replace(" ", "");
                    if (!line.startsWith("//")) {
                        String name = line.split("\\(")[0];
                        String type = line.split("\\(")[1].split("\\)")[0];
                        String[] sources = line.split("<")[1].split(",");
                        build.put(name, type, sources);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new ConvertException("Wrong syntax of .build file");
                }
            }
        } catch (IOException e) {
            throw new ConvertException(e);
        }
        return null;
    }
}
