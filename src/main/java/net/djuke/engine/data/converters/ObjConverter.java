package net.djuke.engine.data.converters;

import net.djuke.engine.data.beans.ModelData;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.io.*;

/**
 * ObjConverter
 *
 * @author Vadim
 * @version 1.0 04.07.2017
 */
public class ObjConverter implements Converter {

    public String getFormat() {
        return "obj";
    }

    public void save(Object object, BufferedOutputStream bos) throws ConvertException {
        if (object instanceof ModelData) {
            // TODO: Write methods for saving object
        } else throw new ConvertException("Input object is not model data");
    }

    public Object load(BufferedInputStream bis) throws ConvertException {
        try {
            ModelData data = new ModelData();
            BufferedReader reader = new BufferedReader(new InputStreamReader(bis));

            String line;
            while ((line = reader.readLine()) != null) {
                // Read vertices from file
                if (line.startsWith("v ")) {
                    data.addVertex(new Vector3f(
                            Float.valueOf(line.split(" ")[1]),
                            Float.valueOf(line.split(" ")[2]),
                            Float.valueOf(line.split(" ")[3])));
                }
                // Read normal from the file
                if (line.startsWith("vn ")) {
                    data.addNormal(new Vector3f(
                            Float.valueOf(line.split(" ")[1]),
                            Float.valueOf(line.split(" ")[2]),
                            Float.valueOf(line.split(" ")[3])));
                }
                // Read texture coordinates from the file
                if (line.startsWith("vt ")) {
                    data.addTextureCoords(new Vector2f(
                            Float.valueOf(line.split(" ")[1]),
                            -Float.valueOf(line.split(" ")[2])));
                }
                // Read faces information from the file
                if (line.startsWith("f ")) {

                    ModelData.Face face = new ModelData.Face();

                    try {
                        String[] values = line.split(" ")[1].split("/");
                        face.vertex1 = Integer.valueOf(values[0]) - 1;
                        face.texture1 = Integer.valueOf(values[1]) - 1;
                        face.normal1 = Integer.valueOf(values[2]) - 1;

                        values = line.split(" ")[2].split("/");
                        face.vertex2 = Integer.valueOf(values[0]) - 1;
                        face.texture2 = Integer.valueOf(values[1]) - 1;
                        face.normal2 = Integer.valueOf(values[2]) - 1;

                        values = line.split(" ")[3].split("/");
                        face.vertex3 = Integer.valueOf(values[0]) - 1;
                        face.texture3 = Integer.valueOf(values[1]) - 1;
                        face.normal3 = Integer.valueOf(values[2]) - 1;
                    } catch (NumberFormatException e) {
                        // do nothing
                    }

                    data.addFace(face);
                }
            }

            reader.close();

            data.createBuffers();

            return data;
        } catch (IOException e) {
            throw new ConvertException(e);
        }
    }
}
