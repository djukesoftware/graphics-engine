package net.djuke.engine.data.converters;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;

/**
 * Object converters
 * @version 08/05/2017
 */
public interface Converter<T> {
    String getFormat(); // get format of current converter
	void save(T object, BufferedOutputStream bos) throws ConvertException;
	T load(BufferedInputStream bis) throws ConvertException;
}
