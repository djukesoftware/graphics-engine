package net.djuke.engine.data.converters;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.joml.Vector2f;
import org.joml.Vector3f;

import java.io.*;

/**
 * JSONConverter
 *
 * @author Vadim
 * @version 1.0 29.08.2017
 */
public class JSONConverter implements Converter {

    private Gson gson;

    public JSONConverter() {
        /*
        GSON не умеет работать с некоторыми необходимыми нам типами и поэтому
        надо добавить несколько своих адаптеров
         */
        // В частности будем записывать вектора в более коротком виде
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Vector2f.class, new Vector2fAdapter());
        builder.registerTypeAdapter(Vector3f.class, new Vector3fAdapter());
        builder.setPrettyPrinting();

        gson = builder.create();
    }

    @Override
    public String getFormat() {
        return "json";
    }

    /**
     * Saves object to JSON file
     * @param object object for saving
     * @param bos output stream
     * @throws ConvertException
     */
    @Override
    public void save(Object object, BufferedOutputStream bos) throws ConvertException {
        OutputStreamWriter writer = new OutputStreamWriter(bos);
        try {
            writer.write("// " + object.getClass().getName() + "\n");
            writer.write(gson.toJson(object));
            writer.close();
        } catch (IOException e) {
            throw new ConvertException(e);
        }
    }

    @Override
    public Object load(BufferedInputStream bis) throws ConvertException {
        Class<?> objectClass = null; // class of returned object

        // Read source of json file
        BufferedReader reader = new BufferedReader(new InputStreamReader(bis));
        StringBuilder json = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("// ")) {
                    objectClass = Class.forName(line.split(" ")[1]);
                } else json.append(line);
            }
        } catch (ClassNotFoundException | IOException e) {
            throw new ConvertException(e);
        }

        // Return object if file contain information about his class
        try {
            if (objectClass != null) return gson.fromJson(json.toString(), objectClass);
            else throw new ConvertException("File don't contain any information about class");
        } catch (JsonSyntaxException e) {
            throw new ConvertException(e.getMessage());
        }
    }

    /**
     * Специальный адаптер для более компактной записи векторов в JSON
     */
    public static class Vector2fAdapter extends TypeAdapter<Vector2f> {

        @Override
        public void write(JsonWriter out, Vector2f value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            String entry = value.x + "," + value.y;
            out.value(entry);
        }

        @Override
        public Vector2f read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String[] entry = in.nextString().split(",");
            return new Vector2f(Float.parseFloat(entry[0]), Float.parseFloat(entry[1]));
        }
    }

    /**
     * Специальный адаптер для более компактной записи векторов в JSON
     */
    public static class Vector3fAdapter extends TypeAdapter<Vector3f> {

        @Override
        public void write(JsonWriter out, Vector3f value) throws IOException {
            if (value == null) {
                out.nullValue();
                return;
            }
            String entry = value.x + "," + value.y + "," + value.z;
            out.value(entry);
        }

        @Override
        public Vector3f read(JsonReader in) throws IOException {
            if (in.peek() == JsonToken.NULL) {
                in.nextNull();
                return null;
            }
            String[] entry = in.nextString().split(",");
            return new Vector3f(Float.parseFloat(entry[0]), Float.parseFloat(entry[1]), Float.parseFloat(entry[2]));
        }
    }
}
