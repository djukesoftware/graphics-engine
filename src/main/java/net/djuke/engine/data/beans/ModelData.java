package net.djuke.engine.data.beans;

import org.joml.Vector2f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * ModelData contain information about vertices, normals, texture coordinates of 3D model.
 *
 * @author Vadim
 * @version 1.0 04.07.2017
 */
public class ModelData implements Bean {

    private List<Vector3f> vertices = new ArrayList<>();
    private List<Vector3f> normals = new ArrayList<>();
    private List<Vector2f> textures = new ArrayList<>();
    private List<Face> faces = new ArrayList<>();

    private FloatBuffer vertexBuffer;
    private FloatBuffer normalBuffer;
    private FloatBuffer textureBuffer;

    private FloatBuffer tangentBuffer;
    private FloatBuffer bitangentBuffer;

    private int vertexCount;


    public void addVertex(Vector3f vertex) {
        vertices.add(vertex);
    }

    public void addNormal(Vector3f normal) {
        normals.add(normal);
    }

    public void addTextureCoords(Vector2f texture) {
        textures.add(texture);
    }

    public void addFace(Face face) {
        faces.add(face);
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public FloatBuffer getTangentBuffer() {
        return tangentBuffer;
    }

    public FloatBuffer getBitangentBuffer() {
        return bitangentBuffer;
    }

    public FloatBuffer getVertexBuffer() {
        return vertexBuffer;
    }

    public FloatBuffer getNormalBuffer() {
        return normalBuffer;
    }

    public FloatBuffer getTextureBuffer() {
        return textureBuffer;
    }

    public void createBuffers() {
        tangentBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
        bitangentBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
        Vector3f delta1 = new Vector3f();
        Vector3f delta2 = new Vector3f();
        Vector2f uvDelta1 = new Vector2f();
        Vector2f uvDelta2 = new Vector2f();
        for (Face face : faces) {
            // Get vertices of this face
            Vector3f vertex1 = vertices.get(face.vertex1);
            Vector3f vertex2 = vertices.get(face.vertex2);
            Vector3f vertex3 = vertices.get(face.vertex3);

            // Get uv coordinates
            Vector2f uv1 = textures.get(face.texture1);
            Vector2f uv2 = textures.get(face.texture2);
            Vector2f uv3 = textures.get(face.texture3);

            // Calculate delta
            delta1.set(
                    vertex2.x() - vertex1.x(),
                    vertex2.y() - vertex1.y(),
                    vertex2.z() - vertex1.z() );
            delta2.set(
                    vertex3.x() - vertex1.x(),
                    vertex3.y() - vertex1.y(),
                    vertex3.z() - vertex1.z() );

            // Calculate delta for texture coordinates
            uvDelta1.set(
                    uv2.x() - uv1.x(),
                    uv2.y() - uv1.y() );
            uvDelta2.set(
                    uv3.x() - uv1.x(),
                    uv3.y() - uv1.y() );

            float r = 1.0f / (uvDelta1.x() * uvDelta2.y() - uvDelta1.y() * uvDelta2.x());

            float tangentX = (delta1.x() * uvDelta2.y() - delta2.x() * uvDelta1.y()) * r;
            float tangentY = (delta1.y() * uvDelta2.y() - delta2.y() * uvDelta1.y()) * r;
            float tangentZ = (delta1.z() * uvDelta2.y() - delta2.z() * uvDelta1.y()) * r;

            tangentBuffer.put(tangentX).put(tangentY).put(tangentZ);
            tangentBuffer.put(tangentX).put(tangentY).put(tangentZ);
            tangentBuffer.put(tangentX).put(tangentY).put(tangentZ);

            float bitangentX = (delta2.x() * uvDelta1.x() - delta1.x() * uvDelta2.x()) * r;
            float bitangentY = (delta2.y() * uvDelta1.x() - delta1.y() * uvDelta2.x()) * r;
            float bitangentZ = (delta2.z() * uvDelta1.x() - delta1.z() * uvDelta2.x()) * r;

            bitangentBuffer.put(bitangentX).put(bitangentY).put(bitangentZ);
            bitangentBuffer.put(bitangentX).put(bitangentY).put(bitangentZ);
            bitangentBuffer.put(bitangentX).put(bitangentY).put(bitangentZ);
        }
        tangentBuffer.flip();
        bitangentBuffer.flip();

        vertexBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
        for (Face face : faces) {
            Vector3f vertex = vertices.get(face.vertex1);
            vertexBuffer.put(vertex.x());
            vertexBuffer.put(vertex.y());
            vertexBuffer.put(vertex.z());

            vertex = vertices.get(face.vertex2);
            vertexBuffer.put(vertex.x());
            vertexBuffer.put(vertex.y());
            vertexBuffer.put(vertex.z());

            vertex = vertices.get(face.vertex3);
            vertexBuffer.put(vertex.x());
            vertexBuffer.put(vertex.y());
            vertexBuffer.put(vertex.z());
        }
        vertexBuffer.flip(); // close writing data to buffer
        vertices = null;


        normalBuffer = BufferUtils.createFloatBuffer(faces.size() * 9);
        for (ModelData.Face face : faces) {
            Vector3f normal;
            normal = normals.get(face.normal1);
            normalBuffer.put(normal.x());
            normalBuffer.put(normal.y());
            normalBuffer.put(normal.z());

            normal = normals.get(face.normal2);
            normalBuffer.put(normal.x());
            normalBuffer.put(normal.y());
            normalBuffer.put(normal.z());

            normal = normals.get(face.normal3);
            normalBuffer.put(normal.x());
            normalBuffer.put(normal.y());
            normalBuffer.put(normal.z());
        }
        normalBuffer.flip();
        normals = null;

        textureBuffer = BufferUtils.createFloatBuffer(faces.size() * 6);
        for (ModelData.Face face : faces) {
            Vector2f texture;
            texture = textures.get(face.texture1);
            textureBuffer.put(texture.x());
            textureBuffer.put(texture.y());

            texture = textures.get(face.texture2);
            textureBuffer.put(texture.x());
            textureBuffer.put(texture.y());

            texture = textures.get(face.texture3);
            textureBuffer.put(texture.x());
            textureBuffer.put(texture.y());
        }
        textureBuffer.flip();
        textures = null;
        vertexCount = faces.size() * 3;
        faces = null;
    }

    /**
     * Face contains ID of each vertex, normal and texture coordinates.
     */
    public static class Face {
        public int vertex1;
        public int vertex2;
        public int vertex3;

        public int normal1;
        public int normal2;
        public int normal3;

        public int texture1;
        public int texture2;
        public int texture3;
    }
}
