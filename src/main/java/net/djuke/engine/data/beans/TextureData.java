package net.djuke.engine.data.beans;

import org.liquidengine.legui.image.ImageChannels;
import org.liquidengine.legui.image.LoadableImage;

import java.nio.ByteBuffer;

/**
 * TextureData - object for contain texture data
 *
 * @author Vadim
 * @version 1.0 15.07.2017
 */
public class TextureData extends LoadableImage implements Bean {

    private ByteBuffer buffer;
    private int width;
    private int height;

    public ByteBuffer getBuffer() {
        return buffer;
    }

    public void setBuffer(ByteBuffer buffer) {
        this.buffer = buffer;
    }

    public int getWidth() {
        return width;
    }

    public void load() {

    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public ByteBuffer getImageData() {
        return buffer;
    }

    public ImageChannels getChannels() {
        return ImageChannels.RGB;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
