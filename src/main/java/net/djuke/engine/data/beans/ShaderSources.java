package net.djuke.engine.data.beans;

import net.djuke.engine.graphics.components.Shader;

import java.util.ArrayList;
import java.util.List;

/**
 * ShaderSources - contain collection of all shader sources for creating one valid shader program.
 * @see ShaderSource
 * @see Shader
 *
 * @author Vadim
 * @version 1.0 03.08.2017
 */
public class ShaderSources implements Bean{

    private List<ShaderSource> sources = new ArrayList<>();

    public void addSource(ShaderSource source) {
        sources.add(source);
    }

    public List<ShaderSource> getSources() {
        return sources;
    }
}
