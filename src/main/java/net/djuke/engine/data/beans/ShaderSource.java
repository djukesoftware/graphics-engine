package net.djuke.engine.data.beans;

/**
 * ShaderSource - contain source of OpenGL shader with specific type (vertexShader, fragmentShader and others).
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class ShaderSource implements Bean {

    private StringBuilder builder = new StringBuilder();
    private String type;

    /**
     * Create shader source with type (vertexShader, fragmentShader, geometryShader and others)
     * @param type type of shader
     */
    public ShaderSource(String type) {
        this.type = type;
    }

    /**
     * Get source of shader as a string
     * @return shader source inside string
     */
    public StringBuilder getSource() {
        return builder;
    }

    /**
     * Include source of one shader to source of another shader
     * @param source included shader source
     */
    public void include(ShaderSource source) {
        builder.insert(builder.indexOf("\n"), "\n" + source.getSource().toString());
    }

    /**
     * Get type of current shader
     * @return type of shader
     */
    public String getType() {
        return type;
    }

}
