package net.djuke.engine.data.beans;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * ResourcesInformation - contain information about how to build objects from several files
 *
 * @author Vadim
 * @version 1.0 01.09.2017
 */
public class BuildInformation implements Bean {

    private List<Entry> entries = new ArrayList<>();

    public BuildInformation() {

    }

    /**
     * Put entry about building information
     * @param name name of output object
     * @param type type of output object
     * @param sources sources for building this object
     */
    public void put(String name, String type, String... sources) {
        entries.add(new Entry(name, type, sources));
    }

    public List<Entry> getEntries() {
        return entries;
    }

    public class Entry {
        private String name;
        private String type;
        private List<String> sources;

        public Entry(String name, String type, String... sources) {
            this.name = name;
            this.type = type;
            this.sources = Arrays.asList(sources);
        }
    }

}
