package net.djuke.engine.data;

import net.djuke.engine.data.converters.*;

import java.util.ArrayList;

/**
 * Create converters by name of file format
 * Pattern: Factory method
 * @version 08/05/2017
 */
class ConverterManager {

	private static ConverterManager instance;

	private ArrayList<Converter> converters = new ArrayList<>();

	private ConverterManager() {
		converters.add(new TextConverter());
		converters.add(new XMLConverter());
		converters.add(new JSONConverter());
		converters.add(new BuildConverter());
		converters.add(new GLSLConverter());
		converters.add(new ObjConverter());
		converters.add(new TextureConverter());
		converters.add(new PNGConverter());
	}

	/**
	 * Get converter by format of file
	 * @param format - file format
	 * @return new converter
	 * @throws UnknownFileFormatException if converter for this format not found or if format is null
	 */
	Converter getConverterByFormat(String format) throws UnknownFileFormatException {
		if (format == null) throw new UnknownFileFormatException("Empty file format. Impossible to find converter.");
		Converter result = null;
	    for (Converter converter : converters) {
	    	if (converter.getFormat().equals(format)) result = converter;
		}
		if (result != null) return result;
		else throw new UnknownFileFormatException("Converter for " + format + " file format not found.");
	}

	/**
	 * Add new converter from plugin
	 * @param converter new converter
	 */
	public void addConverter(Converter converter) {
		converters.add(converter);
	}

	public static ConverterManager getInstance() {
		if (instance == null) instance = new ConverterManager();
		return instance;
	}
}
