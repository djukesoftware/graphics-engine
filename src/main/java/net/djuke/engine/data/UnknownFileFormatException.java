package net.djuke.engine.data;

import java.io.IOException;

public class UnknownFileFormatException extends IOException {

    public UnknownFileFormatException() {

    }

    public UnknownFileFormatException(String message) {
        super(message);
    }

    public UnknownFileFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnknownFileFormatException(Throwable cause) {
        super(cause);
    }
}

