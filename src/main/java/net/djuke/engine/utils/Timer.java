package net.djuke.engine.utils;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

/**
 * Timer - simple timer for events
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class Timer {

    private Action action;
    private double start;
    private double duration;

    private boolean loop = false;
    private boolean end = false;

    public Timer(double duration, boolean loop) {
        this.duration = duration;
        this.loop = loop;
    }

    public void update() {
        if (loop) {
            if (glfwGetTime() > start + duration) {
                start = glfwGetTime();
                if (action != null) action.run();
            }
        } else {
            if (!end & glfwGetTime() > start + duration) {
                end = true;
                if (action != null) action.run();
            }
        }
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
