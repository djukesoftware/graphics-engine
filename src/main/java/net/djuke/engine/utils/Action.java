package net.djuke.engine.utils;

/**
 * Action
 *
 * @author Vadim
 * @version 1.0 05.07.2017
 */
public interface Action {

    void run();
}
