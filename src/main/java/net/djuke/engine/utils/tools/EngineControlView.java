package net.djuke.engine.utils.tools;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.texture.Frame;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.Slider;
import org.liquidengine.legui.component.TextArea;
import org.liquidengine.legui.component.Widget;

/**
 * EngineControlView
 *
 * @author Vadim
 * @version 1.0 11.10.2017
 */
public class EngineControlView extends Widget {

    private static EngineControlView instance;

    private TextArea output;

    private EngineControlView() {
        super(0,0, Graphics.interfaceUnitToPixels(5), Graphics.interfaceUnitToPixels(5));
        getTitleTextState().setText("Engine Control");
        setCloseable(false);

        int unit = Graphics.interfaceUnitToPixels(1);
        float buttonSize = unit * (5-0.2f);

        float current = unit * 0.4f;
        output = new TextArea();
        output.setPosition(unit * 0.1f, current);
        output.setSize(buttonSize, unit);
        output.setEditable(false);
        add(output);

        /**
        Button terminal = new Button("Terminal");
        terminal.setSize(buttonSize, unit * 0.5f);
        terminal.setPosition(unit * 0.1f, current);
        add(terminal);
         **/

        current += unit * (0.5f + 0.1f);
        /**
        Button graphicsSettings = new Button("GraphicsSettings");
        graphicsSettings.setSize(buttonSize, unit * 0.5f);
        graphicsSettings.setPosition(unit * 0.1f, current);
        add(graphicsSettings);
         **/

        current += unit * (0.5f + 0.1f);
        Label info = new Label("Contrast");
        info.setPosition(unit * 0.1f, current);
        add(info);
        Slider slider = new Slider(unit * 0.1f, current + unit * 0.05f, buttonSize, unit * 0.5f);
        slider.setValue(Graphics.getSettings().getContrast() * 50f);
        slider.addSliderChangeValueEventListener((d) -> {
            Graphics.getSettings().setContrast(slider.getValue() / 50f);
        });
        add(slider);

        current += unit * (0.5f + 0.1f);
        info = new Label("Brightness");
        info.setPosition(unit * 0.1f, current);
        add(info);
        Slider slider2 = new Slider(unit * 0.1f, current + unit * 0.05f, buttonSize, unit * 0.5f);
        slider2.setValue(Graphics.getSettings().getBrightness() * 100f);
        slider2.addSliderChangeValueEventListener((d) -> {
            Graphics.getSettings().setBrightness(slider2.getValue() / 100f);
        });
        add(slider2);

        current += unit * (0.5f + 0.1f);
        info = new Label("Gamma");
        info.setPosition(unit * 0.1f, current);
        add(info);
        Slider slider3 = new Slider(unit * 0.1f, current + unit * 0.05f, buttonSize, unit * 0.5f);
        slider3.setValue(Graphics.getSettings().getGamma() * 50f);
        slider3.addSliderChangeValueEventListener((d) -> {
            Graphics.getSettings().setGamma(slider3.getValue() / 50f);
        });
        add(slider3);
    }

    public void update() {
        output.getTextState().setText("");
        for (Frame frame : Graphics.getWindow().getFrameList()) {
            output.getTextState().append(Graphics.getWindow().getFrameList().indexOf(frame));
            output.getTextState().append(": ");
            output.getTextState().append(frame.getName());
            output.getTextState().append("\n");
        }
    }


    public static EngineControlView getInstance() {
        if (instance == null) instance = new EngineControlView();
        return instance;
    }
}
