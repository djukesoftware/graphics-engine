package net.djuke.engine.utils.tools;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import net.djuke.engine.script.ScriptManager;
import org.liquidengine.legui.component.*;
import org.liquidengine.legui.event.KeyEvent;
import org.liquidengine.legui.listener.KeyEventListener;
import org.lwjgl.glfw.GLFW;

import java.lang.reflect.Method;

/**
 * JSEngineView
 *
 * @author Vadim
 * @version 1.0 29.08.2017
 */
public class JSEngineView {

    private ScriptManager engine = ScriptManager.getInstance();

    public JSEngineView(InterfaceFrame gui) {
        // Main window for all components
        Widget window = new Widget();
        int size = Graphics.interfaceUnitToPixels(5);
        window.setSize(size, size);
        window.setPosition(0,0);
        window.setTitleEnabled(true);
        window.getTitleTextState().setText("JavaScript Engine");

        window.setMinimized(true);

        // Padding between components
        final float padding = 0.3f;

        // Create input field for javascript source
        TextInput input = new TextInput();
        size = Graphics.interfaceUnitToPixels(5f - padding * 2);
        input.setSize(size, Graphics.interfaceUnitToPixels(padding));
        input.setPosition(Graphics.interfaceUnitToPixels(padding), Graphics.interfaceUnitToPixels(padding * 2));
        window.add(input);

        // TextArea for output of JS engine
        TextArea output= new TextArea();
        size = Graphics.interfaceUnitToPixels(5f - padding * 2);
        output.setEditable(false);
        output.getTextState().setText("Katerina, hello!\nI love you");
        output.setPosition(Graphics.interfaceUnitToPixels(padding), Graphics.interfaceUnitToPixels(1.2f));
        output.setSize(size, Graphics.interfaceUnitToPixels(3.5f));
        window.add(output);

        // Run JS code if user press Enter
        input.getListenerMap().addListener(KeyEvent.class, (KeyEventListener) event -> {
            if (event.getKey() == GLFW.GLFW_KEY_ENTER && event.getAction() == GLFW.GLFW_RELEASE) {
                output.getTextState().setText(engine.eval(input.getTextState().getText()));
                input.getTextState().setText("");
            }
        });

        // Panel for on-fly help
        Panel panel = new Panel();
        panel.setSize(Graphics.interfaceUnitToPixels(5 - padding*2), Graphics.interfaceUnitToPixels(padding));
        panel.setPosition(Graphics.interfaceUnitToPixels(padding), input.getPosition().y() + Graphics.interfaceUnitToPixels(padding));
        panel.setCornerRadius(0);
        panel.setVisible(false);

        // Proposed java objects
        Label note = new Label();
        note.getTextState().setText("nothing");
        note.setPosition(Graphics.interfaceUnitToPixels(0.1f), Graphics.interfaceUnitToPixels(0.1f));
        panel.add(note);
        window.add(panel);

        // Search java objects with same name
        input.getListenerMap().addListener(KeyEvent.class, (KeyEventListener) event -> {
            if (event.getAction() == GLFW.GLFW_RELEASE) {
                boolean isExist = false;
                for (String current : engine.getBindings()) {
                    if (!input.getTextState().getText().equals("")) {
                        // Сначала делим текущую строку на точки
                        if (input.getTextState().getText().contains("\\.")) {
                            String[] cmds = input.getTextState().getText().split("\\.");
                        } else {
                            // Если нет точек
                            if (current.startsWith(input.getTextState().getText())) {
                                note.getTextState().setText(current);
                                isExist = true;
                                break;
                            }
                        }

                        // Ищем методы похожие по имени
                        if (input.getTextState().getText().contains(current) && input.getTextState().getText().split("\\.").length == 2) {
                            String methodName = input.getTextState().getText().split("\\.")[1];
                            for (Method method : engine.getBindingClass(current).getMethods()) {
                                if (method.getName().startsWith(methodName)) {
                                    note.getTextState().setText(method.getName());
                                    isExist = true;
                                }
                            }
                            break;
                        }
                    }
                }
                if (!isExist) panel.setVisible(false);
                else panel.setVisible(true);
            }
        });

        gui.getContainer().add(window);
    }
}
