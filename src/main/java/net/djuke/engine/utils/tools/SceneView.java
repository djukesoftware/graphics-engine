package net.djuke.engine.utils.tools;

import net.djuke.engine.graphics.Graphics;
import net.djuke.engine.graphics.components.camera.Camera;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.scene.SceneObject;
import net.djuke.engine.graphics.gui.InterfaceFrame;
import org.liquidengine.legui.component.Label;
import org.liquidengine.legui.component.TextArea;
import org.liquidengine.legui.component.Widget;

/**
 * SceneView
 *
 * @author Vadim
 * @version 1.0 11.09.2017
 */
public class SceneView {

    private Label cameraInformation = new Label();
    private TextArea output;
    private Scene scene;

    public SceneView(Scene scene, InterfaceFrame gui) {
        this.scene = scene;
        // Main window for all components
        Widget window = new Widget();
        int size = Graphics.interfaceUnitToPixels(5);
        window.setSize(size, size);
        window.setPosition(0,0);
        window.setTitleEnabled(true);
        window.getTitleTextState().setText("Scene view: " + scene.getClass().getSimpleName());

        window.setMinimized(true);

        final float padding = 0.3f;
        cameraInformation.getTextState().setText("Camera: ...");
        cameraInformation.setPosition(Graphics.interfaceUnitToPixels(padding), Graphics.interfaceUnitToPixels(padding * 2));
        window.add(cameraInformation);

        Label objectInformation = new Label("Objects:");
        objectInformation.setPosition(Graphics.interfaceUnitToPixels(padding), Graphics.interfaceUnitToPixels(padding * 3));
        window.add(objectInformation);

        output= new TextArea();
        output.setPosition(Graphics.interfaceUnitToPixels(padding), objectInformation.getPosition().y + Graphics.interfaceUnitToPixels(padding));
        output.setSize(Graphics.interfaceUnitToPixels(4.4f), Graphics.interfaceUnitToPixels(3.5f));
        output.setEditable(false);
        window.add(output);

        gui.getContainer().add(window);
    }

    public void update() {
        Camera camera = scene.getCamera();
        cameraInformation.getTextState().setText("Camera: " + camera.getClass().getSimpleName());

        output.getTextState().setText("");
        for (SceneObject object : scene.getObjects()) {
            output.getTextState().append(object.getName());
            output.getTextState().append("\n");
        }

    }
}
