package net.djuke.engine.utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Statistics
 *
 * @author Vadim
 * @version 1.0 11.10.2017
 */
public class Statistics {

    private List<Integer> values = new ArrayList<>();

    public void addValue(int value) {
        values.add(value);
    }

    public void save() {
        File file = new File("data.plot");
        try {
            FileOutputStream fos = new FileOutputStream(file);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(fos));
            int counter = 0;
            for (int value : values) {
                writer.write(counter + " " + value);
                writer.newLine();
                counter++;
            }
            writer.close();
        } catch (IOException e) {

        }
    }
}
