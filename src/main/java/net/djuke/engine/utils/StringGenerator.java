package net.djuke.engine.utils;

import java.util.Random;

/**
 * StringGenerator - generates strings
 *
 * @author Vadim
 * @version 1.0 03.10.2017
 */
public class StringGenerator {

    private static char[] wordG = { 'a', 'e', 'i', 'o', 'u'}; // Гласные
    private static char[] wordS = { 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'w', 'z'};
    private static Random random = new Random();

    public static String generateName() {
        StringBuilder name = new StringBuilder();
        for (int counter = 0; counter < 2 + random.nextInt(4); counter++) {
            if (counter == 0) name.append(Character.toUpperCase(wordS[random.nextInt(wordS.length)]));
            else name.append(wordS[random.nextInt(wordS.length)]);
            name.append(wordG[random.nextInt(wordG.length)]);
        }
        return name.toString();
    }
}
