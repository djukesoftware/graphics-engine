package net.djuke.engine.old.error;

import javafx.fxml.FXML;
import javafx.scene.control.TextArea;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * Created by Vadim on 06.07.2016.
 */
public class Controller {

    @FXML
    TextArea errorDisplay;

    public void openOfficialPage() {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null) {
            try {
                desktop.browse(URI.create("https://vk.com/djuke"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void openIssueTracker() {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null) {
            try {
                desktop.browse(URI.create("https://bitbucket.org/vadimgush/perihelion/issues"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendEmail() {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null) try {
            desktop.mail(new URI("mailto:support@djuke.net?subject=Bug"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
