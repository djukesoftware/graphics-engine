package net.djuke.engine.old.error;

/**
 * ErrorDisplay - отображает ошибку в отдельном окне.
 * А так же информацию о том, как можно решить ошибку или куда стоит
 * обратиться за её решением
 * Created by Vadim on 06.07.2016.
 */

import net.djuke.engine.old.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;

public class ErrorDisplay extends Application implements Runnable {

    public static void show() {
        // Воспроизводим системный звук, чтобы уведомить пользователя
        // Работает только для Windows
        final Runnable runnable = (Runnable) Toolkit.getDefaultToolkit().getDesktopProperty("win.sound.exclamation");
        if (runnable != null) runnable.run();

        // Запускаем отдельный поток для того, чтобы игра не зависла фоном
        Thread thread = new Thread(new ErrorDisplay());
        thread.start();
    }

    public void run() {
        launch(new String[]{});
    }

    @Override
    public void start(Stage primaryStage) {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Application.fxml"));
        try {
            Parent root = fxmlLoader.load();
            Controller controller = fxmlLoader.getController();
            controller.errorDisplay.setText(Logger.lastError);

            primaryStage.setTitle("Perihelion Error");
            primaryStage.setScene(new Scene(root, 472, 400));
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
