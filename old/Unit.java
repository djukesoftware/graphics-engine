package net.djuke.engine.gui;

import net.djuke.engine.input.Mouse;
import net.djuke.engine.math.Vector3;

import java.util.ArrayList;

/**
 * Unit - composite pattern for GUI
 *
 * @author Vadim
 * @version 1.0 04.05.2017
 */
public abstract class Unit {

    protected Vector3 position = new Vector3();
    protected Vector3 size = new Vector3();
    protected ActionListener actionListener;
    protected ArrayList<Unit> units;
    protected Unit parent;

    // two flags for rendering
    protected boolean hover = false;
    protected boolean push = false;
    protected boolean release = false;
    protected Display display;

    public Unit(Display display) {
        this.display = display;
    }

    public Vector3 getPosition() {
        return position;
    }

    public Vector3 getSize() {
        return size;
    }

    public int getWidth() {
        return (int)size.getX();
    }

    public int getHeight() {
        return (int)size.getY();
    }

    public void setWidth(float width) {
        size.setX(width);
    }

    public void setHeight(float height) {
        size.setY(height);
    }

    public void setPosition(float x, float y) {
        position.set(x, y, 0);
    }

    public void setRelativePosition(float x, float y) {
        if (getParent() != null) position.set(getParent().getPosition().x + x, getParent().getPosition().y + y, 0);
        else setPosition(x, y);
    }

    /**
     * Render unit or all units from units collection
     */
    abstract void render();

    abstract void addUnit(Unit unit);

    abstract void removeUnit(Unit unit);

    /**
     * Update instance depending on the mouse position
     */
    void updateInstance(Mouse mouse) {
        // get absolute position
        float absX = getPosition().x;
        float absY = getPosition().y;
        // if unit is hover
        if (mouse.getPosition().x > absX &&
                mouse.getPosition().y > absY &&
                mouse.getPosition().x < absX + getSize().x &&
                mouse.getPosition().y < absY + getSize().y) {
            release = mouse.isLeftButtonRelease();
            push = mouse.isLeftButtonPush();
            hover = true;
            if (units != null) { // updateGraphicParameters instance of units
                Unit first = null; // updateGraphicParameters instance of first unit
                // Check every unit in array
                for (int i=units.size()-1; i>=0; i--) {
                    if (units.get(i).isHover(mouse.getPosition())) {
                        first = units.get(i);
                        break;
                    }
                }
                // After find unit
                if (first != null) first.updateInstance(mouse);
            }
        }
    }

    public void setPush(boolean push) {
        this.push = push;
    }

    public void setHover(boolean hover){
        this.hover = hover;
    }

    public void setRelease(boolean release) {
        this.release = release;
    }

    public void setParent(Unit parent) {
        this.parent = parent;
    }

    public Unit getParent() {
        return parent;
    }

    public void setActionListener(ActionListener actionListener) {
        this.actionListener = actionListener;
    }

    /**
     * Method for Display class
     * @param mouse - position of mouse
     * @return is hover
     */
    protected boolean isHover(Vector3 mouse) {
        float absX = getPosition().x;
        float absY = getPosition().y;
        if (mouse.x > absX && mouse.y > absY && mouse.x < absX + getSize().x && mouse.y < absY + getSize().y) {
            return true;
        } else return false;
    }

}
