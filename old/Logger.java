package net.djuke.engine.old;

import net.djuke.engine.old.error.ErrorDisplay;

/**
 * net.djuke.engine.old.Logger - класс для логирования
 * Created by vadim on 10.06.16.
 */
public class Logger {

    public static String lastError;

    public static void print(String message) {
        System.out.println("[log] " + message);
    }

    public static void error(String message) {
        System.out.println("\u001B[31m" + "[err] " + message + "\u001B[0m");
    }

    public static void displayError(String message) {
        lastError = message;
        ErrorDisplay.show();
    }

}
