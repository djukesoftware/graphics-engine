package net.djuke.engine.old.utils;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.old.graphic.gui.Window;
import net.djuke.engine.old.graphic.gui.components.Text;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Button;
import net.djuke.engine.old.graphic.gui.interactiveComponents.CheckButton;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Input;

/**
 * Created by Vadim on 09.07.2016.
 */
public class SettingsWindow implements CoreObject {


    private static SettingsWindow instance = new SettingsWindow();

    private Window window;
    private Input displayWidth;
    private Input displayHeight;

    private SettingsWindow() {
        Core.addObject(this);

        window = new Window();
        window.setSize(250, 300);
        window.setTitle("Game settings");
        window.setPadding(10);

        displayHeight = new Input();
        displayHeight.setText("Height: ");
        displayHeight.setWidth(100);
        displayHeight.getPosition().set(110, window.getHeight() - 25, 0);
        displayHeight.setInputText(Integer.toString(Settings.getInstance().height));

        displayWidth = new Input();
        displayWidth.setText("Width: ");
        displayWidth.setWidth(100);
        displayWidth.getPosition().set(0, window.getHeight() - 25, 0);
        displayWidth.setInputText(Integer.toString(Settings.getInstance().width));

        Text resolutionText = new Text();
        resolutionText.setText("Display resolution: ");
        resolutionText.getPosition().set(0, window.getHeight(), 0);

        window.addComponent(resolutionText);
        window.addInteractiveComponent(displayWidth);
        window.addInteractiveComponent(displayHeight);

        /* Настройка полноэкранного режима */
        Text fullscreenText = new Text();
        fullscreenText.setText("Fullscreen");
        fullscreenText.getPosition().set(25, window.getHeight() - 50 - 2, 0);
        window.addComponent(fullscreenText);

        CheckButton fullscreen = new CheckButton();
        fullscreen.setPosition(0, window.getHeight() - 72, 0);
        fullscreen.setTrigger(true);
        window.addInteractiveComponent(fullscreen);

        /* Настройка MSAA сглаживания */
        Text msaaText = new Text("MSAA (smooth)");
        msaaText.getPosition().set(25, window.getHeight() - 74 - 2);
        window.addComponent(msaaText);

        CheckButton msaa = new CheckButton();
        msaa.getPosition().set(0, window.getHeight() - 72 - 18 - 5);
        window.addInteractiveComponent(msaa);

        /* Предупреждающий текст */
        Text warningText = new Text();
        warningText.setText("Warning: changes will apply\nafter reboot.");
        warningText.getPosition().set(0, window.getHeight() - 76 - 44, 0);
        window.addComponent(warningText);

        Button saveButton = new Button("Save", 75, 25);
        saveButton.setColor(new Color(0.1f, 0.3f, 0.1f, 1f));
        saveButton.getPosition().set(window.getWidth() - saveButton.getWidth(), 0);
        window.addInteractiveComponent(saveButton);

        Button closeButton = new Button("Close", 75, 25);
        closeButton.setColor(new Color(0.3f, 0.1f, 0.1f, 1f));
        window.addInteractiveComponent(closeButton);
    }

    public void update() {

    }

    public void delete() {

    }

    public void close() {
        window.closeWindow();
    }

    public static SettingsWindow getInstance() {
        return instance;
    }

}
