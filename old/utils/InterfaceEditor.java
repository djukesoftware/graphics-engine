package net.djuke.engine.old.utils;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.TextSystem;
import net.djuke.engine.old.graphic.gui.Window;
import net.djuke.engine.old.graphic.gui.WindowSystem;
import net.djuke.engine.old.graphic.gui.components.Text;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Button;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Mouse;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static net.djuke.engine.old.graphic.GraphicUtils.enableTextures;
import static org.lwjgl.opengl.GL11.GL_QUADS;

/**
 * InterfaceDebug -
 * Created by vadim on 05.07.16.
 */
public class InterfaceEditor implements CoreObject {

    private static InterfaceEditor instance;
    private int windowID = 0;

    private Window windowInfo;
    private Text text;
    private Button deleteWindow;
    private Button hideWindow;
    private Button closeEditor;

    private InterfaceEditor() {
        Core.addObject(this);

        windowInfo = new Window();
        windowInfo.setTitle("Window Control");
        windowInfo.setSize(200, 200);
        windowInfo.setPadding(10);
        text = new Text();
        text.setText("" +
                "Window id: \n" +
                "Height: \n" +
                "Width: \n" +
                "Padding: \n" +
                "Position: \n" );
        text.getPosition().setY(windowInfo.getHeight());
        windowInfo.addComponent(text);

        deleteWindow = new Button("Delete", 75, 30);
        deleteWindow.setColor(new Color(0.5f, 0, 0));
        hideWindow = new Button("Hide", 75, 30);
        hideWindow.getPosition().set(85, 0, 0);
        closeEditor = new Button("Close editor", 100, 30);
        closeEditor.getPosition().set(0, 40, 0);
        closeEditor.setColor(new Color(0.5f, 0.25f, 0));
        windowInfo.addInteractiveComponent(deleteWindow);
        windowInfo.addInteractiveComponent(hideWindow);
        windowInfo.addInteractiveComponent(closeEditor);

        // Обрабатываем собития с кнопки
        closeEditor.addHandler(new Button.Handler() {
            public void push() {windowInfo.closeWindow();}
            public void hover() {}
        });

        deleteWindow.addHandler(new Button.Handler() {
            public void push() {WindowSystem.getInstance().deleteWindow(windowID);}
            public void hover() {}
        });
    }

    public void update() {
        /** Отрисовка текста рядом с курсором*/
        disableTextures();
        // Фон
        Quad.getInstance().changeColors(
                new Color(0, 0, 0, 0.9f), new Color(0, 0, 0, 0.9f),
                new Color(0, 0, 0, 0.9f), new Color(0, 0, 0, 0.9f));
        Quad.getInstance().render(
                new Vector(10 + Mouse.getMouse().getX(), Mouse.getMouse().getY() - 22 * 2, 0),
                TextSystem.getInstance().getWidth("x: " + (int)Mouse.getMouse().getX() + " y: " + (int)Mouse.getMouse().getY()) + 30,
                22 * 2, GL_QUADS);

        enableTextures();
        // Текст
        TextSystem.getInstance().render(
                new Vector(Mouse.getMouse().getX() + 20, Mouse.getMouse().getY(), 0),
                "x: " + (int)Mouse.getMouse().getX() + " y: " + (int)Mouse.getMouse().getY(),
                new Color(1, 1, 1));
        if (WindowSystem.getInstance().getActiveWindow() != null) {
            TextSystem.getInstance().render(
                    new Vector(Mouse.getMouse().getX() + 20, Mouse.getMouse().getY() - 22, 0),
                    "window id: " + WindowSystem.getInstance().getActiveWindow().getId(),
                    new Color(1, 1, 1));
        }

        /** Вывод информации об окне **/
        if (WindowSystem.getInstance().getActiveWindow() != null) {
            if (!WindowSystem.getInstance().getActiveWindow().equals(windowInfo)) {
                text.setText("" +
                        "Window id: " + WindowSystem.getInstance().getActiveWindow().getId() +
                        "\nHeight: " + WindowSystem.getInstance().getActiveWindow().getHeight() +
                        "\nWidth: " + WindowSystem.getInstance().getActiveWindow().getWidth() +
                        "\nPadding: " + WindowSystem.getInstance().getActiveWindow().getPadding() +
                        "\nPosition: ");
                windowID = WindowSystem.getInstance().getActiveWindow().getId();
            }
        }

    }

    public void delete() { }

    public void close() {
        windowInfo.closeWindow();
    }

    public static InterfaceEditor getInstance() {
        if (instance == null) instance = new InterfaceEditor();
        return instance;
    }

}
