package net.djuke.engine.old.utils;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static org.lwjgl.opengl.GL11.*;

/**
 * WorldGrid - отрисовка сетки в трёхмерном пространстве
 * Created by vadim on 18.06.16.
 */
public class WorldGrid {

    private static float alpha = 1f;
    private static int size = 50;

    public static void render() {
        disableTextures();
        for (int x = -size; x <= size; x++) {
            glBegin(GL_LINES);
            glColor4f(1, 1, 1, alpha);
            glVertex3f(x, 0, -size);
            glVertex3f(x, 0, size);
            glEnd();
        }
        for (int z = -size; z <= size; z++) {
            glBegin(GL_LINES);
            glVertex3f(-size, 0, z);
            glVertex3f(size, 0, z);
            glEnd();
        }
    }

    public static void setAlpha(float alpha) {
        WorldGrid.alpha = alpha;
    }

    public static void setSize(int size) {
        WorldGrid.size = size;
    }
}
