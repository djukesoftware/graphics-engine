package net.djuke.engine.old.utils;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Script;
import net.djuke.engine.old.graphic.gui.Window;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Input;
import net.djuke.engine.old.graphic.gui.components.Text;

/**
 * JSEngine - графическая оболочка для JavaScript движка
 * Created by vadim on 12.06.16.
 */
public class JSEngine implements CoreObject {

    private static JSEngine instance;
    private boolean enable = false;
    private Window window;
    private Input input;
    private Text text;

    private JSEngine() {
        Core.addObject(this);

        window = new Window();
        window.setSize(300, 100);
        window.setTitle("JavaScript Engine");
        window.setPadding(5);

        text = new Text();
        text.setText("Output:");
        text.getPosition().setY(window.getHeight() - 22);

        input = new Input();
        input.setText("In: ");
        input.setWidth(300);
        input.getPosition().setY(window.getHeight());

        window.addInteractiveComponent(input);
        window.addComponent(text);
    }

    public void update() {
        if (input.isDone()) {
            text.setText(Script.getInstance().run(input.getInputText()));
        }
    }

    public void delete() {

    }

    public void close() {
        window.closeWindow();
    }

    public static JSEngine getInstance() {
        if (instance == null) instance = new JSEngine();
        return instance;
    }

}
