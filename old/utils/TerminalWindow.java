package net.djuke.engine.old.utils;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.gui.Window;
import net.djuke.engine.old.graphic.gui.components.Image;
import net.djuke.engine.old.graphic.gui.components.Text;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Button;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Input;
import net.djuke.engine.math.Vector;

/**
 * Отрисовка терминала на экране
 * Created by Vadim on 12.07.2016.
 */
public class TerminalWindow {

    private static TerminalWindow instance;

    private TerminalWindow() {
        Window window = new Window();
        window.setSize(250, 300);
        window.setPosition(new Vector(10, Settings.getInstance().height - window.getHeight() - 10));

        Image header = new Image("terminalHead.png");
        header.setSize(250, 100);
        header.setPosition(new Vector(0, window.getHeight() - 100));
        window.addComponent(header);

        Input input = new Input();
        input.setWidth(230);
        input.getPosition().set(10, window.getHeight() - 70);
        input.setText("In: ");
        window.addInteractiveComponent(input);

        Button clear = new Button("Clear", 75, 25);
        clear.getPosition().set(10, 10);
        window.addInteractiveComponent(clear);

        Text text = new Text();
        text.setText("Output will be here in future.");
        text.getPosition().set(10, window.getHeight() - 100);
        window.addComponent(text);
    }

    public static TerminalWindow getInstance() {
        if (instance ==  null) instance = new TerminalWindow();
        return instance;
    }

}
