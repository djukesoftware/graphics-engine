package net.djuke.engine.old.utils;

import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Information;
import net.djuke.engine.old.Core;
import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.Window;
import net.djuke.engine.old.graphic.gui.components.Graphic;
import net.djuke.engine.old.graphic.gui.components.Text;

import static org.lwjgl.glfw.GLFW.glfwGetTime;

/**
 * SystemWindow - системное окно с технической информацией
 * Created by vadim on 12.06.16.
 */
public class SystemWindow implements CoreObject {

    private static SystemWindow instance;
    private Text text = new Text();
    private Graphic graphic = new Graphic();
    private Window window;

    private SystemWindow() {
        Core.addObject(this);

        // Создаём окно
        window = new Window();
        window.setSize(200, 200);
        window.setPadding(5);
        window.setTitle("System info");
        // Создаём график
        graphic.setLenght(50);
        graphic.getPosition().set(0, 11, 0);
        // Добавляем график в окно
        window.addComponent(graphic);

        // Устанавлиаем цвет текста
        text.setColor(new Color(1, 1, 1, 1));
        // Добавляем текст в окно
        window.addComponent(text);

        // Устанавливаем текст
        text.setText(
                "Time: " + String.format("%.2f", glfwGetTime()) +
                        "\nFPS: " + Core.getFps() +
                        "\nSystem: " + Information.getInstance().os +
                        "\nSys. version: " + Information.getInstance().osVersion +
                        "\nOpenGL version: " + Information.getInstance().glVersion +
                        "\nGLSL version: " + Information.getInstance().glShadingLanguageVersion
        );
        window.setSize(text.getWidth(), text.getHeight() + 22 + 100);

        graphic.setWidth(window.getWidth());
        text.getPosition().set(0, window.getHeight(), 0);

        window.setPosition(new Vector(10, Settings.getInstance().height - window.getWindowHeight() - 22 - 10, 0));
    }


    public void update() {
            text.setText(
                    "Time: " + String.format("%.2f", glfwGetTime()) +
                            "\nFPS: " + Core.getFps() +
                            "\nSystem: " + Information.getInstance().os +
                            "\nSys. version: " + Information.getInstance().osVersion +
                            "\nOpenGL version: " + Information.getInstance().glVersion +
                            "\nGLSL version: " + Information.getInstance().glShadingLanguageVersion
            );

            if (Core.isSecondPas()) graphic.addVariable((int) (Core.getFps() / 60f * 100f));
    }

    public void delete() { }

    public void close() {
        window.closeWindow();
    }

    public static SystemWindow getInstance() {
        if (instance == null) instance = new SystemWindow();
        return instance;
    }

}
