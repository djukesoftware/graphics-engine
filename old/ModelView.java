package net.djuke.engine.graphics;

import net.djuke.engine.graphics.components.Mesh;
import net.djuke.engine.graphics.components.Shader;
import net.djuke.engine.graphics.components.camera.Camera;
import net.djuke.engine.graphics.components.scene.Scene;
import net.djuke.engine.graphics.components.texture.Texture;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

/**
 * ModelView - view of model
 *
 * @deprecated
 * @author Vadim
 * @version 1.0 04.08.2017
 */
public class ModelView {

    private FloatBuffer modelMatrixBuffer;
    private Matrix4f modelMatrix = new Matrix4f();

    private Vector3f position = new Vector3f();
    private Vector3f rotation = new Vector3f();

    private List<Texture> textures = new ArrayList<>();
    private Mesh model;
    private Shader shader;

    public ModelView(Mesh model) {
        this.model = model;
        modelMatrixBuffer = BufferUtils.createFloatBuffer(16);
        updateModelMatrix();
    }

    public void setShader(Shader shader) {
        this.shader = shader;
    }

    public void addTexture(Texture texture) {
        textures.add(texture);
    }

    public void translate(float x, float y, float z) {
        position.set(x, y, z);
    }

    public void rotate(float x, float y, float z) {
        rotation.set(x, y, z);
    }

    public void render(Scene context) {
        if (shader != null) {
            // Bind model shader
            shader.bind();

            // Bind all textures to render state
            int textureid = 0;
            for (Texture texture : textures) {
                texture.bind(textureid);
                shader.sendTexture(texture.getName(), textureid);
                textureid++;
            }

            // Create new transformation matrix for this model
            updateModelMatrix();
            // Send matrix to model shader
            shader.sendMatrix4f("modelMatrix", modelMatrixBuffer);

            // Send camera parameters to shader
            Camera camera = context.getCamera();
            shader.sendMatrix4f("projectionMatrix", camera.getProjectionMatrixBuffer());
            shader.sendMatrix4f("viewMatrix", camera.getViewMatrixBuffer());
        }

        // Render current model
        model.render();

        if (shader != null) shader.unbind();
    }

    public Matrix4f getModelMatrix() {
        return modelMatrix;
    }

    private void updateModelMatrix() {
        modelMatrix.translation(position)
                .rotate(rotation.x, 1, 0, 0)
                .rotate(rotation.y, 0, 1, 0)
                .rotate(rotation.z, 0, 0, 1);
        // update matrix buffer
        modelMatrix.get(modelMatrixBuffer);
    }
}
