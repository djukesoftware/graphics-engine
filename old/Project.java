package net.djuke.engine.old;

/**
 * Game - интерфейс для проекта движка
 * Created by vadim on 18.06.16.
 */
public interface Project {

    void load();
    void update();
    void end();

}
