package net.djuke.engine.utils;

import net.djuke.engine.data.DataManager;
import net.djuke.engine.data.beans.ShaderSources;
import net.djuke.engine.graphics.components.Shader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * LiveShader - instrument for live shader coding. Update shader program every half second.
 *
 * <p>
 *     Example of using:
 *     {@code
 *          // Creating data manager for this demo
 *          DataManager data = new DataManager(new OSFileSystem());
 *          Model = new Quad(); // creating model
 *          // Create live shader and add path of shader sources
 *          LiveShader liveShader = new LiveShader(data);
 *          liveShader.addShaderPath("shader.fs.glsl");
 *          liveShader.addShaderPath("shader.vs.glsl:);
 *
 *          while (true) {
 *               // Update instance of live shader and get last version of shader program for model
 *               liveShader.update();
 *               liveShader.getShader().bind();
 *               model.render();
 *               liveShader.getShader().unbind();
 *          }
 *     }
 * </p>
 *
 * @author Vadim
 * @version 1.0 05.07.2017
 */
public class LiveShader {

    private Shader shader;
    private DataManager data;
    private String path = null;

    private long sum = 0;
    private Logger logger = LogManager.getLogger("LiveShader");
    private Timer timer = new Timer(0.5, true);

    public LiveShader(DataManager data) {
        this.data = data;
        // Set action of updating shader program every half second
        timer.setAction(() -> {
            long sum = 0;
            try {
                if (path != null) sum = this.data.lastModified(path);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
            // If some file is updateGraphicParameters
            if (sum != this.sum) {
                logger.info("Shaders was modified");
                this.sum = sum;
                if (path != null) {
                    try {
                        // Loading shaders sources
                        ShaderSources sources = (ShaderSources) this.data.loadObject(path);
                        // Create valid shader program
                        shader = new Shader(sources);
                    } catch (IOException e) {
                        logger.error(e.getMessage());
                    }
                }
            }
        });
    }

    public void addShaderSource(String path) {
        this.path = path;
    }

    public void update() {
        timer.update();
    }

    public Shader getShader() {
        return shader;
    }
}
