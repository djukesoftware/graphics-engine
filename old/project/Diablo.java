package net.djuke.engine.project;

import net.djuke.engine.old.graphic.Model;
import net.djuke.engine.old.graphic.Shader;
import net.djuke.engine.old.graphic.Texture;
import net.djuke.engine.old.graphic.world.World;
import net.djuke.engine.old.graphic.world.WorldObject;

import static org.lwjgl.glfw.GLFW.glfwGetTime;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;

/**
 * Diablo - отрисовка тестовой модели с дьяволом
 * Created by Vadim on 10.07.2016.
 */
public class Diablo implements WorldObject {

    private Shader shader = new Shader();
    private Texture texture = new Texture("model/diabloTexture.png");
    private Texture normalTexture = new Texture("model/diabloNormals.png");
    private Texture glowTexture = new Texture("model/diabloGlow.png");
    private Model model = new Model();
    private Matrix4 matrix = new Matrix4();

    public Diablo() {
        World.getInstance().addObject(this);
        model.load("model/diablo.obj", true, false);
        shader.loadFragmentShader("shaders/modelShader.fs.glsl");
        shader.loadVertexShader("shaders/modelShader.vs.glsl");
        //shader.loadTessellationShader("shaders/modelShader.ts.glsl");
        //shader.loadTessellationEvaluationShader("shaders/modelShader.tes.glsl");
        shader.createShaderProgram();

       matrix = new Matrix4(new float[]{
               1, 1, 1, 1,
               1, 1, 1, 1,
               1, 1, 1, 1,
               1, 1, 1, 1
       });
    }

    public void render() {
        shader.bind();

        shader.uniformFloat("time", (float)glfwGetTime());
        shader.uniformMatrix("gameMatrix", matrix.getFloatBuffer());
        texture.bindTexture(1);
        shader.uniformTexture("texture", 0);

        model.render(GL_TRIANGLES);
        shader.unbind();
    }

}
