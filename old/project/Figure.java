package net.djuke.engine.project;

import net.djuke.engine.old.graphic.gui.components.Component;
import net.djuke.engine.old.math.MathUtils;
import net.djuke.engine.math.Vector;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static org.lwjgl.opengl.GL11.*;

/**
 * Просто класс для тестирования матриц
 * Created by Vadim on 09.07.2016.
 */
public class Figure implements Component {

    private Vector[] vertices;

    public Figure() {
        /* Только надо сделать уточнение
         * чтобы двухмерный вектор перемещать вдоль оси
         * мы должны перевести его в трёхмерное измерение
         *
         * Аналогично будем делать и с трёхмерным вектором
         * перенося его в четырёхмерное измерение
        */
        // Пусть у нас будет треугольник
        vertices = new Vector[]{
                new Vector(0, 20, 1),
                new Vector(20, 0, 1),
                new Vector(20, -20, 1),
                new Vector(-20, -20, 1),
                new Vector(-20, 20, 1)
        };

        float alpha = 1.7f / 2;
        // Ну а теперь создаём матрицу
        Matrix2 matrix = new Matrix2(
                (float)Math.cos(alpha), (float)-Math.sin(alpha),
                (float)Math.sin(alpha), (float)Math.cos(alpha));

        // Так теперь создадим матрицу три на три для переноса
        Matrix3 matrix3 = new Matrix3(
                (float)Math.cos(alpha), (float)-Math.sin(alpha), 100,
                (float)Math.sin(alpha), (float)Math.cos(alpha), 0,
                0, 0, 1
        );

        // Теперь умножаем матрицу на вектор
        for (int count = 0; count < vertices.length; count++) {
            vertices[count] = MathUtils.vectorMatrix(vertices[count], matrix3);
        }
    }

    public void render() {

        disableTextures();
        // Отрисуем его на экране
        glColor4f(1, 1, 1, 1);
        glBegin(GL_LINE_LOOP);
        for (Vector vertex : vertices) {
            glVertex2f(vertex.getX(), vertex.getY());
        }
        glEnd();
    }

}
