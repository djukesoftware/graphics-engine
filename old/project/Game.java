package net.djuke.engine.project;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.Project;
import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.*;
import net.djuke.engine.old.graphic.gui.Notice;
import net.djuke.engine.old.graphic.gui.Window;
import net.djuke.engine.old.graphic.gui.components.Image;
import net.djuke.engine.old.graphic.gui.components.ProgressBar;
import net.djuke.engine.old.graphic.gui.components.Text;
import net.djuke.engine.old.graphic.gui.interactiveComponents.Button;
import net.djuke.engine.old.input.Mouse;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.utils.*;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Game - главный игровой класс
 * Created by vadim on 18.06.16.
 */
public class Game implements Project {

    // Переменная и конструктор для запуска проекта
    private static Game instance = new Game();

    // Все остальное для проекта
    public Shader shader = new Shader(); // шейдер для изображения

    private ProgressBar progressBar = new ProgressBar();

    public static Game getInstance() {
        return instance;
    }

    public void load() {

        // Прогружаем шейдеры для изображения
        shader.loadFragmentShader("shaders/shader.fs.glsl");
        shader.loadVertexShader("shaders/shader.vs.glsl");
        shader.createShaderProgram();

        Window window = new Window();
        window.setSize(100, 100);
        window.setPadding(10);
        window.setTitle("Matrix test");
        window.setPosition(new Vector(
                Settings.getInstance().width / 2f,
                Settings.getInstance().height / 2f));

        Figure figure = new Figure();
        window.addComponent(figure);
        window.setHide(false);

        /** Создание окна с текстом **/
        Window textWindow = new Window();
        textWindow.setSize(220 + 10, 220 + 10);
        // Название желательно
        textWindow.setTitle("Testing windows");
        // Самый лучший отсутп
        textWindow.setPadding(10);
        textWindow.setPosition(new Vector(
                Settings.getInstance().width / 2 - textWindow.getWindowWidth() / 2 - 10,
                Settings.getInstance().height / 2 - textWindow.getWindowHeight() / 2 - 10, 0));

        // Загружаем изображения
        // Создаём изображения
        Image image = new Image("image.png");
        image.setSize(100, 100);
        image.setPosition(new Vector(0, 0, 0));
        image.setBorder(new Color(1, 1, 1, 0.5f), 2);
        // подключаем к изображению шейдер
        image.bindImageShader(shader);
        textWindow.addComponent(image);

        // Добавляем фото
        image = new Image("image.png");
        image.setSize(100, 100);
        image.setPosition(new Vector(110, 0, 0));
        image.setBorder(new Color(1, 1, 1, 0.25f), 2);
        textWindow.addComponent(image);

        Button plusButton = new Button("+", 22, 22);
        plusButton.setColor(new Color(0.1f, 0.5f, 0.1f, 1));
        plusButton.getPosition().set(0, textWindow.getHeight());
        textWindow.addInteractiveComponent(plusButton);

        plusButton.addHandler(new Button.Handler() {
            public void push() { }
            public void hover() { Notice.getInstance().show("Add new window"); }
        });

        /** Создание окна с фотографией и кнопками **/
        Window photoWindow = new Window();
        photoWindow.enableClose(false);
        photoWindow.setSize(256, 320);
        photoWindow.setPosition(new Vector(
                Settings.getInstance().width - photoWindow.getWindowWidth() - 10, 10, 0
        ));

        // создаём ещё одно новое изображение
        Image photo = new Image("image2.png");

        photo.setSize(256, 320);
        photo.setPosition(new Vector(0, 0, 0));
        photo.setBorder(new Color(1, 1, 1, 0.5f), 2);
        photoWindow.addComponent(photo);

        Button button = new Button("Press me", 100, 30);
        button.getPosition().set(10, 10, 0);
        photoWindow.addInteractiveComponent(button);
        photoWindow.setPadding(10);
        photoWindow.setTitle("Image and button");

        Button warning = new Button("Do not press", 100, 30);
        warning.setColor(new Color(0.5f, 0.25f, 0));
        warning.getPosition().set(120, 10, 0);
        photoWindow.addInteractiveComponent(warning);

        textWindow.setHide(true);
        photoWindow.setHide(true);

        //SettingsWindow.getInstance();
        TerminalWindow.getInstance();

        /* Новое окно с тестовым изоюржением **/
        Window headWindow = new Window();
        headWindow.setSize(250, 200);
        //headWindow.setTitle("Mining");
        headWindow.setPadding(0);

        Image header = new Image("windowHead.png");
        header.setPosition(new Vector(0, headWindow.getHeight() - 100));
        header.setSize(250, 100);
        headWindow.addComponent(header);

        Button drillButton = new Button("Start", 60, 26);
        drillButton.setColor(new Color(0.1f, 0.3f, 0.1f, 1f));
        drillButton.getPosition().set(70, 10);
        headWindow.addInteractiveComponent(drillButton);

        Button closeButton = new Button("Close", 60, 26);
        //closeButton.setColor(new Color(0.3f, 0.1f, 0.1f, 1f));
        closeButton.getPosition().set(10, 10);
        headWindow.addInteractiveComponent(closeButton);

        Text sampleText = new Text();
        sampleText.setText("Hoc est exemplum eam mos\nvultus amo a fenestra.");
        sampleText.getPosition().set(10, headWindow.getHeight() - 90);
        headWindow.addComponent(sampleText);

        new Diablo();
    }

    public void update() {
        // Обновление прогресс бара
        progressBar.setProgress( (int)(( (1f + Math.sin(glfwGetTime())) / 2f) * 100f) );

        shader.bind();
        // Передача новых данных в шейдерную программу изображения
        shader.uniformFloat("k_var", ((float)Math.sin(glfwGetTime()) + 1f) / 2f);
        shader.unbind();

        // Обновление камеры
        Camera.getInstance().getPosition().setY(0);
        if (glfwGetKey(Core.window, GLFW_KEY_W) == 1)
            Camera.getInstance().moveForward(0.1f);
        if (glfwGetKey(Core.window, GLFW_KEY_S) == 1)
            Camera.getInstance().moveForward(-0.1f);
        if (glfwGetKey(Core.window, GLFW_KEY_A) == 1)
            Camera.getInstance().strafe(0.1f);
        if (glfwGetKey(Core.window, GLFW_KEY_D) == 1)
            Camera.getInstance().strafe(-0.1f);
        if (glfwGetKey(Core.window, GLFW_KEY_E) == 1)
            Camera.getInstance().getRotation().setY(Camera.getInstance().getRotation().getY() + Mouse.getMouseSpeed().getX());
    }

    public void end() {

    }
}
