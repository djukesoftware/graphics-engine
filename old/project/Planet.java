package net.djuke.engine.project;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.graphic.Model;
import net.djuke.engine.old.graphic.Shader;
import net.djuke.engine.old.graphic.world.World;
import net.djuke.engine.old.graphic.world.WorldObject;
import net.djuke.engine.old.input.Mouse;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_F;
import static org.lwjgl.glfw.GLFW.glfwGetKey;
import static org.lwjgl.glfw.GLFW.glfwGetTime;

/**
 * Planet - просто класс планеты
 * Created by Vadim on 07.07.2016.
 */
public class Planet implements WorldObject {

    private Shader modelShader = new Shader(); // шейдер для модели
    private Model model = new Model(); // модель

    public Planet() {
        World.getInstance().addObject(this);

        // Прогружаем шейдеры для модели
        modelShader.loadFragmentShader("shaders/modelShader.fs.glsl");
        modelShader.loadVertexShader("shaders/modelShader.vs.glsl");
        modelShader.createShaderProgram();

        // Прогрузка модели
        model.load("model/planet.obj", true, false);
    }

    public void render() {
        /** Отрисовка модели на экране **/
        modelShader.bind(); // подключаем шейдер для модели
        modelShader.attributeVector("offset", 0, 1f + (float)Math.sin(glfwGetTime()), 0, 0);

        // Устанавливаем местоположение модели
        model.getPosition().set(0, 1, 0);
        // Варащение моделью по нажатию кнопки
        if (glfwGetKey(Core.window, GLFW_KEY_F) == 1) {
            model.getRotation().set(model.getRotation().getX() - Mouse.getMouseSpeed().getY() / 2,
                    model.getRotation().getY() + Mouse.getMouseSpeed().getX() / 2, 0);
        }
        // Отрисовываем модель
        // model.render(GL_LINES);
        modelShader.unbind();
    }

}
