package net.djuke.engine.gui;

/**
 * Button
 *
 * @author Vadim
 * @version 1.0 11.05.2017
 */
public class Button extends Unit {

    public Button(Display display) {
        super(display);
    }

    public void render() {
        // Do action if  ...
        if (push) actionListener.action(ActionListener.ACTION_TYPE.PUSH);
        if (hover) actionListener.action(ActionListener.ACTION_TYPE.HOVER);
        if (release) actionListener.action(ActionListener.ACTION_TYPE.RELEASE);

        // render this component


        // Update flags for rendering new frame
        setPush(false);
        setHover(false);
        setRelease(false);
    }

    public void addUnit(Unit unit) {
        // do nothing
    }

    public void removeUnit(Unit unit) {
        // do nothing
    }

}
