package net.djuke.engine.gui;

import net.djuke.engine.input.Keyboard;
import net.djuke.engine.input.Mouse;

import java.util.ArrayList;

/**
 * Display
 *
 * @author Vadim
 * @version 1.0 24.05.2017
 */
public class Display {

    private Mouse mouse;
    private Keyboard keyboard;

    private ArrayList<Unit> units = new ArrayList<>();

    public void addUnit(Unit unit) {
        units.add(unit);
    }

    public void setMouse(Mouse mouse) {
        this.mouse = mouse;
    }

    public void setKeyboard(Keyboard keyboard) {
        this.keyboard = keyboard;
    }

    public void render() {
        updateInstance(); // updateGraphicParameters instance of each element
        units.forEach(Unit::render); // render every unit
    }

    private void updateInstance() {
        Unit first = null; // this is first unit on the current display
        // Check all units
        for (int i = units.size()-1; i >= 0; i--) {
            if (units.get(i).isHover(mouse.getPosition())) {
                first = units.get(i);
                break;
            }
        }
        // If we find first unit on the screen
        if (first != null) {
            first.updateInstance(mouse);
        }
    }

    public Mouse getMouse() {
        return mouse;
    }

    public Keyboard getKeyboard() {
        return keyboard;
    }

}
