package net.djuke.engine.gui;

import java.util.ArrayList;

/**
 * Pane
 *
 * @author Vadim
 * @version 1.0 12.05.2017
 */
public class Pane extends Unit {

    public Pane(Display display) {
        super(display);
    }

    public void render() {
        units.forEach(Unit::render); // just render all units inside pane
        setPush(false);
        setHover(false);
    }

    public void addUnit(Unit unit) {
        if (units == null) units = new ArrayList<>();
        unit.setParent(this);
        units.add(unit);
    }

    public void removeUnit(Unit unit) {
        if (units == null) units = new ArrayList<>();
        unit.setParent(null);
        units.remove(unit);
    }
}
