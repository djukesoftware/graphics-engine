package net.djuke.engine.gui;

/**
 * ActionListener
 *
 * @author Vadim
 * @version 1.0 24.05.2017
 */
public interface ActionListener {
    enum ACTION_TYPE { HOVER, PUSH, RELEASE };
    void action(ACTION_TYPE actionType);
}
