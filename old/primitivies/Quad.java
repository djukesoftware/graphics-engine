package net.djuke.engine.graphics.primitivies;

import net.djuke.engine.graphics.data.beans.ModelData;
import net.djuke.engine.math.Vector2;
import net.djuke.engine.math.Vector3;

/**
 * Quad
 *
 * @author Vadim
 * @version 1.0 04.07.2017
 */
public class Quad {

    private static ModelData data = null;

   public static ModelData getModel() {
        if (data == null) {
             data = new ModelData();
             // one normal for two faces
             data.addNormal(new Vector3(0, 0, 1));

             data.addVertex(new Vector3(-1f, 1f, 0));
             data.addVertex(new Vector3(1f, 1f, 0));
             data.addVertex(new Vector3(1f, -1f, 0));
             data.addVertex(new Vector3(-1f, -1f, 0));

             data.addTextureCoords(new Vector2(0, 0));
             data.addTextureCoords(new Vector2(1, 0));
             data.addTextureCoords(new Vector2(1, 1));
             data.addTextureCoords(new Vector2(0, 1));

             ModelData.Face face = new ModelData.Face();
             face.normal1 = 0;
             face.normal2 = 0;
             face.normal3 = 0;

             face.vertex1 = 0;
             face.texture1 = 0;

             face.vertex2 = 1;
             face.texture2 = 1;

             face.vertex3 = 2;
             face.texture3 = 2;
             data.addFace(face);

             face = new ModelData.Face();
             face.normal1 = 0;
             face.normal2 = 0;
             face.normal3 = 0;

             face.vertex1 = 0;
             face.texture1 = 0;

             face.vertex2 = 2;
             face.texture2 = 2;

             face.vertex3 = 3;
             face.texture3 = 3;
             data.addFace(face);

             data.createBuffers();
        }
        return data;
    }

}
