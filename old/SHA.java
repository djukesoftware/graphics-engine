package net.djuke.engine.old;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * SHA - просто класс для хеширования сообщений
 */

public class SHA {

    // Хешируем сообщение
    public static String toHash(String input) {
        // Преобразуем в HEX строку
        StringBuffer result = new StringBuffer();
        try {
            // Хешируем сообщение по алгоритму 256
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            // Получаем массив байт
            byte[] hash = md.digest(input.getBytes("UTF-8"));
            for (byte byt : hash) {
                result.append(Integer.toString((byt & 0xff) + 0x100, 16).substring(1)); }
            hash = null;
            md = null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(); }
        return result.toString();
    }

}
