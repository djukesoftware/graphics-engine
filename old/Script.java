package net.djuke.engine.old;

import javax.script.*;

/**
 * Script - JavaScript движок
 * Created by vadim on 10.06.16.
 */
public class Script {

    public static Script instance;

    private ScriptEngine engine = null;
    private Bindings binds = null;
    // Режим отладки
    public boolean debug = false;

    private Script() {
        init();
    }

    private void init() {
        Logger.print("Init JavaScript Engine...");
        // Создаём скриптовый движок
        engine = new ScriptEngineManager().getEngineByName("javascript");
        // Линковка классов
        binds = engine.getBindings(ScriptContext.GLOBAL_SCOPE);
    }

    // Линкуем новые классы
    public void bindClass(String name, Object object) {
        if (debug) Logger.print(name + " " + object.toString());
        binds.put(name, object);
    }

    // Запускаем скрипт
    public String run(String command) {
        if (debug) Logger.print(command);
        String result = "";
        try {
            // Выполнение команды
            try {
                result = engine.eval(command).toString();
            } catch (NullPointerException e) {
                // Просто проигнорирую
            }
        } catch (ScriptException e) {
            e.printStackTrace();
        }
        if (result == null) result = "empty";
        return result;
    }

    public static Script getInstance() {
        if (instance == null) instance = new Script();
        return instance;
    }

}
