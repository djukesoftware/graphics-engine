package net.djuke.engine.old;

import static org.lwjgl.opengl.GL11.GL_VERSION;
import static org.lwjgl.opengl.GL11.glGetString;
import static org.lwjgl.opengl.GL20.GL_SHADING_LANGUAGE_VERSION;

/**
 * Information - класс для сбора технической информации
 * Created by vadim on 10.06.16.
 */
public class Information {

    private static Information instance;

    public String glVersion;
    public String glShadingLanguageVersion;
    public double glSLVersionShort;
    public String os;
    public String osVersion;
    public String pcid;
    public String username;

    private Information() {}

    // Генерируем информацию
    public void genInformation() {
        // Вывод версии OpenGL
        Logger.print("Getting information about OpenGL...");
        Logger.print("OpenGL version: " + glGetString(GL_VERSION));
        glVersion = glGetString(GL_VERSION);
        Logger.print("OpenGL shader version: " + glGetString(GL_SHADING_LANGUAGE_VERSION));
        glShadingLanguageVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
        glSLVersionShort = Double.parseDouble(glGetString(GL_SHADING_LANGUAGE_VERSION).split(" ")[0]);
        Logger.print("GLSL short: " + Double.toString(glSLVersionShort));
        username = System.getProperty("user.name");

        // Уникальный номер компьютера
        pcid = SHA.toHash(
                System.getProperty("os.name") +
                        System.getProperty("os.version") +
                        System.getProperty("os.arch") +
                        System.getProperty("user.name"));
        Logger.print("PCID: " + String.format("%.10s...", pcid));

        // Определяем операционную систему
        os = System.getProperty("os.name");
        osVersion = System.getProperty("os.version");
    }

    public static Information getInstance() {
        if (instance == null) instance = new Information();
        return instance;
    }

}
