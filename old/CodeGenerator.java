package net.djuke.engine.old;

import java.util.Random;

/**
 * CodeGenerator - класс для генерации имён и ключей
 */

public class CodeGenerator {

    private static String[] wordG = {"a", "e", "i", "o", "u"}; // Гласные
    private static String[] wordS = { // Слагаемые
            "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "z"};

    // Генерация имени
    public static String generateName() {
        String name = "";
        for (int counter = 0; counter < 2 + Core.random.nextInt(4); counter++) {
            if (counter == 0) {
                if (Core.random.nextBoolean()) {
                    name += wordS[Core.random.nextInt(wordS.length)];
                    name += wordG[Core.random.nextInt(wordG.length)];
                } else {
                    name += wordG[Core.random.nextInt(wordG.length)];
                    name += wordS[Core.random.nextInt(wordS.length)];
                }
            } else {
                name += wordS[Core.random.nextInt(wordS.length)];
                name += wordG[Core.random.nextInt(wordG.length)];
            }
        }
        char[] cm = name.toCharArray();
        cm[0] = Character.toUpperCase(cm[0]);
        name = String.valueOf(cm);
        return name;
    }

    // По определённому сиду
    public static String generateName(Random nrandom) {
        String name = "";
        for (int counter = 0; counter < 2 + nrandom.nextInt(4); counter++) {
            name += wordS[nrandom.nextInt(wordS.length)];
            name += wordG[nrandom.nextInt(wordG.length)];}
        char[] cm = name.toCharArray();
        cm[0] = Character.toUpperCase(cm[0]);
        name = String.valueOf(cm);
        return name;
    }
}
