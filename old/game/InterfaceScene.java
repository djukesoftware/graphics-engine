package net.djuke.game.scenes.game;

import net.djuke.engine.graphics.GraphicsManager;
import net.djuke.engine.graphics.Scene;
import net.djuke.engine.graphics.theatre.camera.Camera;
import net.djuke.engine.graphics.theatre.camera.Camera2D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector2i;
import org.liquidengine.legui.component.Button;
import org.liquidengine.legui.component.Frame;
import org.liquidengine.legui.event.MouseClickEvent;
import org.liquidengine.legui.listener.EventProcessor;
import org.liquidengine.legui.listener.MouseClickEventListener;
import org.liquidengine.legui.system.context.CallbackKeeper;
import org.liquidengine.legui.system.context.Context;
import org.liquidengine.legui.system.context.DefaultCallbackKeeper;
import org.liquidengine.legui.system.processor.SystemEventProcessor;
import org.liquidengine.legui.system.renderer.Renderer;
import org.liquidengine.legui.system.renderer.nvg.NvgRenderer;
import org.liquidengine.legui.system.renderer.nvg.NvgRendererProvider;
import org.lwjgl.glfw.GLFWKeyCallback;

import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;

/**
 * InterfaceScene
 *
 * @author Vadim
 * @version 1.0 18.07.2017
 */
public class InterfaceScene extends Scene {

    private Logger logger = LogManager.getLogger("Game interface");
    private Camera2D camera = new Camera2D();

    private Context context;
    private Renderer renderer;
    private Frame frame;
    private SystemEventProcessor systemEventProcessor;
    private EventProcessor eventProcessor;

    public void load() {
        int width = (int)GraphicsManager.getInstance().getSettings().getResolutionWidth();
        int height = (int)GraphicsManager.getInstance().getSettings().getResolutionHeight();

        logger.info("Creating frame");
        frame = new Frame(width, height);
        createComponents(frame);

        eventProcessor = new EventProcessor();
        context = new Context(GraphicsManager.getInstance().getRootWindow().getWindow(), frame, eventProcessor);

        DefaultCallbackKeeper keeper = new DefaultCallbackKeeper();
        keeper.registerCallbacks(GraphicsManager.getInstance().getRootWindow().getWindow());

        systemEventProcessor = new SystemEventProcessor(frame, context, keeper);

        renderer = new NvgRenderer(context, NvgRendererProvider.getInstance());
        renderer.initialize();
    }

    public void render() {
        context.updateGlfwWindow();

        renderer.render(frame);

        systemEventProcessor.processEvent();
        eventProcessor.processEvent();
    }

    public void deleteData() {
        System.out.println("Delete data");
        renderer.destroy();
    }

    public void createComponents(Frame frame) {
        Button button = new Button("add compoonent", 20,20,160,30);

        button.getListenerMap().addListener(MouseClickEvent.class, (MouseClickEventListener) event -> {
                    System.out.println("hello");
                });
        frame.getContainer().add(button);
    }

    public Camera getCamera() {
        return camera;
    }
}
