package net.djuke.game.scenes.game;

import net.djuke.engine.core.Core;
import net.djuke.engine.data.Data;
import net.djuke.engine.graphics.*;
import net.djuke.engine.graphics.theatre.camera.Camera;
import net.djuke.engine.graphics.theatre.camera.Camera2D;
import net.djuke.engine.graphics.theatre.camera.Camera3D;
import net.djuke.engine.graphics.primitivies.Quad;
import net.djuke.engine.math.Vector2;
import net.djuke.game.GameData;
import net.djuke.game.scenes.viewers.UnitView;

import java.io.FileNotFoundException;

/**
 * WorldScene
 *
 * @author Vadim
 * @version 1.0 18.07.2017
 */
public class WorldScene extends Scene {

    private Camera3D camera = new Camera3D();
    private Camera2D interfaceCamera = new Camera2D();

    private Shader modelShader;
    private Model model;

    private Texture texture;
    private Texture normalTexture;
    private Texture illuminationTexture;
    private Texture specularTexture;

    private Model lightModel;
    private Shader lightModelShader;

    private Model quad;
    private Shader interfaceShader;
    private Texture dotTexture;

    private Billboard modelBillboard;

    private UnitView pirate;
    private UnitView police;
    private UnitView miner;

    private Billboard pirateCoord;
    private Billboard policeCoord;
    private Billboard minerCoord;

    public void load() {

        pirate = new UnitView("Galaxy pirate");
        police = new UnitView("Police");
        miner = new UnitView("Miner");

        pirateCoord = new Billboard(camera, pirate.getModel());
        policeCoord = new Billboard(camera, police.getModel());
        minerCoord = new Billboard(camera, miner.getModel());

        try {
            Data shaders = GameData.getInstance().getData("shaders");
            interfaceShader = (Shader)shaders.getObject("interface.glsl");
            modelShader = (Shader)shaders.getObject("model.glsl");
            lightModelShader = (Shader)shaders.getObject("game.models.lightModel.glsl");

            Data spaceShipData = GameData.getInstance().getData("spaceShip");
            model = (Model)spaceShipData.getObject("model.obj");
            texture = (Texture)spaceShipData.getObject("texture.txr");
            normalTexture = (Texture)spaceShipData.getObject("normal.txr");
            specularTexture = (Texture)spaceShipData.getObject("specular.txr");
            illuminationTexture = (Texture)spaceShipData.getObject("illumination.txr");

            Data lightModel = GameData.getInstance().getData("game.models.lightModel");
            this.lightModel = (Model)lightModel.getObject("model.obj");

            Data icons = GameData.getInstance().getData("game.icons");
            dotTexture = (Texture)icons.getObject("model.txr");

            modelBillboard = new Billboard(camera, model);
            quad = new Model(Quad.getModel());

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void render() {
        // Set 3D camera as current camera of this scene
        setCamera(camera);

        // Get coordinates of model on the screen
        Vector2 coordinates = modelBillboard.getScreenCoordinates();

        modelShader.bind();

        // Update parameters of camera
        camera.translate(0,1,-.7f);
        camera.rotate(0.6f,0,0);

        // Send parameters of camera
        modelShader.sendCamera(camera);

        // Send textures of model
        texture.bind(0);
        modelShader.sendTexture("texture", 0);
        normalTexture.bind(1);
        modelShader.sendTexture("normalTexture", 1);
        illuminationTexture.bind(2);
        modelShader.sendTexture("illumination", 2);
        specularTexture.bind(3);
        modelShader.sendTexture("specular", 3);

        // Update parameters of model
        float time = Core.getInstance().getTime();

        // Render model
        police.setPosition(0, 0, -2);
        modelShader.sendModelMatrix(police.getModel());
        police.render();

        pirate.setPosition((float)Math.sin(time + 1) * 2, 0, -3);
        modelShader.sendModelMatrix(pirate.getModel());
        pirate.render();


        miner.setPosition((float)Math.sin(time + 2) * 2, 0, -4);
        modelShader.sendModelMatrix(miner.getModel());
        miner.render();

        model.translate(0,(float)Math.sin(time),-3.5f);
        model.rotate(0,time,0);
        modelShader.sendModelMatrix(model);
        model.render();


        // Unbind modelShader
        modelShader.unbind();

        // Set 2D camera as current camera of this scene
        setCamera(interfaceCamera);

        // Render light
        lightModelShader.bind();
        lightModelShader.sendCamera(camera);

        lightModel.translate(0, 0.5f, -1);
        lightModel.scale(0.025f);
        lightModelShader.sendModelMatrix(lightModel);

        lightModel.render();

        lightModelShader.unbind();

        // Render something in center of the screen
        interfaceShader.bind();
        interfaceShader.sendCamera(interfaceCamera);

        dotTexture.bind(0);
        interfaceShader.sendTexture("texture", 0);

        quad.scale(0.05f);
        quad.translate(coordinates.getX(), coordinates.getY(), 0);
        interfaceShader.sendModelMatrix(quad);

        quad.render();

        interfaceShader.unbind();

    }

    public void deleteData() {
    }

    public Camera getCamera() {
        return camera;
    }
}
