package net.djuke.engine.old;

import java.io.*;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Resources - класс для загрузки файлов и классов
 * Created by vadim on 10.06.16.
 */
public class Resources {

    // JAR файл с ресурсами игры
    private static JarFile res;

    public static Object loadObject(String path) {
        Object object = null;
        File file = new File(path);
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            object = ois.readObject();
            ois.close();
            fis.close();
        }  catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return object;
    }

    public static void saveObject(Object object, String path) {
        // Создаём файл сохранения игры
        File file = new File(path);
        try {
            // Сериализация
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(object);
            oos.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Прогрузка всех данных игры из архива
    public static void load() {
        try {
            // Загружаем файл игровых данных
            res = new JarFile("data/res.dat");
        } catch (IOException e) {
            e.printStackTrace(); }
    }

    // Получить входяший поток из файла в архиве
    public static InputStream getInputStream(String path) {
        Logger.print("Getting file: " + path);
        JarEntry entry = new JarEntry(path);
        try {
            return res.getInputStream(entry);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    // Шифрование файлов
    private static byte[] encrypt(long key, byte[] input) {
        // Загружаем генератор случайных чисел
        Random random = new Random(key);
        int a = random.nextInt(1000);
        // Шифруем
        for (int counter = 0; counter < input.length; counter++)
            input[counter] = (byte) (input[counter] + (byte) random.nextInt(a));
        // возращаем
        return input;
    }

    // Расшифрование файлов
    private static byte[] decrypt(long key, byte[] input) {
        // Загружаем генератор случайных чисел
        Random random = new Random(key);
        int a = random.nextInt(1000);
        // Расшифруем
        for (int counter = 0; counter < input.length; counter++)
            input[counter] = (byte)(input[counter] - (byte)random.nextInt(a));
        // возращаем
        return input;
    }

    // Сохранение файлов с сжатием и шифрованием
    public static void saveObjectWithEncrypting(String path, Object object, String key) {
        // Создаём файл сохранения игры
        File file = new File(path);
        // Заранее создаём буфер
        byte[] buffer;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            /** Сериализация **/
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            buffer = baos.toByteArray();
            oos.close();
            /** Шифрование **/
            buffer = Resources.encrypt(key.hashCode(), buffer);
            /** Архивирование **/
            // Преврящаем массив байтов в поток байтов из buffer
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            // Выходной поток байтов для файла
            FileOutputStream fos = new FileOutputStream(file);
            // Сжатие данных
            GZIPOutputStream zos = new GZIPOutputStream(fos);
            byte[] timeBuffer = new byte[1024];
            int len;
            while ((len = bais.read(timeBuffer)) != -1) {
                // В поток сжатия данных записываем байты из временного буфера,
                // полученного из байтового потока
                zos.write(timeBuffer, 0, len);
            }
            // Закрываем потоки
            zos.close();
            fos.close();
            bais.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.gc();
    }

    // Загружаем обьект с расшифровкой
    public static Object loadObjectWithDecrypting(String path, String key) {
        Object object = null;
        // Загружаем файл и подготваливаем массив байтов
        File file = new File(path);
        byte[] buffer = new byte[(int)file.length()];
        try {
            // Считываем массив байтов
            FileInputStream fis = new FileInputStream(file);
            fis.read(buffer); // IDEA пишет что данная строка может быть проигнорирована
            // И я совершенно не понимаю что это значит, но оно работает и это главное
            fis.close();
            /** Разархивируем **/
            // Массив байтов в поток
            ByteArrayInputStream bais = new ByteArrayInputStream(buffer);
            // Разархивирование
            GZIPInputStream zis = new GZIPInputStream(bais);
            // Выходной поток байнтов
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] timeBuffer = new byte[1024];
            int len;
            while ((len = zis.read(timeBuffer)) != -1) {
                baos.write(timeBuffer, 0, len);
            }
            buffer = baos.toByteArray();
            baos.close();
            zis.close();
            bais.close();
            /** Расшифровка **/
            buffer = Resources.decrypt(key.hashCode(), buffer);
            /** Десиреализация **/
            bais = new ByteArrayInputStream(buffer);
            ObjectInputStream ois = new ObjectInputStream(bais);
            object = ois.readObject();
            ois.close();
            bais.close();
            // МУЛЬТИКЕТЧ!
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.gc();
        return object;
    }

}
