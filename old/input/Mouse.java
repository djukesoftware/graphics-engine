package net.djuke.engine.old.input;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.Settings;
import net.djuke.engine.math.Vector;
import org.lwjgl.glfw.GLFWCursorPosCallback;
import org.lwjgl.glfw.GLFWMouseButtonCallback;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Mouse - класс для работы с мышью
 * Created by vadim on 10.06.16.
 */
public class Mouse {

    // Отслеживаем положение курсора
    private static GLFWCursorPosCallback glfwCursorPosCallback;
    private static GLFWMouseButtonCallback glfwMouseButtonCallback;
    private static float timerForMouseUpdate = 0;
    private static Vector mouse = new Vector(0, 0, 0);
    private static Vector oldMouse = new Vector(0, 0, 0);
    private static Vector mouseSpeed = new Vector(0, 0, 0);
    private static Vector startPosition = new Vector(0, 0, 0);
    // Нажата ли мышь
    private static boolean mouseRelease = false;
    private static boolean push = false;
    private static boolean pushLastInstance = false;

    public static void init() {
        // Отслеживаем положение курсора
        glfwSetCursorPosCallback(Core.window, glfwCursorPosCallback = new GLFWCursorPosCallback() {
            public void invoke(long window, double xpos, double ypos) {
                mouse.setX((float) xpos);
                mouse.setY(Settings.getInstance().height - (float)ypos);
            }});
        // Отслеживаем нажатии мыши
        glfwSetMouseButtonCallback(Core.window, glfwMouseButtonCallback = new GLFWMouseButtonCallback() {
            public void invoke(long l, int i, int i1, int i2) {
                mouseRelease = i == GLFW_MOUSE_BUTTON_LEFT & i1 == GLFW_RELEASE;
            }});
    }

    public static void updateMouse() {
        // Высчитываем скорость мыши
        mouseSpeed.setX(mouse.getX() - oldMouse.getX());
        mouseSpeed.setY(mouse.getY() - oldMouse.getY());
        if (glfwGetMouseButton(Core.window, GLFW_MOUSE_BUTTON_1) == 1) push = true;
        else push = false;

        if (push != pushLastInstance) {
            pushLastInstance = push;
            startPosition.setX(mouse.getX());
            startPosition.setY(mouse.getY());
        }
    }

    public static Vector getStartPosition() {
        return startPosition;
    }

    public static boolean isPush() {
        return push;
    }

    public static boolean isMouseRelease() {
        return mouseRelease;
    }

    public static void updatePosition() {
        // Отслеживаем координаты мыши
        if (glfwGetTime() > timerForMouseUpdate + 0.005) {
            oldMouse.setX(mouse.getX());
            oldMouse.setY(mouse.getY());
            timerForMouseUpdate = (float)glfwGetTime();
        }
        if (mouseRelease) mouseRelease = false;
    }

    public static Vector getMouse() {
        return mouse;
    }

    public static Vector getMouseSpeed() {
        return mouseSpeed;
    }
}
