package net.djuke.engine.old.input;

import net.djuke.engine.old.Core;
import org.lwjgl.glfw.GLFWCharCallback;

import java.io.UnsupportedEncodingException;

import static org.lwjgl.glfw.GLFW.glfwSetCharCallback;

/**
 * Keyboard - символьный ввод с клавиатуры
 * Created by vadim on 12.06.16.
 */
public class Keyboard {


    private static GLFWCharCallback glfwCharCallback;
    private static String lastCharFromKeyboard;

    public static void init() {
        // Отслеживаем ввод текста с клавиатуры
        glfwSetCharCallback(Core.window, glfwCharCallback = new GLFWCharCallback() {
            public void invoke(long l, int i) {
                byte[] c = {(byte) i};
                try {
                    lastCharFromKeyboard = new String(c, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace(); }
            }});
    }

    public static String getLastCharFromKeyboard() {
        String result;
        if (lastCharFromKeyboard != null)
            result = lastCharFromKeyboard;
        else result = "";
        lastCharFromKeyboard = "";
        return result;
    }
}
