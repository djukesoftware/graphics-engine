package net.djuke.engine.old;

import net.djuke.engine.old.input.Keyboard;
import net.djuke.engine.old.input.Mouse;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;
import net.djuke.engine.project.Game;
import java.util.ArrayList;
import java.util.Random;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_MULTISAMPLE;

public class Core {

    // Текущее окно
    public static long window;
    // Счётчик FPS
    private static int fpsCounter = 0;
    private static float timerForFrameCounter = 0;
    private static int fps = 0;
    // Стандартные обьекты с удалением и загрузкой
    private static ArrayList<CoreObject> objects = new ArrayList<>();
    // Мониторинг потребления оперативной памяти
    private static Runtime runtime = Runtime.getRuntime();
    // Прошла ли секунда
    private static boolean secondPas = false;
    public static Random random = new Random();
    // Проект присоеденяемый к движку
    private static Project project;

    public static void main(String[] args) {
        load(); // load data for engine
        Settings.getInstance().shutdown = false;

        init(); // graphics init

        // TODO: Rewrite system of projects
        project = Game.getInstance(); // add game
        project.load(); // loading data for game

        // TODO: Delete this, if this is unusual
        // Get information about computer
        // Information.getInstance().genInformation();

        // Check current version of OpenGL Shader Language
        if (Information.getInstance().glSLVersionShort < 4.0)
            Logger.displayError("" +
                    "Perihelion need GLSL: 4.0\n" +
                    "Your version of OpenGL Shading Language is to old.\n" +
                    "How to fix it: you can to try to update your graphics drivers.\n\n" +
                    "P.S. The game can continue to run, but not correctly, and possibly with errors.");

        while (glfwGetKey(window, GLFW_KEY_F1) != 1 & !Settings.getInstance().shutdown) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            // Обновляем мышь
            /**Mouse.updateMouse();

            // Отрисовка всего трёхмерного мира
            init3D();
            Camera.getInstance().bind();
            World.getInstance().render();
            Camera.getInstance().unbind();

            // Отрисовка всего интерфейса
            init2D();
            InterfaceSystem.getInstance().render();

            // Обновление проекта
            game.update();

            // Обвнляем все обьекты добавленные к движку
            objects.forEach(CoreObject::update);

            updateFPS();
             **/

            glfwSwapBuffers(window);
            glfwPollEvents();
        }

        saveSettings(); // Save settings
        project.end(); // Delete data of game
        delete(); // Delete data of engine
    }

    private static void init() {
        // Creating new window with rendering
        Logger.print("Creating window...");
        // Check work of GLFW library
        if (glfwInit()) Logger.print("Failed create window");
        if (Settings.getInstance().msaa) glfwWindowHint(GLFW_SAMPLES, Settings.getInstance().msaaQuality);
        // Creating window with window mode or fullscreen mode
        if (!Settings.getInstance().fullscreen)
            window = glfwCreateWindow(Settings.getInstance().width, Settings.getInstance().height, "Perihelion Engine", GL_NONE, GL_NONE);
        else window = glfwCreateWindow(Settings.getInstance().width, Settings.getInstance().height, "Perihelion Engine", glfwGetPrimaryMonitor(), GL_NONE);
        glfwMakeContextCurrent(window);
        glfwShowWindow(window);
        // Get current resolution of screen
        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        // Set resolution of screen
        glfwSetWindowPos(window,
                (vidmode.width() - Settings.getInstance().width) / 2,
                (vidmode.height() - Settings.getInstance().height) / 2);

        glfwSwapInterval(1); // v-sync

        Logger.print("Init OpenGL...");

        GL.createCapabilities();
        glViewport(0, 0, Settings.getInstance().width, Settings.getInstance().height);
        // Enable alpha channel
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        // Enable smooth of lines
        // glEnable(GL_LINE_SMOOTH);
        // glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
        // Enable smooth of points
        // glEnable(GL_POINT_SMOOTH);
        // glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
        glShadeModel(GL_SMOOTH);

        // Enable MSAA smoothing
        if (Settings.getInstance().msaa) glEnable(GL_MULTISAMPLE);

        // Init control devices
        Mouse.init();
        Keyboard.init();
    }

    private static void updateFPS() {
        secondPas = false;
        fpsCounter++;
        Mouse.updatePosition();
        // Update FPS counter
        if (glfwGetTime() > timerForFrameCounter + 1.0) {
            timerForFrameCounter = (float)glfwGetTime();
            fps = fpsCounter;
            fpsCounter = 0;
            glfwSetWindowTitle(window, "" + runtime.totalMemory() / 1024);
            secondPas = true;
        }
    }

    // TODO: Delete this function
    public static boolean isSecondPas() {
        return secondPas;
    }

    private static void load() {
        // TODO: Delete this worst part of code in the game
        /**Settings.loadInstance(Resources.loadObject("settings.f")); // loading settings**/
        Script.getInstance().bindClass("settings", Settings.getInstance()); // add settings to JSE
    }

    private static void delete() {
        objects.forEach(CoreObject::delete); // delete core objects
    }

    private static void saveSettings() {
        Resources.saveObject(Settings.getInstance(), "settings.f");
    }

    public static void addObject(CoreObject object) {
        objects.add(object);
    }

    public static int getFps() {
        return fps;
    }

}
