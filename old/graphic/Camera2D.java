package net.djuke.engine.graphics.theatre.camera;

import net.djuke.engine.math.Matrix;
import net.djuke.engine.math.Matrix3;
import net.djuke.engine.math.Vector;

/**
 * Camera for 2D graphical interface
 * Created by azusa on 3/16/17.
 */
public class Camera2D implements Camera {

    private Matrix3 matrix = new Matrix3(); // main matrix of camera
    private Vector position = new Vector(0,0); // position of camera in 2D space
    private float rotateAngle = 0f;

    /**
     * @return matrix of camera
     */
    public Matrix getMatrix() {
        matrix.createIdentity(); // create identity matrix
        matrix.rotate(rotateAngle);
        matrix.translate(position);
        return matrix;
    }

    /**
     * Translate camera
     * @param x - x coordinate
     * @param y - y coordinate
     */
    public void translate(float x, float y) {
        position.set(x, y);
    }

    public void rotate(float angle) {
        rotateAngle = angle;
    }

}
