package net.djuke.engine.old.graphic;

import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Core;
import de.matthiasmann.twl.utils.PNGDecoder;
import java.io.*;
import java.nio.ByteBuffer;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL14.*;

/**
 * Texture - класс для прогрузки текстур
 */

public class Texture implements CoreObject {

    public int width;
    public int height;
    private int texture;

    public Texture(String path) {
        Core.addObject(this); // Пока текстуры будут
        // загружаться исключительно из основного каталога
        loadTexture(path);
    }

    private void loadTexture(String path) {
        // Генерируем имя текстуры
        texture = glGenTextures();
        try {
            // Open texture file
            FileInputStream in = new FileInputStream(new File(path));
            PNGDecoder decoder = new PNGDecoder(in);

            // Read texture
            ByteBuffer buffer = ByteBuffer.allocateDirect(4 * decoder.getWidth() * decoder.getHeight());
            decoder.decode(buffer, decoder.getWidth() * 4, PNGDecoder.Format.RGBA);
            buffer.flip();

            width = decoder.getWidth();
            height = decoder.getHeight();

            // Bind and setting texture
            glBindTexture(GL_TEXTURE_2D, texture);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, decoder.getWidth(), decoder.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
            glBindTexture(GL_TEXTURE_2D, 0);

            in.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void bindTexture(float alpha) {
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    public void delete() {
        glDeleteTextures(texture);
    }

    public void update() { }

    public int getTexture() {
        return texture;
    }

    // Пустой неиспользованный метод
    public void load() {}
}
