package net.djuke.engine.old.graphic;

import java.io.Serializable;

/**
 * Color - цвета
 * Created by vadim on 30.05.16.
 */
public class Color implements Serializable {

    private float red;
    private float green;
    private float blue;
    private float alpha = 1;

    public Color() {
        red = 1;
        green = 1;
        blue = 1;
    }

    public Color(float red, float green, float blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public Color(float red, float green, float blue, float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public void set(float red, float green, float blue, float alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public float getRed() {
        return red;
    }

    public void setRed(float red) {
        this.red = red;
    }

    public float getGreen() {
        return green;
    }

    public void setGreen(float green) {
        this.green = green;
    }

    public float getBlue() {
        return blue;
    }

    public void setBlue(float blue) {
        this.blue = blue;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public Color getDarker(float dark) {
        return new Color(this.red - dark, this.green - dark, this.blue - dark, this.alpha);
    }

    public Color getLighter(float light) {
        return new Color(this.red + light, this.green + light, this.blue + light, this.alpha);
    }

}
