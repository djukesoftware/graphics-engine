package net.djuke.engine.old.graphic.gui;


import net.djuke.engine.old.graphic.gui.components.Component;

import java.util.ArrayList;

/**
 * InterfaceSystem - для работы со всем интерфейсом
 * Created by vadim on 18.06.16.
 */
public class InterfaceSystem {

    private static InterfaceSystem instance = new InterfaceSystem();
    private ArrayList<Component> components = new ArrayList<>();

    public void render() {
        // Отрисовка оконной системы
        WindowSystem.getInstance().render();
        // Отрисовка компонентов
        components.forEach(Component::render);
        // Отрисовка подсказок
        Notice.getInstance().render();
    }

    public void addComponent(Component component) {
        components.add(component);
    }

    public static InterfaceSystem getInstance() {
        return instance;
    }

}
