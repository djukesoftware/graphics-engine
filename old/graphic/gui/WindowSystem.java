package net.djuke.engine.old.graphic.gui;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.input.Mouse;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * WindowSystem - класс для управления окнами
 * определяет какое окно должно рисоваться первым
 * Created by vadim on 10.06.16.
 */
public class WindowSystem implements Serializable {

    private static WindowSystem instance;

    // Лист окон
    private ArrayList<Window> windows = new ArrayList<>();
    private Window activeWindow;
    private int id = 0;

    public void render() {
        // Если окна не скрыты, то рисуем их
        if (!Settings.getInstance().hideWindows) {
            // Сначало отрисовка окон (наоборот!)
            for (int count = windows.size() - 1; count >= 0; count--)
                windows.get(count).render();

            // Потом обрабатываем
            Window currentWindow = null;
            activeWindow = null;
            // Перебор всех окон
            for (Window window : windows) {
                if (window.getHover()) { // Находим окно, на которое навели мышь
                    currentWindow = window; // Запоминмаем его
                    activeWindow = window;
                    break; // Закрываем перебор
                }
            }

            // Если на текущее окно нажали
            if (currentWindow != null) {
                if (currentWindow.getBeenHover() & Mouse.isPush()) {
                    // То кидаем его поверх других окон
                    windows.remove(currentWindow);
                    windows.add(0, currentWindow);
                    for (Window window : windows) window.active = false;
                    currentWindow.active = true; // Делаем окно активным
                }
            }
            if (currentWindow != null) currentWindow.update(); // обновляем текущее окно
        }
    }

    public Window getActiveWindow() {
        return activeWindow;
    }

    public int getId() {
        int result = id;
        id++;
        return result;
    }

    public void addWindow(Window window) {
        if (!windows.contains(window))
            windows.add(window); // добавляем окно
    }

    public void deleteWindow(int id) {
        Window target = null;
        for (Window window : windows) {
            if (window.getId() == id) target = window;
        }
        windows.remove(target);
    }

    public Window getWindow(int id) {
        Window target = null;
        for (Window window : windows) {
            if (window.getId() == id) target = window;
        }
        return target;
    }

    public void closeWindow(Window window) {
        windows.remove(window);
    }

    public void openWindow(Window window) {
        addWindow(window);
    }

    public static WindowSystem getInstance() {
        if (instance == null) instance = new WindowSystem();
        return instance;
    }

}
