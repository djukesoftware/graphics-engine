package net.djuke.engine.old.graphic.gui.components;

import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.primitives.Quad;

import java.io.Serializable;

import static org.lwjgl.opengl.GL11.*;

/**
 * Graphic - график чего-либо
 * Created by vadim on 12.06.16.
 */
public class Graphic implements Component, Serializable {

    private Vector position = new Vector();
    private int width; // ширина графика
    private int[] points; // точки по которым строится график

    public Vector getPosition() {
        return position;
    }

    public void setLenght(int length) {
        points = new int[length];
        for (int count = 0; count < points.length; count++) {
            points[count] = 0;
        }
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void addVariable(int var) {
        if (var < 0) var = 0;
        if (var > 99) var = 100;
        for (int count = 0; count < points.length; count++) {
            if (count != 0) {
                points[count - 1] = points[count];
            }
        }
        points[points.length - 1] = var;
    }

    public void render() {
        glDisable(GL_TEXTURE_2D);
        // Рамка графика
        Quad.getInstance().defaultColors();
        Quad.getInstance().render(position, width, 100 + 2, GL_LINE_LOOP);

        for (int count = 0; count < points.length - 1; count++) {
            glBegin(GL_LINES);
            glColor4f(1 - points[count] / 100f, 1, 0, 1);
            glVertex2f(position.getX() + (float)count * ((float)width / (float)points.length),
                    position.getY() + points[count]);
            glColor4f(1 - points[count + 1] / 100f, 1, 0, 1);
            glVertex2f(position.getX() + (float)(count + 1) * ((float)width / (float)points.length),
                    position.getY() + points[count + 1]);
            glEnd();
        }
    }

}
