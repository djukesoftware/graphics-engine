package net.djuke.engine.old.graphic.gui.components;

import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.TextSystem;

import java.io.Serializable;

/**
 * Text - текстовый компонент
 * Created by vadim on 11.06.16.
 */
public class Text implements Component, Serializable {

    private Vector position = new Vector();
    private Vector renderPosition = new Vector();
    private Color color = new Color();
    private String text = "";
    private int height = 0;
    private int width = 0;

    public Text() {

    }

    public Text(String text) {
        this.text = text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Vector getPosition() {
        return position;
    }

    public void render() {
        String[] result = text.split("\n");
        height = 22 * result.length;
        renderPosition.set(position.getX(), position.getY(), position.getZ());

        for (String print : result) {
            TextSystem.getInstance().render(renderPosition, print, color);
            renderPosition.setY(renderPosition.getY() - TextSystem.getInstance().getHeight(print));
            if (width < TextSystem.getInstance().getWidth(print))
                width = TextSystem.getInstance().getWidth(print);
        }
    }

    public int getHeight() {
        String[] result = text.split("\n");
        return height = 22 * result.length;
    }

    public int getWidth() {
        String[] result = text.split("\n");
        width = 0;
        for (String print : result) {
            if (width < TextSystem.getInstance().getWidth(print))
                width = TextSystem.getInstance().getWidth(print);
        }
        return width;
    }

    public String getText() {
        return text;
    }
}
