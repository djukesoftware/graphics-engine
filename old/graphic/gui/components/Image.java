package net.djuke.engine.old.graphic.gui.components;

import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.old.graphic.Shader;
import net.djuke.engine.old.graphic.Texture;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.primitives.Quad;

import java.io.Serializable;

import static org.lwjgl.opengl.GL11.*;

/**
 * Image - отрисовка изображения
 * Created by vadim on 11.06.16.
 */
public class Image implements Component, Serializable {

    private Texture texture;
    private Vector position;
    private int width = 64;
    private int height = 64;
    private Shader shader;

    private boolean border = false;
    private int borderSize = 5;
    private Color borderColor = new Color();

    public Image(String path) {
        texture = new Texture(path);
        position = new Vector();
    }

    public void bindImageShader(Shader shader) {
        this.shader = shader;
    }

    public void setBorder(Color color, int size) {
        borderColor = color;
        borderSize = size;
        border = true;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public void render() {

        if (shader != null) shader.bind();
        // Рисуем изображение
        glEnable(GL_TEXTURE_2D);
        texture.bindTexture(1);

        Quad.getInstance().defaultColors();
        Quad.getInstance().render(position, width, height, GL_QUADS);

        if (shader != null) shader.unbind();

        if (border) {
            glDisable(GL_TEXTURE_2D);
            glLineWidth(borderSize);
            Quad.getInstance().changeColors(borderColor, borderColor, borderColor, borderColor);
            Quad.getInstance().render(position, width, height, GL_LINE_LOOP);
            glLineWidth(1);
        }
    }

}
