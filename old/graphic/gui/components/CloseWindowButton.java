package net.djuke.engine.old.graphic.gui.components;

import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.old.graphic.Texture;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Mouse;

import java.io.Serializable;

import static org.lwjgl.opengl.GL11.GL_QUADS;

/**
 * Button - класс для отрисовки кнопки из изображения
 * Created by vadim on 10.06.16.
 */
public class CloseWindowButton implements Component, Serializable {

    private boolean hover = false;
    public boolean available = true;
    public boolean push = false;

    private Texture texture;
    private Vector position = new Vector();

    public void setPosition(Vector position) {
        this.position = position;
    }

    public CloseWindowButton(String path) {
        texture = new Texture(path);
    }

    public void render() {
        // Рисуем кнопку
        texture.bindTexture(1);

        float alpha;
        Color color;
        // Если кнопка доступна
        if (available) {
            // То проверяем, на ней ли курсор и изменяем прозрачность
            if (hover) alpha = 1;
            else alpha = 0.25f;
            color = new Color(1, 1, 1, alpha);
        } else {
            // Если кнопка недоступна, то она чёрная
            color = new Color(0, 0, 0, 0.5f);
        }

        // Рисуем кнопку
        Quad.getInstance().changeColors(
                color, color, color, color);
        Quad.getInstance().render(position, texture.width, texture.height, GL_QUADS);
        // Обрабатываем кнопку
        update();
    }

    private void update() {
        // Обрабатываем местоположение курсора относительно кнопки
        if (available) {
            if (Mouse.getMouse().getX() > position.getX() & Mouse.getMouse().getX() < position.getX() + texture.width &
                    Mouse.getMouse().getY() > position.getY() & Mouse.getMouse().getY() < position.getY() + texture.height) {
                hover = true;
                if (Mouse.isMouseRelease()) push = true;
                else push = false;
            } else hover = false;
        }
    }

}
