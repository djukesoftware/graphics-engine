package net.djuke.engine.old.graphic.gui.components;

/**
 * Component - компонент графического интерфейса
 * Created by vadim on 11.06.16.
 */
public interface Component {

    void render();

}
