package net.djuke.engine.old.graphic.gui.components;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.TextSystem;
import net.djuke.engine.old.graphic.primitives.Quad;

import java.io.Serializable;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static net.djuke.engine.old.graphic.GraphicUtils.enableTextures;
import static org.lwjgl.opengl.GL11.*;

/**
 * ProgressBar -
 * Created by vadim on 05.07.16.
 */
public class ProgressBar implements Component, Serializable {

    private Vector position = new Vector();
    private int width;
    private int height;
    private int progress; // в процентах

    private boolean showText = true;

    private Color borderColor = new Color(1, 1, 1, 0.5f);
    private Color progressColor = new Color(0.5f, 0.5f, 1f, 1f);
    private Color shadowColor = new Color(0.25f, 0.25f, 0.5f, 1f);

    public void setShowText(boolean showText) {
        this.showText = showText;
    }

    public Vector getPosition() {
        return position;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void render() {
        disableTextures();

        // Рисуем фон
        Quad.getInstance().changeColors(
                Settings.getInstance().colorTheme, Settings.getInstance().colorTheme,
                Settings.getInstance().colorTheme.getDarker(0.1f), Settings.getInstance().colorTheme.getDarker(0.1f));
        Quad.getInstance().render(position, width, height, GL_QUADS);

        // Рисуем фон
        Quad.getInstance().changeColors(
                shadowColor, shadowColor,
                progressColor, progressColor);
        Quad.getInstance().render(position, (int)(100f * (float)progress / (float)width), height, GL_QUADS);

        // Рисуем рамку
        Quad.getInstance().changeColors(borderColor, borderColor, borderColor, borderColor);
        Quad.getInstance().render(position, width, height, GL_LINE_LOOP);

        enableTextures();
        TextSystem.getInstance().render(new Vector(
                (int)(position.getX() + width / 2f - TextSystem.getInstance().getWidth(progress + "%") / 2f),
                (int)(position.getY() + height / 2f + 11), 0), progress + "%", new Color(1, 1, 1));

    }

}
