package net.djuke.engine.old.graphic.gui;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.components.Component;
import net.djuke.engine.old.graphic.gui.components.CloseWindowButton;
import net.djuke.engine.old.graphic.gui.interactiveComponents.InteractiveComponent;
import net.djuke.engine.old.graphic.gui.components.Text;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Mouse;

import java.io.Serializable;
import java.util.ArrayList;
import static org.lwjgl.opengl.GL11.*;

/**
 * Window - класс для отрисовки окна
 * Created by vadim on 10.06.16.
 */
public class Window implements Serializable {

    private int id;

    // Коллекция текстовых компонентов
    private ArrayList<Component> components = new ArrayList<>();
    private ArrayList<InteractiveComponent> interactiveComponents = new ArrayList<>();
    // Характеристики окна
    private Vector position = new Vector(0, 0, 0);
    // Размер окна
    private int width = 100;
    private int height = 100;
    public boolean active = true; // Определяется системой виджетов
    private int titleHeight = 0;
    // Кнопка закрытия
    private CloseWindowButton close = new CloseWindowButton("close.png");
    // Закрываемое ли окно
    private boolean enableClose = true;
    // Открыто ли окно
    private boolean hide = true;
    // Прорисовка сетки внутри окна
    private boolean grid = false;
    private int sizeOfGrid = 10;
    private boolean enableMove = false;
    // Отступ
    private int padding = 0;
    // Заголовок окна
    private Text titleComponent = new Text();

    // Цвета каждого компонента
    private Color backgroundColor = new Color();
    private Color gridColor;
    // Позиции элементов
    private Vector lightPosition = new Vector();
    private Vector backgroundPosition = new Vector();
    private Vector borderPosition = new Vector();
    private Vector gridPosition = new Vector();

    public void setPadding(int padding) {
        this.padding = padding;
    }

    public void setTitle(String title) {
        titleComponent.setText(title);
    }

    public void renderGrid(boolean render) {
        grid = render;
    }

    public void setGridSize(int size) {
        sizeOfGrid = size;
    }

    // Устанавливаем отрыто окно или нет
    public void setHide(boolean hide) {
        this.hide = hide;
    }

    // Окно добавляет себя в систему окон
    public Window() {
        WindowSystem.getInstance().addWindow(this);
        id = WindowSystem.getInstance().getId();
        titleComponent.setColor(new Color(1, 1, 1, 0.75f));
    }

    public int getId() {
        return id;
    }

    public void enableClose(boolean enableClose) {
        this.enableClose = enableClose;
    }

    // Устаналиваем размеры окна
    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    // Устанавливаем положение окна
    public void setPosition(Vector position) {
        this.position = position;
    }

    // Отрисовка окна
    public void render() {
        if (hide) {
            if (!titleComponent.getText().equals("")) titleHeight = 22;

            glPushMatrix();
            // Перемещаем окно
            glTranslatef((int)position.getX(), (int)position.getY(), 0);

            // В зависимости от активности окна, устанавливаем прозрачность
            float alpha;
            if (active) alpha = 0.7f;
            else alpha = 0.4f;
            glDisable(GL_TEXTURE_2D);

            /** Рисуем задний фон **/
            // Устанавливаем цвета и рисуем фон
            backgroundColor.set(
                    Settings.getInstance().colorTheme.getRed() / 2,
                    Settings.getInstance().colorTheme.getGreen() / 2,
                    Settings.getInstance().colorTheme.getBlue() / 2,
                    alpha + 0.5f
            );
            Quad.getInstance().changeColors(
                    backgroundColor, backgroundColor,
                    backgroundColor, backgroundColor);
            Quad.getInstance().render(backgroundPosition, width + padding * 2, height + padding * 2 + titleHeight, GL_QUADS);

            /** Рисуем светлый блик сверху **/
            Quad.getInstance().changeColors(
                    new Color(1, 1, 1, alpha - 1), new Color(1, 1, 1, alpha - 1),
                    new Color(1, 1, 1, alpha / 4), new Color(1, 1, 1, alpha / 4));
            lightPosition.setY(height / 2);
            Quad.getInstance().render(lightPosition, width + padding * 2, height / 2 + padding * 2 + titleHeight, GL_QUADS);

            /** Рисуем рамку для окна **/
            // Устанавливаем цвета и рисуем рамку
            glLineWidth(Settings.getInstance().windowBorderSize);
            Quad.getInstance().changeColors(
                    new Color(1, 1, 1, alpha / 1.5f), new Color(1, 1, 1, alpha / 1.5f),
                    new Color(1, 1, 1, alpha), new Color(1, 1, 1, alpha));
            Quad.getInstance().render(borderPosition, width + padding * 2, height + padding * 2 + titleHeight, GL_LINE_LOOP);
            glLineWidth(1);

            /** Отрисовка компонентов **/
            /*
             * Как я недавно понял, в этом коде есть единственный недостаток,
             * а именно, реализация списков. Дело в том, что в любой нормальной игре, если вы внутри
             * окна листаете список вниз, то весь текст вне окна просто не рисуется. Это реализуется за счёт
             * отсичения. Но как я выяснил такое отсечение можно сделать только с помощью GLUT, которого в LWJGL
             * просто нету. Придётся либо придумать систему отсечения самому, либо найти отдельную библиотеку с GLUT
             */
            glEnable(GL_TEXTURE_2D);
            for (Component component : components) {
                glPushMatrix();
                glTranslatef(padding, padding, 0);
                component.render();
                glPopMatrix();
            }

            /** Отрисовка верхней панели (тёмной) **/
            if (!titleComponent.getText().equals("")) {
                glDisable(GL_TEXTURE_2D);
                backgroundColor.set(0, 0, 0, 0.25f);
                Quad.getInstance().changeColors(
                        backgroundColor, backgroundColor,
                        backgroundColor, backgroundColor);
                Quad.getInstance().render(new Vector(0, height - 22 + padding * 2 + titleHeight, 0), width + padding * 2, 22, GL_QUADS);

                glEnable(GL_TEXTURE_2D);
                titleComponent.getPosition().set(width / 2 - titleComponent.getWidth() / 2, height + padding * 2 + titleHeight, 0);
                titleComponent.render();
            }

            /** Отрисовка сетки **/
            glDisable(GL_TEXTURE_2D);
            // Если сетка активирована, то рисуем её
            if (grid) {
                for (int x = 0; x < width / sizeOfGrid; x++) {
                    for (int y = 0; y < height / sizeOfGrid; y++) {
                        gridColor = new Color(1, 1, 1, 0.5f);
                        gridPosition.setX(x * sizeOfGrid);
                        gridPosition.setY(y * sizeOfGrid);
                        Quad.getInstance().changeColors(gridColor, gridColor, gridColor, gridColor);
                        Quad.getInstance().render(gridPosition, sizeOfGrid, sizeOfGrid, GL_LINE_LOOP);
                    }
                }
            }

            glPopMatrix();

            glEnable(GL_TEXTURE_2D);
            /** Отрисовка кнопки закрытия **/
            if (enableClose) close.available = true;
            else close.available = false;
            close.setPosition(new Vector(position.getX() + width - 16 + padding * 2 - 4,
                    position.getY() + height - 16 + padding * 2 - 4 + titleHeight, 0));
            close.render();

            /** Отрисовка интерактивных компонентов **/
            for (InteractiveComponent component : interactiveComponents) {
                component.getWindowPosition().setX((int)position.getX() + padding);
                component.getWindowPosition().setY((int)position.getY() + padding);
                component.render();
            }


            /** Обработка передвижения окна **/
            // Чай с лимоном и вареньем!
            if (active) { // Если окно активно, то обрабатываем его позицию
                // Если начальная позиция курсора была в окне при нажатии, значит, окно можно передвигать
                if (Mouse.getStartPosition().getX() > position.getX() & Mouse.getStartPosition().getX() < position.getX() + width + padding * 2 &
                        Mouse.getStartPosition().getY() > position.getY() & Mouse.getStartPosition().getY() < position.getY() + height + padding * 2 + titleHeight)
                    enableMove = true;
                // Если мышь не нажата
                if (enableMove & !Mouse.isPush()) enableMove = false;

                // Если передвижение активировано
                if (enableMove) {
                    // И мышь нажата
                    if (Mouse.isPush()) {
                        // То перемещяем окно
                        position.setX(position.getX() + Mouse.getMouseSpeed().getX());
                        position.setY(position.getY() + Mouse.getMouseSpeed().getY());
                    }
                }
            }
            // Блинчики и пирожки с чаем!
        }
    }

    // Отслеживаем передвижение окна
    public void update() {
        /** Обработка интерактивных компонентов **/
        for (InteractiveComponent component : interactiveComponents)
            component.update();

        // Если кнопка закрытия была нажата, значит окно надо закрыть
        if (close.push) closeWindow();
    }

    public boolean getHover() {
        // Если курсор внутри окна
        if (Mouse.getMouse().getX() > position.getX() & Mouse.getMouse().getX() < position.getX() + width + padding * 2 &
                Mouse.getMouse().getY() > position.getY() & Mouse.getMouse().getY() < position.getY() + height + padding * 2 + titleHeight)
            return true; else return false;
    }

    public boolean getBeenHover() {
        // Если курсор внутри окна
        if (Mouse.getStartPosition().getX() > position.getX() & Mouse.getStartPosition().getX() < position.getX() + width + padding * 2 &
                Mouse.getStartPosition().getY() > position.getY() & Mouse.getStartPosition().getY() < position.getY() + height + padding * 2 + titleHeight)
            return true; else return false;
    }

    public void closeWindow() {
        WindowSystem.getInstance().closeWindow(this);
    }

    public void openWindow() {
        WindowSystem.getInstance().openWindow(this);
    }

    public int getWindowWidth() {
        return width + padding * 2;
    }

    public int getWindowHeight() {
        return height + padding * 2;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getPadding() {
        return padding;
    }

    /** Работа с дополнительными компонентами **/
    // Добавляем компонент в окно
    public void addComponent(Component component) {
        components.add(component);
    }

    public void addInteractiveComponent(InteractiveComponent component) {
        interactiveComponents.add(component);
    }



}
