package net.djuke.engine.old.graphic.gui;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.old.graphic.gui.components.Text;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Mouse;
import net.djuke.engine.math.Vector;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_QUADS;

/**
 * Notice - подсказка
 * Created by Vadim on 12.07.2016.
 */
public class Notice {

    private static Notice instance;

    private Text text = new Text();
    private Vector position = new Vector();
    private boolean show = true;

    private Color borderColor = new Color(1f, 1f, 1f, 0.25f);

    private Notice() {
        text.setText("I love anime and Katerina\nSherer");
    }

    public void render() {
        if (show) {
            int width = text.getWidth();
            int height = text.getHeight();

            disableTextures();
            position.set(Mouse.getMouse().x + 10, Mouse.getMouse().y - height);

            Quad.getInstance().changeColors(Settings.getInstance().colorTheme, Settings.getInstance().colorTheme,
                    Settings.getInstance().colorTheme, Settings.getInstance().colorTheme);
            Quad.getInstance().render(position, width + 10, height, GL_QUADS);

            Quad.getInstance().changeColors(borderColor, borderColor, borderColor, borderColor);
            Quad.getInstance().render(position, width + 10, height, GL_LINE_LOOP);

            text.getPosition().set(Mouse.getMouse().x + 15, Mouse.getMouse().y);
            text.render();
        }
        show = false;
    }

    public void setText(String text) {
        this.text.setText(text);
    }

    public void show(String text) {
        this.text.setText(text);
        show = true;
    }

    public static Notice getInstance() {
        if (instance == null) instance = new Notice();
        return instance;
    }

}
