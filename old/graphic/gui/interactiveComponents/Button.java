package net.djuke.engine.old.graphic.gui.interactiveComponents;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.TextSystem;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Mouse;

import java.io.Serializable;
import java.util.ArrayList;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static org.lwjgl.opengl.GL11.*;

/**
 * Button - обычная кнопка
 * Created by vadim on 03.07.16.
 */
public class Button implements InteractiveComponent, Serializable {

    // Состояния кнопки
    private boolean available = true;
    private boolean hover = false;
    private volatile boolean push = false;
    // Местоположение онка и кнопки
    private Vector windowPosition = new Vector();
    private Vector selfPosition = new Vector();
    private Vector resultPosition = new Vector();
    //  Её размеры
    private int width;
    private int height;
    // Цвета отдельных элементов
    private Color borderColor = new Color(1, 1, 1, 0.75f);
    private Color background = new Color();
    private Color color;
    private Color resultColor = new Color();
    // Текст на кнопке
    private String text;
    // Отслеживание нажатия
    private ArrayList<Handler> handlers = new ArrayList<>();

    public Button(String text, int width, int height) {
        this.text = text;
        this.width = width;
        this.height = height;
        background.setRed(Settings.getInstance().colorTheme.getRed());
        background.setGreen(Settings.getInstance().colorTheme.getGreen());
        background.setBlue(Settings.getInstance().colorTheme.getBlue());
    }

    public void setAvailable() {
        available = true;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Vector getWindowPosition() {
        return windowPosition;
    }

    public void render() {
        // Высчитываем асболютное местоположение
        resultPosition.setX(windowPosition.getX() + selfPosition.getX());
        resultPosition.setY(windowPosition.getY() + selfPosition.getY());

        disableTextures();
        // Рисуем фон
        Quad.getInstance().changeColors(
                resultColor.getDarker(0.1f), resultColor.getDarker(0.1f),
                resultColor, resultColor);
        Quad.getInstance().render(resultPosition, width, height, GL_QUADS);

        // Рисуем рамку
        Quad.getInstance().changeColors(borderColor, borderColor, borderColor, borderColor);
        Quad.getInstance().render(resultPosition, width, height, GL_LINE_LOOP);

        TextSystem.getInstance().render(
                new Vector(resultPosition.getX() + width / 2 - TextSystem.getInstance().getWidth(text) / 2,
                        resultPosition.getY() + 11 + height / 2, 0), text, new Color());

        glPushMatrix();
        glPopMatrix();

        // Обнуляем цвета
        if (color != null) {
            resultColor.setRed(color.getRed());
            resultColor.setGreen(color.getGreen());
            resultColor.setBlue(color.getBlue());
        } else {
            resultColor.setRed(Settings.getInstance().colorTheme.getRed());
            resultColor.setGreen(Settings.getInstance().colorTheme.getGreen());
            resultColor.setBlue(Settings.getInstance().colorTheme.getBlue());
        }
    }

    public Vector getPosition() {
        return selfPosition;
    }

    /**
     * @deprecated
     * @return возвращает, нажата ли кнопка
     */
    public boolean isPush() {
        return push;
    }

    public void update() {
        /** Высчитываем абсолютную позицию **/
        resultPosition.setX(windowPosition.getX() + selfPosition.getX());
        resultPosition.setY(windowPosition.getY() + selfPosition.getY());

        /** Обрабатываем нажатие кнопки **/
        if (Mouse.getMouse().getX() > resultPosition.getX() & Mouse.getMouse().getX() < resultPosition.getX() + width &
                Mouse.getMouse().getY() > resultPosition.getY() & Mouse.getMouse().getY() < resultPosition.getY() + height ) {
            hover = true; // наведён ли курсор
            // Проверяем нажатали клавиша
            if (Mouse.isMouseRelease() & available) push = true;
            else push = false;
        } else { hover = false; }

        /** Обновляем цвета **/
        if (hover) {
            if (Mouse.isPush()) {
                if (color != null) {
                    resultColor.setRed(color.getRed() / 2);
                    resultColor.setGreen(color.getGreen() / 2);
                    resultColor.setBlue(color.getBlue() / 2);
                } else {
                    resultColor.setRed(Settings.getInstance().colorTheme.getRed() / 2);
                    resultColor.setGreen(Settings.getInstance().colorTheme.getGreen() / 2);
                    resultColor.setBlue(Settings.getInstance().colorTheme.getBlue() / 2);
                }
            } else {
                if (color != null) {
                    resultColor.setRed(color.getRed() * 2);
                    resultColor.setGreen(color.getGreen() * 2);
                    resultColor.setBlue(color.getBlue() * 2);
                } else {
                    resultColor.setRed(Settings.getInstance().colorTheme.getRed() * 2);
                    resultColor.setGreen(Settings.getInstance().colorTheme.getGreen() * 2);
                    resultColor.setBlue(Settings.getInstance().colorTheme.getBlue() * 2);
                }
            }
        } else {
            if (color != null) {
                resultColor.setRed(color.getRed());
                resultColor.setGreen(color.getGreen());
                resultColor.setBlue(color.getBlue());
            } else {
                resultColor.setRed(Settings.getInstance().colorTheme.getRed());
                resultColor.setGreen(Settings.getInstance().colorTheme.getGreen());
                resultColor.setBlue(Settings.getInstance().colorTheme.getBlue());
            }
        }

        /** Обрабатываем события **/
        if (push) handlers.forEach(Handler::push);
        if (hover) handlers.forEach(Handler::hover);
    }

    public void addHandler(Handler handler) {
        handlers.add(handler); // Добавляем слушателя
    }

    public interface Handler {
        public void push();
        public void hover();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
