package net.djuke.engine.old.graphic.gui.interactiveComponents;

import net.djuke.engine.old.Core;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import net.djuke.engine.old.graphic.gui.TextSystem;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Keyboard;
import net.djuke.engine.old.input.Mouse;

import java.io.Serializable;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static net.djuke.engine.old.graphic.GraphicUtils.enableTextures;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_QUADS;

/**
 * Input - ввод с клавиатуры
 * Created by vadim on 12.06.16.
 */
public class Input implements InteractiveComponent, Serializable {

    private Vector position = new Vector(); // позиция поля вода
    private Vector windowPosition = new Vector(); // позиция от окна
    private Vector resultPosition = new Vector();

    private double time;
    private boolean done = false;

    private boolean enable = false; // активен ли ввод
    private StringBuffer inputText = new StringBuffer(""); // вводимый текст
    private String text = ""; // текст перед вводом
    private StringBuffer outText = new StringBuffer(""); // текст на отрисовку
    private int width = 100; //  ширина поля ввода

    private int minChar = 0;

    // Цвет заднего фона
    private Color backgroundColor = new Color(0, 0, 0, 0.1f);
    private Color secondBackColor = new Color(0, 0, 0, 0.5f);
    private Color borderColor = new Color(1, 1, 1, 0.25f);

    public Vector getWindowPosition() {
        return windowPosition;
    }

    public Vector getPosition() {
        return position;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    // Для постоянного изменения текста
    public void setText(String text) {
        this.text = text;
    }

    public void render() {
        /** Предварительный процесс **/
        // Высчитываем резултатирующее положение исиходя их положения заданного пользователем и положением
        // заданным от родительского компонента
        resultPosition.setX(position.getX() + windowPosition.getX());
        resultPosition.setY(position.getY() + windowPosition.getY());

        /** Отслеживаем нажатие мышки на компонент **/
        // Если пользователь нажал на поле ввода, то оно становится активным
        if (!(Mouse.getStartPosition().getX() > resultPosition.getX() & Mouse.getStartPosition().getX() < resultPosition.getX() + width &
                Mouse.getStartPosition().getY() > resultPosition.getY() - 22 & Mouse.getStartPosition().getY() < resultPosition.getY()) )
            if (Mouse.isPush()) enable = false;

        /** Рендеринг **/
        // Высчитываем прозрачность
        float alpha;
        if (enable) alpha = 1; else alpha = 0.5f;

        if (TextSystem.getInstance().getWidth(text + outText) > width - 10)
            minChar++;
        outText.delete(0, outText.length());
        outText.append(inputText.substring(minChar, inputText.length()));

        disableTextures();
        // Рисуем фон
        Quad.getInstance().changeColors(backgroundColor, backgroundColor, secondBackColor, secondBackColor);
        Quad.getInstance().render(new Vector(resultPosition.getX() - 2, resultPosition.getY() - 22, 0), width + 2, 22, GL_QUADS);

        // Рисуем рамку
        Quad.getInstance().changeColors(borderColor, borderColor, borderColor, borderColor);
        Quad.getInstance().render(new Vector(resultPosition.getX() - 2, resultPosition.getY() - 22, 0), width + 2, 22, GL_LINE_LOOP);

        enableTextures();
        // Рисуем текст
        if (enable) TextSystem.getInstance().render(resultPosition, text + outText + "|", new Color(1, 1, 1, alpha));
        else TextSystem.getInstance().render(resultPosition, text + outText + "", new Color(1, 1, 1, alpha));

        // Если компонент активен, то обновляем его содержимое
        if (enable) {
            // Получаем последний символ с клавиатуры
            inputText.append(Keyboard.getLastCharFromKeyboard());

            // Если был нажат backspace то удаляем лишние символы
            if (glfwGetKey(Core.window, GLFW_KEY_BACKSPACE) == 1 & glfwGetTime() > time + 0.1) {
                if (inputText.length() != 0)
                    inputText.deleteCharAt(inputText.length() - 1);
                time = glfwGetTime();
                if (minChar > 0)
                    minChar--;
            }

            if (glfwGetKey(Core.window, GLFW_KEY_ENTER) == 1) {
                if (!inputText.toString().equals("")) done = true;
            }
        }
    }

    public void setInputText(String text) {
        inputText.setLength(0);
        inputText.append(text);
    }

    // Обновление (управление поля ввода)
    public void update() {
        /** Отслеживаем нажатие мышки на компонент **/
        // Если пользователь нажал на поле ввода, то оно становится активным
        if (Mouse.getStartPosition().getX() > resultPosition.getX() & Mouse.getStartPosition().getX() < resultPosition.getX() + width &
                Mouse.getStartPosition().getY() > resultPosition.getY() - 22 & Mouse.getStartPosition().getY() < resultPosition.getY() )
            if (Mouse.isPush()) enable = true;
    }

    public boolean isDone() {
        boolean result = done;
        done = false;
        return result;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getInputText() {
        done = false;
        String out = inputText.toString();
        inputText.delete(0, inputText.length());
        return out;
    }
}
