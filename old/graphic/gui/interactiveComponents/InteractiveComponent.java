package net.djuke.engine.old.graphic.gui.interactiveComponents;

import net.djuke.engine.math.Vector;

/**
 * InteractiveComponent - интерактивный компонент
 * типа кнопок или полей ввода
 * Created by vadim on 12.06.16.
 */
public interface InteractiveComponent {

    Vector getWindowPosition();
    void render();
    void update();

}
