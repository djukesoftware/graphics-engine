package net.djuke.engine.old.graphic.gui.interactiveComponents;

import net.djuke.engine.old.Settings;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.old.graphic.primitives.Quad;
import net.djuke.engine.old.input.Mouse;
import net.djuke.engine.math.Vector;

import java.io.Serializable;

import static net.djuke.engine.old.graphic.GraphicUtils.disableTextures;
import static org.lwjgl.opengl.GL11.GL_LINE_LOOP;
import static org.lwjgl.opengl.GL11.GL_QUADS;

/**
 * ChechButton - обычный тригер
 * Created by Vadim on 09.07.2016.
 */
public class CheckButton implements InteractiveComponent, Serializable {

    private Vector selfPosition = new Vector();
    private Vector windowPosition = new Vector();
    private Vector resultPosition = new Vector();
    private Vector triggerPosition = new Vector();

    private Color borderColor = new Color(1, 1, 1, 0.25f);
    private Color triggerColor = new Color();

    private boolean trigger = false;

    public void render() {
        resultPosition.set(selfPosition.x + windowPosition.x, selfPosition.y + windowPosition.y, 0);
        disableTextures();

        Quad.getInstance().changeColors(
                Settings.getInstance().colorTheme.getDarker(0.05f),
                Settings.getInstance().colorTheme.getDarker(0.05f),
                Settings.getInstance().colorTheme.getDarker(0.1f),
                Settings.getInstance().colorTheme.getDarker(0.1f));
        Quad.getInstance().render(resultPosition, 18, 18, GL_QUADS);

        Quad.getInstance().changeColors(borderColor, borderColor, borderColor, borderColor);
        Quad.getInstance().render(resultPosition, 18, 18, GL_LINE_LOOP);


        triggerColor.set(Settings.getInstance().colorTheme.getRed(),
                Settings.getInstance().colorTheme.getGreen(),
                Settings.getInstance().colorTheme.getBlue(), 1);
        if (trigger) {triggerColor.setAlpha(1);
        }
        else triggerColor.setAlpha(0);

        triggerPosition.set(resultPosition.x + 3, resultPosition.y + 3, 0);
        Quad.getInstance().changeColors(triggerColor, triggerColor, triggerColor.getLighter(0.3f), triggerColor.getLighter(0.3f));
        Quad.getInstance().render(triggerPosition, 12, 12, GL_QUADS);
    }

    public void update() {
        resultPosition.set(selfPosition.x + windowPosition.x, selfPosition.y + windowPosition.y, 0);

        if (Mouse.getMouse().x > resultPosition.x & Mouse.getMouse().x < resultPosition.x + 18 &
                Mouse.getMouse().y > resultPosition.y & Mouse.getMouse().y < resultPosition.y + 18) {
            if (Mouse.isMouseRelease()) trigger = !trigger;
        }
    }

    public boolean getTrigger() {
        return trigger;
    }

    public void setTrigger(boolean trigger) {
        this.trigger = trigger;
    }

    public void setPosition(float x, float y, float z) {
        selfPosition.set(x, y, z);
    }

    public Vector getWindowPosition() {
        return windowPosition;
    }

    public Vector getPosition() {
        return selfPosition;
    }

}
