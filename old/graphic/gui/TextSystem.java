package net.djuke.engine.old.graphic.gui;

import net.djuke.engine.old.Logger;
import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.math.Vector;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.opengl.TextureImpl;
import org.newdawn.slick.util.ResourceLoader;

import java.awt.*;
import java.io.IOException;
import java.io.InputStream;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glPopMatrix;

/**
 * TextSystem - текстовая система для прорисовки текста
 * Created by vadim on 11.06.16.
 */
public class TextSystem {

    private static TextSystem instance;

    private static TrueTypeFont font;

    private TextSystem() {

        Logger.print("Loading font...");
        InputStream is = ResourceLoader.getResourceAsStream("FreeSans.ttf");
        Font awtFont = null;

        try {
            awtFont = Font.createFont(Font.TRUETYPE_FONT, is);
        } catch (IOException | FontFormatException e) {
            Logger.error("Error in loading fonts");
        }

        try {
            font = new TrueTypeFont(awtFont.deriveFont(15f), true);
        } catch (NullPointerException e) {
            Logger.error("Error in loading fonts [null]");
        }
    }

    public void render(Vector position, String text, Color color) {
        glPushMatrix();
        glTranslatef((int)position.getX(), (int)position.getY(), 0);
        glScalef(1, -1, 1);
        TextureImpl.bindNone();
        font.drawString(0, 0, text,
                new org.newdawn.slick.Color(
                        color.getRed(), color.getGreen(), color.getBlue(), color.getAlpha()));
        glPopMatrix();
    }

    public int getHeight (String text) {
        return font.getHeight(text);
    }

    public int getWidth (String text) {
        return font.getWidth(text);
    }

    public static TextSystem getInstance() {
        if (instance == null) instance = new TextSystem();
        return instance;
    }
}
