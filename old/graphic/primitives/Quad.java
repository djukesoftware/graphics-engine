package net.djuke.engine.old.graphic.primitives;

import net.djuke.engine.old.graphic.Color;
import net.djuke.engine.old.graphic.VBO;
import net.djuke.engine.math.Vector;

import static org.lwjgl.opengl.GL11.*;

/**
 * Quad - устарешвий класс для отрисовки квадрата
 * Created by vadim on 10.06.16.
 */
public class Quad {

    private static Quad instance;
    private VBO vbo = new VBO();
    private Color def = new Color(1, 1, 1, 1);

    private Quad() {
        load();
    }

    // Прогружаем наш квадрат
    private void load() {
        vbo.setColors(
                new Color(1, 1, 1, 1),
                new Color(1, 1, 1, 1),
                new Color(1, 1, 1, 1),
                new Color(1, 1, 1, 1));
        vbo.setVertex(
                new Vector(  0,   0, 0),
                new Vector(100,   0, 0),
                new Vector(100, 100, 0),
                new Vector(  0, 100, 0));
        vbo.setTextureVertex(
                new Vector(0, 1, 0),
                new Vector(1, 1, 0),
                new Vector(1, 0, 0),
                new Vector(0, 0, 0));
        vbo.create(true, true);
    }

    // Рисуем квадрат
    public void render(Vector position, int width, int height, int mode) {
        glPushMatrix();
        glTranslatef(position.getX(), position.getY(), 0);
        glScalef((float)width / 100.0f, (float)height / 100.0f, 0);
        vbo.render(mode);
        glPopMatrix();
    }

    // Изменяем цвета по необходимости
    public void changeColors(Color... colors) {
        vbo.changeColorBuffer(colors);
    }

    public static Quad getInstance() {
        if (instance == null) instance = new Quad();
        return instance;
    }

    // Сбросить настройки цветов
    public void defaultColors() {
        vbo.changeColorBuffer(def, def, def, def);
    }


}
