package net.djuke.engine.old.graphic;

import static org.lwjgl.opengl.GL11.*;

/**
 * GraphicUtils - класс для облегчённой работы с настройкой графики
 * Created by vadim on 18.06.16.
 */
public class GraphicUtils {

    public static void enableTextures() {
        glEnable(GL_TEXTURE_2D);
    }

    public static void disableTextures() {
        glDisable(GL_TEXTURE_2D);
    }

}
