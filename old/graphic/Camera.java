package net.djuke.engine.old.graphic;

import net.djuke.engine.math.Vector;

import static org.lwjgl.opengl.GL11.*;

/**
 * Camera - класс для работы с камерой
 * Created by vadim on 12.06.16.
 */
public class Camera {

    private static Camera instance;
    float rot;
    private Vector position = new Vector(0.0f, 0.0f, 0.0f);
    private Vector rotation = new Vector(5.0f, 0.0f, 0);

    private Camera() {

    }

    public void bind() {
        glPushMatrix();
        glMatrixMode(GL_MODELVIEW);
        rot = rotation.getY() * (float)Math.PI / 180;
        glRotatef(rotation.getY(), 0, 1, 0);
        //glRotatef(rotation.getX(), (float)Math.cos(rot), 0, (float)Math.sin(rot));
        glTranslatef( position.getX(), position.getY(),  position.getZ());
    }

    public void unbind() {
        glPopMatrix();
    }

    public void moveForward(float speed) {
        position.setX(position.getX() + (float)Math.cos(rot + Math.PI / 2) * speed);
        position.setZ(position.getZ() + (float)Math.sin(rot + Math.PI / 2) * speed);
    }

    public void strafe(float speed) {
        position.setX(position.getX() + (float)Math.cos(rot) * speed);
        position.setZ(position.getZ() + (float)Math.sin(rot) * speed);
    }


    public Vector getPosition() {
        return position;
    }

    public Vector getRotation() {
        return rotation;
    }

    public static Camera getInstance() {
        if (instance == null) instance = new Camera();
        return instance;
    }

}
