package net.djuke.engine.old.graphic;

import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Core;
import net.djuke.engine.math.Vector;
import org.lwjgl.BufferUtils;

import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
/**
 * VBO - класс для работы с VBO
 * Created by vadim on 27.05.16.
 */
public class VBO implements CoreObject {

    private int vertexbufferid;
    private int colorbufferid;
    private int texturebufferid;
    private FloatBuffer vertexBuffer;
    private FloatBuffer colorBuffer;
    private FloatBuffer textureBuffer;
    private Vector[] textureVectors;
    private Vector[] vectors;
    private Color[] colors;

    public VBO() {
        Core.addObject(this);
    }

    // Устанавливаем вершины
    public void setVertex(Vector... vectors) {
        this.vectors = vectors;
    }

    // Устанавливаем цвета
    public void setColors(Color... colors) {
        this.colors = colors;
    }

    // Устанавливаем координаты текстуры
    public void setTextureVertex(Vector... vectors) {
        textureVectors = vectors;
    }

    public void create(boolean vertexDynamic, boolean colorDynamic) {
        // Вершинный буфер
        if (colors != null) {
            vertexBuffer = BufferUtils.createFloatBuffer(vectors.length * 3);
            for (Vector vector : vectors)
                vertexBuffer.put(vector.getX()).put(vector.getY()).put(vector.getZ());
            vertexBuffer.flip();

            // Получаем имя буфера
            vertexbufferid = glGenBuffers();
            // Создаём точку связывания
            glBindBuffer(GL_ARRAY_BUFFER, vertexbufferid);
            // Выделяем память и загружаем в неё наши данные
            if (!vertexDynamic) glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
            else glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_DYNAMIC_DRAW);
        }

        // Цветовой буфер
        if (colors != null) {
            colorBuffer = BufferUtils.createFloatBuffer(colors.length * 4);
            for (Color color : colors)
                colorBuffer.put(color.getRed()).put(color.getGreen()).put(color.getBlue()).put(color.getAlpha());
            colorBuffer.flip();

            // Создаём цветовой буфер
            colorbufferid = glGenBuffers();
            // Соеденяем буффер с его ID
            glBindBuffer(GL_ARRAY_BUFFER, colorbufferid);
            // Загружаем цветовой буфер
            if (!colorDynamic) glBufferData(GL_ARRAY_BUFFER, colorBuffer, GL_STATIC_DRAW);
            else glBufferData(GL_ARRAY_BUFFER, colorBuffer, GL_DYNAMIC_DRAW);
        }

        if (textureVectors != null) {
            textureBuffer = BufferUtils.createFloatBuffer(textureVectors.length * 2);
            for (Vector vector : textureVectors)
                textureBuffer.put(vector.getX()).put(vector.getY());
            textureBuffer.flip();

            texturebufferid = glGenBuffers();
            glBindBuffer(GL_ARRAY_BUFFER, texturebufferid);
            glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_STATIC_DRAW);
        }

        textureBuffer.clear();
        textureBuffer = null;
        colorBuffer.clear();
        vertexBuffer.clear();
        vertexBuffer = null;
        colors = null;
        textureVectors = null;

        System.gc();
    }

    public void render(int type) {
        glBindBuffer(GL_ARRAY_BUFFER, vertexbufferid);
        glVertexPointer(3, GL_FLOAT, 0, 0L);

        glBindBuffer(GL_ARRAY_BUFFER, colorbufferid);
        glColorPointer(4, GL_FLOAT, 0, 0L);

        glBindBuffer(GL_ARRAY_BUFFER, texturebufferid);
        glTexCoordPointer(2, GL_FLOAT, 0, 0L);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_COLOR_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glDrawArrays(type, 0, vectors.length);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_COLOR_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
    }

    public void changeColorBuffer(Color... colors) {
        // Очищаем буфер
        colorBuffer.clear();
        // Заполняем новыми значениями
        for (Color color : colors)
            colorBuffer.put(color.getRed()).put(color.getGreen()).put(color.getBlue()).put(color.getAlpha());
        // Заканчиваем запись и переходим в режим чтения
        colorBuffer.flip();

        // Начинаем переписывать цветовой буфер
        // Подключаем цветовой буфер
        glBindBuffer(GL_ARRAY_BUFFER, colorbufferid);
        // переписываем буфер
        glBufferSubData(GL_ARRAY_BUFFER, 0, colorBuffer);
    }

    public void changeVertexBuffer(Vector... vectors) {
        // Очищаем буфер
        vertexBuffer.clear();
        // Заполняем новыми значениями
        for (Vector vector : vectors)
            vertexBuffer.put(vector.getX()).put(vector.getY()).put(vector.getZ());
        // Заканчиваем запись и переходим в режим чтения
        vertexBuffer.flip();

        // Начинаем переписывать цветовой буфер
        // Подключаем цветовой буфер
        glBindBuffer(GL_ARRAY_BUFFER, vertexbufferid);
        // переписываем буфер
        glBufferSubData(GL_ARRAY_BUFFER, 0, vertexBuffer);
    }

    public Vector[] getVectors() {
        return vectors;
    }

    public Color[] getColors() {
        return colors;
    }

    public void delete() {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glDeleteBuffers(colorbufferid);
        glDeleteBuffers(vertexbufferid);
    }

    public void update() {}

}
