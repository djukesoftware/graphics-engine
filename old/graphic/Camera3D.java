package net.djuke.engine.graphics.theatre.camera;

import net.djuke.engine.math.Matrix;
import net.djuke.engine.math.Matrix4;
import net.djuke.engine.math.Vector;

/**
 * Camera for 3D scene
 * Created by azusa on 3/16/17.
 */
public class Camera3D implements Camera {

    private Matrix4 matrix = new Matrix4();
    private Vector position = new Vector(0,0,0);
    private Vector rotation = new Vector(0,0,0);
    private float fov;
    private float near;
    private float far;

    public Matrix getMatrix() {
        matrix.createIdentity();
        // Я надеюсь, что я не спутал последовательность умнажения матриц
        matrix.rotate(rotation);
        matrix.translate(position);
        matrix.perspective(fov, near, far);
        return matrix;
    }

    public void translate(float x, float y, float z) {
        position.set(x, y, z);
    }

    public void rotate(float Rx, float Ry, float Rz) {
        rotation.set(Rx, Ry, Rz);
    }

    public void perspective(float fov, float near, float far) {
        this.fov = fov;
        this.near = near;
        this.far = far;
    }


}
