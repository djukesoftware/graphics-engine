package net.djuke.engine.old.graphic;

import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Logger;

import java.io.*;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL40.*;

/**
 * Shaders - класс для загрузки и использовании шейдеров
 * Created by vadim on 31.05.16.
 */
public class Shader implements CoreObject {

    // идентификаторы под шейдерную программу
    private int shaderProgram = -1;
    private int vertexShader = -1;
    private int fragmentShader = -1;
    private int tessellationShader = -1;
    private int tessellationEvaluationShader = -1;
    private int geometryShader = -1;

    public void loadFragmentShader(String path) {
        Logger.print("Loading fragment shader: " + path);
        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

        glShaderSource(fragmentShader, loadSource(path));
        glCompileShader(fragmentShader);
        Logger.print("[fragmentShader] " + glGetShaderInfoLog(fragmentShader));
    }

    public void loadTessellationShader(String path) {
        Logger.print("Loading tessellation shader: " + path);
        tessellationShader = glCreateShader(GL_TESS_CONTROL_SHADER);

        glShaderSource(tessellationShader, loadSource(path));
        glCompileShader(tessellationShader);
        Logger.print("[tessellation shader] " + glGetShaderInfoLog(tessellationShader));
    }

    public void loadTessellationEvaluationShader(String path) {
        Logger.print("Loading tessellation evaluation shader: " + path);
        tessellationEvaluationShader = glCreateShader(GL_TESS_EVALUATION_SHADER);

        glShaderSource(tessellationEvaluationShader, loadSource(path));
        glCompileShader(tessellationEvaluationShader);
        Logger.print("[tessellation evaluation shader] " + glGetShaderInfoLog(tessellationEvaluationShader));
    }

    public void loadGeometryShader(String path) {
        Logger.print("Loading geometry shader: " + path);

        glShaderSource(geometryShader, loadSource(path));
        glCompileShader(geometryShader);
        Logger.print("[geometry shader] " + glGetShaderInfoLog(geometryShader));
    }

    public void loadVertexShader(String path) {
        Logger.print("Loading vertex shader: " + path);
        // создаём идентификаторы под шейдерные программы
        vertexShader = glCreateShader(GL_VERTEX_SHADER);

        // На идентификаторы шейдеров загружаем исходный код
        glShaderSource(vertexShader, loadSource(path));
        // Компилируем шейдеры
        glCompileShader(vertexShader);
        Logger.print("[vertexShader] " + glGetShaderInfoLog(vertexShader));
    }

    public void createShaderProgram() {
        shaderProgram = glCreateProgram();
        // Прикрепляем два шейдера к основной шейдерной программе
        if (vertexShader != -1) glAttachShader(shaderProgram, vertexShader);
        if (fragmentShader != -1) glAttachShader(shaderProgram, fragmentShader);
        if (tessellationShader != -1) glAttachShader(shaderProgram, tessellationShader);
        if (tessellationEvaluationShader != -1) glAttachShader(shaderProgram, tessellationEvaluationShader);
        if (geometryShader != -1) glAttachShader(shaderProgram, geometryShader);
        // Линкуем прикреплённые шейдеры в одну шейдерную программу
        glLinkProgram(shaderProgram);
        // Проверяем, может ли программа исполняться в данном OpenGL контексте
        glValidateProgram(shaderProgram);

        System.gc();
    }

    // Подключет программу указанную в shaderProgram к текущему этапу рендеринга
    public void bind() {
        glUseProgram(shaderProgram);
    }

    // Подключаем совершенно пустую программу
    public void unbind() {
        glUseProgram(0);
    }

    // Очищаем видеопамять
    public void delete() {
        glUseProgram(0);
        if (geometryShader != -1) glDeleteProgram(geometryShader);
        if (vertexShader != -1) glDeleteShader(vertexShader);
        if (fragmentShader != -1) glDeleteShader(fragmentShader);
        if (shaderProgram != -1) glDeleteProgram(shaderProgram);
        if (tessellationShader != -1) glDeleteProgram(tessellationShader);
        if (tessellationEvaluationShader != -1) glDeleteShader(tessellationEvaluationShader);
    }

    // Пустой неиспользованный метод
    public void load() {}

    // Передача данных в шейдер
    public void uniformFloat(String varName, float var) {
        // Передаём в неё данные
        glUniform1f(glGetUniformLocation(shaderProgram, varName), var);
    }

    // Передача вектора в шейдер
    public void uniformVector(String varName, float x, float y, float z) {
        glUniform3f(glGetUniformLocation(shaderProgram, varName), x, y, z);
    }

    public void attributeVector(String varName, float x, float y, float z, float w) {
        glVertexAttrib4f(glGetAttribLocation(shaderProgram, varName), x, y, z, w);
    }

    public void uniformTexture(String textureName, int var) {
        // Получаем местоположение переменной в шейдерной программе
        glUniform1i(glGetUniformLocation(shaderProgram, textureName), var);
    }

    public void uniformMatrix(String matrixName, FloatBuffer buffer) {
        //glUniformMatrix4(glGetUniformLocation(shaderProgram, matrixName), false, buffer);
    }

    public void attributeFloat(String varName, float var) {
        glVertexAttrib1f(glGetAttribLocation(shaderProgram, varName), var);
    }

    public void attributeMatrix(String matrixName, FloatBuffer buffer) {
        //glVertexAttrib4(glGetAttribLocation(shaderProgram, matrixName), buffer);
    }

    public void update() {  }


    public StringBuilder loadSource(String path) {
        StringBuilder source = new StringBuilder();
        // Заружаем исходники шейдерных программ из указанных файлов
        BufferedReader reader;
        try {
            // Построчно загружаем вершинный шейдер
            File file = new File(path);
            FileInputStream fis = new FileInputStream(file);
            reader = new BufferedReader(new InputStreamReader(fis));
            String line;
            while ((line = reader.readLine()) != null)
                source.append(line.split("//")[0]).append("\n");
            reader.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return source;
    }
}
