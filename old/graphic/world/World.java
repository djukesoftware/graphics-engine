package net.djuke.engine.old.graphic.world;

import java.util.ArrayList;

/**
 * World - трёхмерный мир, отрисованный движком
 * Created by Vadim on 07.07.2016.
 */
public class World {

    private static World instance = new World();
    private ArrayList<WorldObject> objects = new ArrayList<>();

    public void render() {
        objects.forEach(WorldObject::render);
    }

    public void addObject(WorldObject object) {
        objects.add(object);
    }

    public static World getInstance() {
        return instance;
    }

}
