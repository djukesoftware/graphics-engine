package net.djuke.engine.old.graphic.world;

/**
 * WorldObject - обьект трёхмерного мира
 * Created by Vadim on 07.07.2016.
 */
public interface WorldObject {

    public void render();

}
