package net.djuke.engine.old.graphic;

import net.djuke.engine.old.CoreObject;
import net.djuke.engine.old.Core;
import net.djuke.engine.old.Resources;
import net.djuke.engine.math.Vector;
import org.lwjgl.BufferUtils;

import java.io.*;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;

public class Model implements CoreObject {

    // Характеристики модели
    private Vector position;
    private Vector rotation;
    private Vector scale;

    private int facesCounter = 0;
    private int vboVertexBuffer;
    private int vboTextureBuffer;
    private int vboNormalBuffer;

    public Model() {
        Core.addObject(this);
        rotation = new Vector();
        position = new Vector();
        scale = new Vector(1, 1, 1);
    }

    public void update() { }

    public void load(String modelPath, boolean hasTexture, boolean archive) {

        List<Vector> vertices = new ArrayList();
        List<Vector> normals = new ArrayList();
        List<Vector> textureCoords = new ArrayList();
        List<Face> faces = new ArrayList();

        /** Загружаем данные из текстового файла **/
        BufferedReader reader;
        float x, y, z;
        try {
            if (archive) {
                reader = new BufferedReader(new InputStreamReader(Resources.getInputStream(modelPath)));
            } else {
                File file = new File(modelPath);
                FileInputStream fis = new FileInputStream(file);
                InputStreamReader isr = new InputStreamReader(fis);
                reader = new BufferedReader(isr);
            }
            String line;
            while ((line = reader.readLine()) != null) {
                // Зугражаем вершины
                if (line.startsWith("v ")) { // vertices
                    x = Float.valueOf(line.split(" ")[1]);
                    y = Float.valueOf(line.split(" ")[2]);
                    z = Float.valueOf(line.split(" ")[3]);
                    vertices.add(new Vector(x, y, z));
                }
                // Загружаем нормали
                if (line.startsWith("vn ")) { //normals
                    x = Float.valueOf(line.split(" ")[1]);
                    y = Float.valueOf(line.split(" ")[2]);
                    z = Float.valueOf(line.split(" ")[3]);
                    normals.add(new Vector(x, y, z));
                }
                // Загржуаем координаты текстур
                if (line.startsWith("vt ")) { //texture coordinates
                    x = Float.valueOf(line.split(" ")[1]);
                    y = Float.valueOf(line.split(" ")[2]);
                    textureCoords.add(new Vector(x, y, 0));
                }
                // Загружаем полигоны (лицы)
                if (line.startsWith("f ")) { // faces
                    faces.add(new Face()); // create face
                    faces.get(facesCounter).vertex1 = Integer.valueOf(line.split(" ")[1].split("/")[0]) - 1;
                    if (hasTexture) faces.get(facesCounter).texture1 = Integer.valueOf(line.split(" ")[1].split("/")[1]) - 1;
                    faces.get(facesCounter).normal1 = Integer.valueOf(line.split(" ")[1].split("/")[2]) - 1;
                    faces.get(facesCounter).vertex2 = Integer.valueOf(line.split(" ")[2].split("/")[0]) - 1;
                    if (hasTexture) faces.get(facesCounter).texture2 = Integer.valueOf(line.split(" ")[2].split("/")[1]) - 1;
                    faces.get(facesCounter).normal2 = Integer.valueOf(line.split(" ")[2].split("/")[2]) - 1;
                    faces.get(facesCounter).vertex3 = Integer.valueOf(line.split(" ")[3].split("/")[0]) - 1;
                    if (hasTexture) faces.get(facesCounter).texture3 = Integer.valueOf(line.split(" ")[3].split("/")[1]) - 1;
                    faces.get(facesCounter).normal3 = Integer.valueOf(line.split(" ")[3].split("/")[2]) - 1;
                    facesCounter++;
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        }

        /** Создаём буферы **/
        // Вершинный буфер
        FloatBuffer vertexBuffer = BufferUtils.createFloatBuffer(facesCounter * 9);
        Face face;
        for (int counter = 0; counter < facesCounter; counter++) {
            face = faces.get(counter);
            vertexBuffer.put(vertices.get(face.vertex1).x).put(vertices.get(face.vertex1).y).put(vertices.get(face.vertex1).z);
            vertexBuffer.put(vertices.get(face.vertex2).x).put(vertices.get(face.vertex2).y).put(vertices.get(face.vertex2).z);
            vertexBuffer.put(vertices.get(face.vertex3).x).put(vertices.get(face.vertex3).y).put(vertices.get(face.vertex3).z);
        }
        vertexBuffer.flip();
        vertices.clear();
        vertices = null;
        // Буфер нормалей
        FloatBuffer normalBuffer = BufferUtils.createFloatBuffer(facesCounter * 9);
        for (int counter = 0; counter < facesCounter; counter++) {
            face = faces.get(counter);
            normalBuffer.put(normals.get(face.normal1).x).put(normals.get(face.normal1).y).put(normals.get(face.normal1).z);
            normalBuffer.put(normals.get(face.normal2).x).put(normals.get(face.normal2).y).put(normals.get(face.normal2).z);
            normalBuffer.put(normals.get(face.normal3).x).put(normals.get(face.normal3).y).put(normals.get(face.normal3).z);
        }
        normalBuffer.flip();
        normals.clear();
        normals = null;
        // Буфер текстурных координат
        FloatBuffer textureBuffer = BufferUtils.createFloatBuffer(facesCounter * 9);
        for (int counter = 0; counter < facesCounter; counter++) {
            face = faces.get(counter);
            textureBuffer.put(textureCoords.get(face.texture1).x).put(textureCoords.get(face.texture1).y);
            textureBuffer.put(textureCoords.get(face.texture2).x).put(textureCoords.get(face.texture2).y);
            textureBuffer.put(textureCoords.get(face.texture3).x).put(textureCoords.get(face.texture3).y);
        }
        textureBuffer.flip();
        textureCoords.clear();
        textureCoords = null;
        faces.clear();
        faces = null;
        // Очищаем оперативную память
        System.gc();

        /** Создаём Vertex Buffer Object **/
        vboVertexBuffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vboVertexBuffer);
        glBufferData(GL_ARRAY_BUFFER, vertexBuffer, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        vboNormalBuffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vboNormalBuffer);
        glBufferData(GL_ARRAY_BUFFER, normalBuffer, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        vboTextureBuffer = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vboTextureBuffer);
        glBufferData(GL_ARRAY_BUFFER, textureBuffer, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);

        // Удаляем созданные в JVM буферы
        vertexBuffer.clear();
        vertexBuffer = null;
        normalBuffer.clear();
        normalBuffer = null;
        textureBuffer.clear();
        textureBuffer = null;
        // Очищаем память
        System.gc();
    }

    public void render(int type) {
        glBindBuffer(GL_ARRAY_BUFFER, vboVertexBuffer);
        glVertexPointer(3, GL_FLOAT, 0, 0L);

        glBindBuffer(GL_ARRAY_BUFFER, vboNormalBuffer);
        glNormalPointer(GL_FLOAT, 0, 0L);

        glBindBuffer(GL_ARRAY_BUFFER, vboTextureBuffer);
        glTexCoordPointer(2, GL_FLOAT, 0, 0L);

        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glDrawArrays(type, 0, facesCounter * 9);
        glDisableClientState(GL_VERTEX_ARRAY);
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    }

    public void delete() {
        glDeleteBuffers(vboVertexBuffer);
        glDeleteBuffers(vboNormalBuffer);
        glDeleteBuffers(vboTextureBuffer);
    }

    // Класс одного полигона
    private class Face {
        // Вершины
        int vertex1;
        int vertex2;
        int vertex3;
        // Нормали
        int normal1;
        int normal2;
        int normal3;
        // Текстура
        int texture1;
        int texture2;
        int texture3;
    }

    public Vector getPosition() {
        return position;
    }

    public Vector getRotation() {
        return rotation;
    }

    public Vector getScale() {
        return scale;
    }

    public void setPosition(Vector position) {
        this.position = position;
    }

    public void setRotation(Vector rotation) {
        this.rotation = rotation;
    }

    public void setScale(Vector scale) {
        this.scale = scale;
    }
}
