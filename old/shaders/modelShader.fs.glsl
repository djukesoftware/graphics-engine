#version 130

// Fragment Shader

in vec4 position;
in vec3 normals;
uniform sampler2D texture;
uniform float time;

void main() {
    // Устаналиваем цвет фрагмента
    vec4 texColor = texture(texture, vec2(gl_TexCoord[0].st.x, gl_TexCoord[0].st.y));

    vec4 color = vec4(texColor.x, texColor.y, texColor.z, 1.0);

    gl_FragColor = color;
}

