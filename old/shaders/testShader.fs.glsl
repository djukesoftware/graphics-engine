#version 430 core

out vec4 color;

in VS_OUT {
    vec4 color;
} fs_in;

void main() {
    if (gl_FragCoord.x > 400) {
        color = vec4(0, 1, 0, 1);
    } else {
        color = vec4(0, 0, 1, 1);
    }
}