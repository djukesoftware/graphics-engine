#version 430 core

layout (triangles) in; // берём на обработку треугольники
layout (points, max_vertices = 3) out; // говорим OpenGL что на выходе мы будет отдавать три точки

void main(void) {
    for (int i = 0; i < gl_in.length(); i++) {
        gl_Position = gl_in[i].gl_Position; // не изменяем позицию текущей точки
        EmitVertex(); // генерируем вектор, как выходные данные геометрического шейдера
    }
}