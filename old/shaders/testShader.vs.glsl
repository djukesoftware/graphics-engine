#version 430 core

//gl_VertexID - id of current vertex in vertex shader
// it is vertex attribute to OpenGL Pipeline
layout (location = 0) in vec4 offset;
layout (location = 1) in vec4 color;

out VS_OUT {
    vec4 color;
} vs_out;

void main() {
    const vec4 vertices[3] = vec4[3](
        vec4(0.25, -0.25, 0.5, 1.0),
        vec4(-0.25, -0.25, 0.5, 1.0),
        vec4(0.25, 0.25, 0.5, 1.0)
    );
    gl_Position = vec4(vertices[gl_VertexID]+offset) * vec4(1,-1,1,1); // last number is 1.0 because shaders use 4x4 matrix
    vs_out.color = color;
}