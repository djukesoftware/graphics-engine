#version 430 core

layout (vertices = 3) out; // количество точек на путь или количество точек, которые
// собираются в группы для обработки tessellation control shader

void main(void) {
    // Если в группе контрольных точек, которые обрабатываются данным шейдером
    // мы имеем дело с первой точкой в группе, то мы редактируем уровень тесселяции
    if (gl_InvocationID == 0) {
        // ...
        gl_TessLevelInner[0] = 5.0;
        // ...
        gl_TessLevelOuter[0] = 5.0;
        gl_TessLevelOuter[1] = 5.0;
        gl_TessLevelOuter[2] = 5.0;
    }
    // Всё что получаем, просто передаём дальше по pipeline
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}