#version 130

attribute vec4 coord;
varying vec2 texCoords;

void main() {
    gl_Position = gl_ModelViewProjectionMatrix * coord;
    gl_FrontColor = gl_Color;
    texCoords = gl_MultiTexCoord0.st;
}