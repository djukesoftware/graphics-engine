#version 130

// Vertex Shader

out vec4 position;
out vec3 normals;
uniform float time;
uniform mat4 gameMatrix;

void main() {
    // Устанавливаем местоположение вершины
    vec4 offset = vec4(sin(time), 0, 0, 0);
    gl_Position = (gl_Vertex + offset) * gameMatrix;
    position = gl_Vertex;
    normals = gl_Normal;

    gl_TexCoord[0].st = gl_MultiTexCoord0.st;
}

