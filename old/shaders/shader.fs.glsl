#version 130

uniform vec4 color;
uniform sampler2D tex;
varying vec2 texCoords;

uniform float k_var;

void main() {
    vec4 currentColor = texture2D(tex, texCoords);
    gl_FragColor = vec4(
        (currentColor.x + currentColor.y + currentColor.z) / 3.0,
        (currentColor.x + currentColor.y + currentColor.z) / 3.0,
        (currentColor.x + currentColor.y + currentColor.z) / 3.0, currentColor.w);
}