package net.djuke.engine.old;

/**
 * net.djuke.engine.old.CoreObject - интерфейс описывающий стандартный обьект ядра
 * с удалением данных и обновлением.
 * Created by vadim on 10.06.16.
 */
public interface CoreObject {

    void delete();
    void update();

}
