package net.djuke.game.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import net.djuke.engine.data.converters.ConvertException;
import net.djuke.engine.data.converters.Converter;
import net.djuke.game.mechanics.GObject;

import java.io.*;

/**
 * GameObjectConverter
 *
 * @author Vadim
 * @version 1.0 29.11.2017
 */
public class GObjectConverter implements Converter<GObject> {

    private Gson gson;

    public GObjectConverter() {
        GsonBuilder builder = new GsonBuilder();
        //builder.registerTypeAdapter(GObject.class, new GObjectAdapter());
        builder.registerTypeAdapter(GObject.class, new GObjectSerializer());
        builder.registerTypeAdapter(GObject.class, new GObjectDeserializer());

        builder.setPrettyPrinting();
        gson = builder.create();
    }

    @Override
    public String getFormat() {
        return "gbj";
    }

    /**
     * Saves object to JSON file
     * @param object object for saving
     * @param bos output stream
     * @throws ConvertException
     */
    @Override
    public void save(GObject object, BufferedOutputStream bos) throws ConvertException {
        OutputStreamWriter writer = new OutputStreamWriter(bos);
        try {
            writer.write("// " + object.getClass().getName() + "\n");
            writer.write(gson.toJson(object));
            writer.close();
        } catch (IOException e) {
            throw new ConvertException(e);
        }
    }

    @Override
    public GObject load(BufferedInputStream bis) throws ConvertException {
        Class<?> objectClass = null; // class of returned object

        // Read source of json file
        BufferedReader reader = new BufferedReader(new InputStreamReader(bis));
        StringBuilder json = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("// ")) {
                    objectClass = Class.forName(line.split(" ")[1]);
                } else json.append(line);
            }
        } catch (ClassNotFoundException | IOException e) {
            throw new ConvertException(e);
        }

        // Return object if file contain information about his class
        try {
            if (objectClass != null) {
                if (GObject.class.isAssignableFrom(objectClass)) {
                    System.out.println(objectClass.getName());
                    return (GObject)gson.fromJson(json.toString(), objectClass);
                } else throw new ConvertException("It is not game object");
            }
            else throw new ConvertException("File don't contain any information about class");
        } catch (JsonSyntaxException e) {
            throw new ConvertException(e.getMessage());
        }
    }
}
