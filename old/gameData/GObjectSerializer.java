package net.djuke.game.data;

import com.google.gson.*;
import net.djuke.game.mechanics.GObject;

import java.lang.reflect.Type;

/**
 * GObjectSerializer
 *
 * @author Vadim
 * @version 1.0 01.12.2017
 */
public class GObjectSerializer implements JsonSerializer<GObject> {

    @Override
    public JsonElement serialize(GObject src, Type typeOfSrc, JsonSerializationContext context) {
        if (src instanceof GImmutable) {
            return new JsonPrimitive("hrdlnk:" + src.getClass().getName());
        } else {
            JsonElement element = context.serialize(src, Object.class);
            if (element.isJsonObject()) {
                JsonObject object = element.getAsJsonObject();
                object.add("class", new JsonPrimitive(src.getClass().getName()));
                return object;
            } else {
                return element;
            }
        }
        /*
        if (src instanceof GContainer) {

            System.out.println("Yes, I am container");

            // Обрабатыаем как один объект
        } else {

            // Если такой объект существует, то возвращаем его класс
            if (GData.getInstance().contains(src.getClass())) {
                return new JsonPrimitive(src.getClass().getName());
            } else {
                // Если объекта нет, то нам надо записать его нормально
                return context.serialize(src);
            }

        }
        return null;*/
    }

}
