package net.djuke.game.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import net.djuke.game.mechanics.GObject;

import java.io.IOException;

/**
 * GameObjectAdapter
 *
 * @author Vadim
 * @version 1.0 29.11.2017
 */
public class GObjectAdapter extends TypeAdapter<GObject> {

    private TypeAdapter<Object> adapter;

    public GObjectAdapter() {
        Gson gson = (new GsonBuilder()).create();
        adapter = gson.getAdapter(Object.class);
    }

    @Override
    public void write(JsonWriter out, GObject value) throws IOException {
        if (value == null) {
            out.nullValue();
            return;
        }
        // Проверяем, есть ли такой объект в коллекции неизменяемых объектов
        System.out.println(value.getClass());
        //if (GData.getInstance().contains(value.getClass())) {
            // В таком случае, мы просто запишем имя этого класа для дальнейшего использования
        //    out.value("hrdlnk:" + value.getClass().getName());
        //} else {
            // Если это какой-нибудь уникальный объект, то мы просто конвертируем его в JSON
            // adapter.write(out, value);
        //}
    }

    @Override
    public GObject read(JsonReader in) throws IOException {

        return null;
    }

}
