package net.djuke.game.data;

import com.google.gson.*;
import net.djuke.game.mechanics.GObject;

import java.lang.reflect.Type;

/**
 * GObjectDesializer
 *
 * @author Vadim
 * @version 1.0 01.12.2017
 */
public class GObjectDeserializer implements JsonDeserializer<GObject> {

    private Gson gson = (new GsonBuilder()).create();

    @Override
    public GObject deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (json instanceof JsonPrimitive) {
            // тут типа по строке мы будем находить наш синглетон
            System.out.println(json.getAsString());
            return null;
        } else {
            if (json.isJsonObject()) {
                JsonObject object = json.getAsJsonObject();
                String className = object.get("class").getAsString();
                try {
                    System.out.println(className);
                    return (GObject)gson.fromJson(json, Class.forName(className));
                } catch (ClassNotFoundException e) {
                    throw new JsonParseException(e.getMessage());
                }
            } else {
                throw new JsonParseException("Is not object");
            }
        }
    }

}
