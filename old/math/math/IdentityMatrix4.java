package net.djuke.engine.math;

/**
 * IdentityMatrix4
 *
 * @author Vadim
 * @version 1.0 17.07.2017
 */
public class IdentityMatrix4 extends Matrix {

    public IdentityMatrix4() {
        super(4,4);
        set(0,0,1);
        set(1,1,1);
        set(2,2,1);
        set(3,3,1);
    }

}
