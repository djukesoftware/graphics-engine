package net.djuke.engine.math;

import java.io.Serializable;

/**
 * Vector4
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class Vector4 implements Serializable {

    private float x;
    private float y;
    private float z;
    private float w;

    public Vector4() {

    }

    public Vector4(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public Vector4(Vector3 vector, float w) {
        this.x = vector.getX();
        this.y = vector.getY();
        this.z = vector.getZ();
        this.w = w;
    }

    public Vector4(Vector2 vector, float z, float w) {
        this.z = z;
        this.w = w;
        this.x = vector.getX();
        this.y = vector.getY();
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getW() {
        return w;
    }

    public void setW(float w) {
        this.w = w;
    }

    public void normalize() {
        x /= getLength();
        y /= getLength();
        z /= getLength();
        w /= getLength();
    }

    public float getLength() {
        return (float)Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2) + Math.pow(w,2));
    }

    public Matrix toMatrix() {
        Matrix matrix = new Matrix(4,1);
        matrix.set(0,0,x);
        matrix.set(1,0,y);
        matrix.set(2,0,z);
        matrix.set(3,0,w);
        return matrix;
    }

    public void fromMatrix(Matrix matrix) {
        if (matrix.getColumns() == 1 && matrix.getRows() == 4) {
            x = matrix.get(0,0);
            y = matrix.get(1,0);
            z = matrix.get(2,0);
            w = matrix.get(3,0);
        }
    }

    public Vector3 getProjection() {
        return new Vector3(x/w, y/w, z/w);
    }
}
