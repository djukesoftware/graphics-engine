package net.djuke.engine.math;

/**
 * TranslationMatrix3
 *
 * @author Vadim
 * @version 1.0 30.06.2017
 */
public class TranslationMatrix3 extends Matrix {

    private float x;
    private float y;

    public TranslationMatrix3() {
        super(3,3);
        update();
    }

    public TranslationMatrix3(float x, float y) {
        super(3,3);
        this.x = x;
        this.y = y;
        update();
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        update();
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        update();
    }

    private void update() {
        set(0,0,1);
        set(1,1,1);
        set(2,2,1);
        set(0,2,x);
        set(1,2,y);
    }
}
