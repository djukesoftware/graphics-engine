package net.djuke.engine.math;

/**
 * PlaneScaleMatrix
 *
 * @author Vadim
 * @version 1.0 28.06.2017
 */
public class ScaleMatrix3 extends Matrix {

    private float x;
    private float y;

    public ScaleMatrix3() {
        super(3,3);
        update();
    }

    public ScaleMatrix3(float x, float y) {
        super(3,3);
        this.x = x;
        this.y = y;
        update();
    }

    public void setX(float x) {
        this.x = x;
        update();
    }

    public void setY(float y) {
        this.y = y;
        update();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void update() {
        set(0,0,x);
        set(1,1,y);
        set(2,2,1);
    }
}
