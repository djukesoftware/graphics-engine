package net.djuke.engine.math;

/**
 * RotationMatrix
 *
 * @author Vadim
 * @version 1.0 28.06.2017
 */
public class RotationMatrix3 extends Matrix {

    private float angle;

    public RotationMatrix3() {
        super(3,3);
        update();
    }

    public RotationMatrix3(float angle) {
        super(3,3);
        this.angle = angle;
        update();
    }

    public void setAngle(float angle) {
        this.angle = angle;
        update();
    }

    public float getAngle() {
        return angle;
    }

    private void update() {
        set(0,0, (float)Math.cos(angle));
        set(1,0, (float)Math.sin(angle));
        set(0,1, -(float)Math.sin(angle));
        set(1,1, (float)Math.cos(angle));
        set(2,2,1.0f);
    }
}
