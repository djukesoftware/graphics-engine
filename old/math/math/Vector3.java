package net.djuke.engine.math;

import java.io.Serializable;

/**
 * Vector - 3D vector for math
 * Created by vadim on 30.05.16.
 */
public class Vector3 implements Serializable {

    private float x;
    private float y;
    private float z;

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3() {

    }

    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3(Vector2 vector, float z) {
        x = vector.getX();
        y = vector.getY();
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void normalize() {
        x /= getLength();
        y /= getLength();
        z /= getLength();
    }

    public float getLength() {
        return (float)Math.sqrt(Math.pow(x, 2.0) + Math.pow(y, 2.0) + Math.pow(z, 2.0));
    }

    public Matrix toMatrix3() {
        Matrix matrix = new Matrix(3,1);
        matrix.set(0,0, x);
        matrix.set(1,0, y);
        matrix.set(2,0, z);
        return matrix;
    }

    public Matrix toMatrix4() {
        Matrix matrix = new Matrix(4,1);
        matrix.set(0,0,x);
        matrix.set(1,0,y);
        matrix.set(2,0,z);
        matrix.set(3,0,1);
        return matrix;
    }

    public void fromMatrix(Matrix matrix) {
        if (matrix.getRows() == 3 && matrix.getColumns() == 1) {
            x = matrix.get(0,0);
            y = matrix.get(1,0);
            z = matrix.get(2,0);
        }
    }

    public Vector2 getProjection() {
        return new Vector2(x/z, y/z);
    }

}
