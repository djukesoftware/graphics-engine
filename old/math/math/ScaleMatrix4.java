package net.djuke.engine.math;

/**
 * ScaleMatrix
 *
 * @author Vadim
 * @version 1.0 29.06.2017
 */
public class ScaleMatrix4 extends Matrix {

    private float x = 1f;
    private float y = 1f;
    private float z = 1f;

    public ScaleMatrix4() {
        super(4,4);
        update();
    }

    public ScaleMatrix4(float x, float y, float z) {
        super(4,4);
        this.x = x;
        this.y = y;
        this.z = z;
        update();
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        update();
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        update();
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
        update();
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        update();
    }

    private void update() {
        set(0,0,x);
        set(1,1,y);
        set(2,2,z);
        set(3,3,1);
    }
}
