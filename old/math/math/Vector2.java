package net.djuke.engine.math;

import java.io.Serializable;

/**
 * Vector2 - two dimensional vector
 *
 * @author Vadim
 * @version 1.0 28.06.2017
 */
public class Vector2 implements Serializable {

    private float x;
    private float y;

    public Vector2() {

    }

    public Vector2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void set(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void normalize() {
        x /= getLength();
        y /= getLength();
    }

    public float getLength() {
        return (float)Math.sqrt(Math.pow(x,2) + Math.pow(y,2));
    }

    public Matrix toMatrix() {
        Matrix matrix = new Matrix(2,1);
        matrix.set(0,0,x);
        matrix.set(1,0,y);
        return matrix;
    }

    public void fromMatrix(Matrix matrix) {
        if (matrix.getColumns() == 1 && matrix.getRows() == 2) {
            x = matrix.get(0,0);
            y = matrix.get(1,0);
        }
    }
}
