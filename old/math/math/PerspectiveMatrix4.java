package net.djuke.engine.math;

/**
 * PerspectiveMatrix4
 *
 * @author Vadim
 * @version 1.0 12.07.2017
 */
public class PerspectiveMatrix4 extends Matrix {

    private float fov;
    private float near;
    private float far;
    private float aspect;

    public PerspectiveMatrix4(float fov, float far, float near) {
        super(4,4);

        this.fov = fov;
        this.near = near;
        this.far = far;
        // TODO: calculate aspect

        update();
    }

    public float getFov() {
        return fov;
    }

    public void setFov(float fov) {
        this.fov = fov;
        update();
    }

    public float getNear() {
        return near;
    }

    public void setNear(float near) {
        this.near = near;
        update();
    }

    public float getFar() {
        return far;
    }

    public void setFar(float far) {
        this.far = far;
        update();
    }

    public float getAspect() {
        return aspect;
    }

    public void setAspect(float aspect) {
        this.aspect = aspect;
        update();
    }

    private void update() {

        set(0,0, 1f / aspect * (float)Math.tan(fov / 2f));
        set(1,1, 1f / (float)Math.tan(fov / 2f));
        set(2,2, - ((far + near) / (far - near)));
        set(2,3, -((2f * far * near) / (far - near)));
        set(3,2, -1f);
    }

}
