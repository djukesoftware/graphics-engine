package net.djuke.engine.math;

/**
 * TranslationMatrix4
 *
 * @author Vadim
 * @version 1.0 30.06.2017
 */
public class TranslationMatrix4 extends Matrix {

    private float x;
    private float y;
    private float z;

    public TranslationMatrix4() {
        super(4,4);
        update();
    }

    public TranslationMatrix4(float x, float y, float z) {
        super(4,4);
        this.x = x;
        this.y = y;
        this.z = z;
        update();
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
        update();
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        update();
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
        update();
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        update();
    }

    private void update() {
        set(0,0,1);
        set(1,1,1);
        set(2,2,1);
        set(3,3,1);

        set(0,3,x);
        set(1,3,y);
        set(2,3,z);
    }
}
