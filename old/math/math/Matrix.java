package net.djuke.engine.math;

/**
 * Matrix - matrix n*n
 * Created by azusa on 3/17/17.
 */
public class Matrix {

    private int n; // rows
    private int m; // columns
    private float[][] matrix;

    public Matrix(int n, int m) {
        this.n = n;
        this.m = m;
        matrix = new float[n][m];
    }

    /**
     * Additional operation
     * @param b other matrix
     * @return result matrix
     */
    public Matrix add(Matrix b) {
        if (this.getColumns() == b.getColumns() && this.getRows() == b.getRows()) {
            Matrix matrix = new Matrix(n, m);
            for (int n = 0; n < this.n; n++) {
                for (int m = 0; m < this.m; m++) {
                    matrix.set(n, m, this.matrix[n][m] + b.get(n,m));
                }
            }
            return matrix;
        } else return null;
    }

    /**
     * Scalar multiplication operation
     * @param alpha scalar
     * @return result matrix
     */
    public Matrix scalarMultiply(float alpha) {
        Matrix matrix = new Matrix(n, m);
        for (int n = 0; n < this.n; n++) {
            for (int m=0; m < this.m; m++) {
                matrix.set(n, m, this.matrix[n][m] * alpha);
            }
        }
        return matrix;
    }

    /**
     * Multiplication operation
     * @param b other matrix
     * @return result matrix
     */
    public Matrix multiply(Matrix b) {
        if (getColumns() == b.getRows()) {
            Matrix matrix = new Matrix(getRows(), b.getColumns());
            for (int i = 0; i < matrix.getRows(); i++) {
                for (int j = 0; j < matrix.getColumns(); j++) {
                    float sum = 0;
                    for (int r = 0; r < getColumns(); r++) {
                        sum += get(i,r) * b.get(r,j);
                    }
                    matrix.set(i, j, sum);
                }
            }
            return matrix;
        } else return null;
    }

    /**
     * Transpose matrix
     * @return result matrix
     */
    public Matrix transpose() {
        Matrix matrix = new Matrix(m, n);
        for (int n = 0; n < this.n; n++) {
            for (int m = 0; m < this.m; m++) {
                matrix.set(m, n, this.matrix[n][m]);
            }
        }
        return matrix;
    }

    /**
     * Print content of matrix
     * @deprecated
     */
    public void print() {
        for (int n = 0; n < this.n; n++) {
            for (int m = 0; m < this.m; m++) {
                System.out.print(matrix[n][m] + " ");
            }
            System.out.println();
        }
    }

    public float[] toArray() {
        float[] array = new float[n*m];
        int counter = 0;
        for (int m = 0; m < this.m; m++){
            for (int n = 0; n < this.n; n++) {
                array[counter] = get(n, m);
                counter++;
            }
        }
        return array;
    }

    public float get(int n, int m) {
        return matrix[n][m];
    }

    public void set(int n, int m, float value) {
        matrix[n][m] = value;
    }

    public int getColumns() {
        return m;
    }

    public int getRows() {
        return n;
    }

}

