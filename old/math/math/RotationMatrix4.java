package net.djuke.engine.math;

/**
 * RotationMatrix
 *
 * @author Vadim
 * @version 1.0 28.06.2017
 */
public class RotationMatrix4 extends Matrix {

    private float x;
    private float y;
    private float z;

    public RotationMatrix4() {
        super(4,4);
        update();
    }

    public RotationMatrix4(float x, float y, float z) {
        super(4,4);
        this.x = x;
        this.y = y;
        this.z = z;
        update();
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
        update();
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
        update();
    }

    public void setX(float x) {
        this.x = x;
        update();
    }

    public void set(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        update();
    }

    private void update() {
        set(0,0, (float)(Math.cos(y)*Math.cos(z)));
        set(0,1, (float)(Math.cos(x)*Math.sin(z) + Math.sin(x)*Math.sin(y)*Math.cos(z)));
        set(0,2, (float)(Math.sin(x)*Math.sin(z) - Math.cos(x)*Math.sin(y)*Math.cos(z)));

        set(1,0, (float)-(Math.cos(y)*Math.sin(z)));
        set(1,1, (float)(Math.cos(x)*Math.cos(z) - Math.sin(x)*Math.sin(y)*Math.sin(z)));
        set(1,2, (float)(Math.sin(x)*Math.cos(z)+Math.cos(x)*Math.sin(y)*Math.sin(z)));

        set(2,0, (float)Math.sin(y));
        set(2,1, (float)-(Math.sin(x)*Math.cos(y)));
        set(2,2, (float)(Math.cos(x)*Math.cos(y)));

        set(3,3, 1f);
    }

}
