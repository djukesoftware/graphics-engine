package net.djuke.engine.old.math;

import net.djuke.engine.math.Vector;

/**
 * Math - класс для вычесления нормалей
 * Created by vadim on 12.06.16.
 */
public class MathUtils {

    // Вычесляем нормаль треугольника
    public static Vector getTriangleNormal(Vector a, Vector b, Vector c) {
        return cross(
                new Vector(b.getX() - a.getX(), b.getY() - a.getY(), b.getZ() - a.getZ()),
                new Vector(c.getX() - a.getX(), c.getY() - a.getY(), c.getZ() - a.getZ()));
    }

    // Нормализация вектора
    public static Vector normalize(Vector v) {
        float inv = 1.0f / getLength(v);
        return new Vector(v.getX() * inv, v.getY() * inv, v.getZ() * inv);
    }

    // Длина вектора
    public static float getLength(Vector a) {
        return (float)Math.sqrt(Math.pow(a.getX(), 2.0) + Math.pow(a.getY(), 2.0) + Math.pow(a.getZ(), 2.0));
    }

    // Произведение векторов
    public static Vector cross(Vector a, Vector b) {
        return new Vector(
                a.getY() * b.getZ() - a.getZ() * b.getY(),
                a.getZ() * b.getX() - a.getZ() * b.getZ(),
                a.getX() * b.getY() - a.getY() * b.getX());
    }

    // Сложение векторов
    public static Vector add(Vector a, Vector b) {
        return new Vector(
                a.getX() + b.getX(),
                a.getY() + b.getY(),
                a.getZ() + b.getZ());
    }

    // Вычитание векторов
    public static Vector sub(Vector a, Vector b) {
        return new Vector(
                a.getX() - b.getX(),
                a.getY() - b.getY(),
                a.getZ() - b.getZ()
        );
    }

    // Умножение вектора на скаляр
    public static Vector cross(Vector a, float b) {
        return new Vector(a.getX() * b, a.getY() * b, a.getZ() * b);
    }

    // Сумма шести векторов (зачем?)
    public static Vector vectorSum6(Vector a, Vector b, Vector c, Vector d, Vector e, Vector f) {
        return new Vector(
                a.getX() + b.getX() + c.getX() + d.getX() + e.getX() + f.getX(),
                a.getY() + b.getY() + c.getY() + d.getY() + e.getY() + f.getY(),
                a.getZ() + b.getZ() + c.getZ() + d.getZ() + e.getZ() + f.getZ()
        );
    }

    // Трехмёрный вектор в двухмерный
    public static Vector getVector2(Vector v) {
        return new Vector(v.x / v.z, v.y / v.z, 0);
    }

    // Четырёхмерный вектор в трёхмерный
    public static Vector getVector3(Vector v) {
        return new Vector(v.x / v.w, v.y / v.w, v.z / v.w, 0);
    }

    // Умножаем матрицу на вектор
    public static Vector vectorMatrix(Vector v, Matrix2 m) {
        return new Vector(
                m.get( 0)*v.x + m.get( 1)*v.y,
                m.get( 2)*v.x + m.get( 3)*v.y
        );
    }

    // Умножаем матрицу на вектор
    public static Vector vectorMatrix(Vector v, Matrix3 m) {
        return new Vector(
                m.get( 0)*v.x + m.get( 1)*v.y + m.get( 2)*v.z,
                m.get( 3)*v.x + m.get( 4)*v.y + m.get( 5)*v.z,
                m.get( 6)*v.x + m.get( 7)*v.y + m.get( 8)*v.z
        );
    }

    // Умножаем матрицу на вектор
    public static Vector vectorMatrix(Vector v, Matrix4 m) {
        return new Vector(
                m.get( 0)*v.x + m.get( 1)*v.y + m.get( 2)*v.z + m.get( 3)*v.w,
                m.get( 4)*v.x + m.get( 5)*v.y + m.get( 6)*v.z + m.get( 7)*v.w,
                m.get( 8)*v.x + m.get( 9)*v.y + m.get(10)*v.z + m.get(11)*v.w,
                m.get(12)*v.x + m.get(13)*v.y + m.get(14)*v.z + m.get(15)*v.w
        );
    }

}
