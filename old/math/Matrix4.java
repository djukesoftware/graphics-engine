package net.djuke.engine.math;

/**
 * Matrix4 - matrix 4*4
 * Created by azusa on 3/17/17.
 */
public class Matrix4 extends Matrix {


    public Matrix4() {
        super(4, 4);
    }

    public void rotate(Vector axis) {
        Matrix4 rotation = new Matrix4();
        rotation.createIdentity();
        float a = axis.getX();
        float b = axis.getY();
        float c = axis.getZ();
        rotation.set(1,1,(float)Math.cos(b) * (float)Math.cos(c));
        rotation.set(1,2,(float)-Math.cos(b)*(float)Math.sin(c));
        rotation.set(1,3,(float)Math.sin(b));
        rotation.set(2,1,(float)(Math.sin(a)*Math.sin(b)*Math.cos(c)+Math.cos(a)*Math.sin(c)));
        rotation.set(2, 2, (float)(-Math.sin(a)*Math.sin(b)*Math.sin(c)+Math.cos(a)*Math.cos(c)));
        rotation.set(2,3,(float)(-Math.sin(a)*Math.cos(b)));
        rotation.set(3,1,(float)(-Math.cos(a)*Math.sin(b)*Math.cos(c)+Math.sin(a)*Math.sin(c)));
        rotation.set(3,2, (float)(Math.cos(a)*Math.sin(b)*Math.sin(c)+Math.sin(a)*Math.cos(c)));
        rotation.set(3,3,(float)(Math.cos(a)*Math.cos(b)));
        this.multMatrixByMatrix(rotation);
    }

    /**
     * @deprecated
     * @param angle - angle of rotation
     */
    public void rotate(float angle) {
    }

    public void translate(Vector position) {
        Matrix4 translation = new Matrix4();
        translation.createIdentity();
        translation.set(1,4,position.getX());
        translation.set(2,4,position.getY());
        translation.set(3,4,position.getZ());
        this.multMatrixByMatrix(translation);
    }

    public void scale(Vector scale) {
        Matrix4 scaling = new Matrix4();
        scaling.createIdentity();
        scaling.set(1,1,scale.getX());
        scaling.set(2,2,scale.getY());
        scaling.set(3,3,scale.getZ());
        this.multMatrixByMatrix(scaling);
    }

    public void perspective(float fov, float near, float far) {
        float s = 1f/((float)Math.tan((fov/2f)*((float)Math.PI/180f)));
        Matrix4 perspective = new Matrix4();
        perspective.set(1,1,s);
        perspective.set(2,2,s);
        perspective.set(3,3,-(far/(far-near)));
        perspective.set(3,4,-1);
        perspective.set(4,3,-(far/(far-near)));
        this.multMatrixByMatrix(perspective);
    }

}
