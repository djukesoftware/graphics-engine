package net.djuke.engine.math;

/**
 * Matrix3 - matrix 3*3
 * Created by azusa on 3/17/17.
 */
public class Matrix3 extends Matrix {


    public Matrix3() {
        super(3, 3);
    }

    public void rotate(float angle) {
        Matrix3 rotation = new Matrix3();
        rotation.createIdentity();
        rotation.set(1,1,(float)Math.cos(angle));
        rotation.set(1,2,-(float)Math.sin(angle));
        rotation.set(2,1,(float)Math.sin(angle));
        rotation.set(2,2,(float)Math.cos(angle));
        this.multMatrixByMatrix(rotation);
    }

    public void translate(Vector position) {
        Matrix3 translation = new Matrix3();
        translation.createIdentity();
        translation.set(1,3,position.getX());
        translation.set(2,3,position.getY());
        this.multMatrixByMatrix(translation);
    }

    public void scale(Vector scale) {
        Matrix3 scaling = new Matrix3();
        scaling.createIdentity();
        scaling.set(1,1,scale.getX());
        scaling.set(2,2,scale.getY());
        this.multMatrixByMatrix(scaling);
    }

    /**
     * @deprecated
     * @param fov - field of view
     * @param near - near distance from camera
     * @param far - far distance from camera
     */
    public void perspective(float fov, float near, float far) {
    }

    /**
     * @deprecated
     * @param axis - axis of rotation
     */
    public void rotate(Vector axis) {
    }

}
