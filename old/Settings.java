package net.djuke.engine.old;

import net.djuke.engine.old.graphic.Color;

import java.io.Serializable;

/**
 * Settings - класс настроек
 * Created by vadim on 27.05.16.
 */
public class Settings implements Serializable {

    // Singleton
    private static Settings instance;

    // Параметры игры
    public int width = 1366;
    public int height = 720;
    public boolean fullscreen = false;
    public Color colorTheme = new Color(0.18f, 0.19f, 0.24f);
    public boolean hideWindows = false;
    public int windowBorderSize = 2;
    public boolean msaa = true;
    public int msaaQuality = 4;
    public boolean shutdown = false;

    public Settings() {

    }

    public static Settings getInstance() {
        if (instance == null) instance = new Settings();
        return instance;
    }

    public static void loadInstance(Object object) {
        instance = (Settings)object;
    }


}
