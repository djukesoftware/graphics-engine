## 0.3.0
### Added
 * Creating screenshots
 * First version of characters
 * New format of Vector2f and Vector3f in JSON file
 * Localization for game and engine
 * 4D Perlin noise for shaders
 * Prototype of "Explore mode"
 * Calculating speed of mouse
 * Alpha channel of DjukeImage
 * Disable resize option for main window
 * Gravatar support
 * First planets and stars
 * Mouse scroll
 * Taking screenshots by PRINT_SCREEN key
 * PNG converter
 * Simple menu
### Other
 * Refactoring of graphics core and data subsystem

## 0.2.0
### Added
 * Settings of brightness and contrast
 * Gamma correction
 * Added frames for sequential scene rendering
 * Game sessions
 * Game settings
 * Prototype of menu stage
### Fixed
 * Rendering of images
 * Dynamic updating of mouse position
 
## 0.1.0
### Added
 * Refactoring of data subsystem
 * Concurrent data loading